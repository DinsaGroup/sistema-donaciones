<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReverseTable extends Migration
{

    public function up()
    {
        Schema::create('reverses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id')->unsigned();
            $table->foreign('transaction_id')->references('id')->on('transactions');
            $table->string('status')->default('PENDIENTE DE REVERSION');
            $table->timestamps();
            
        });
    }

    public function down()
    {
         Schema::dropIfExists('reverses');
    }
}
 