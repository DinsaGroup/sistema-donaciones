<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{

    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('process_type')->nullable();

            $table->integer('donation_type_id')->unsigned();
            $table->foreign('donation_type_id')->references('id')->on('donation_types');
            $table->integer('creditcard_id')->unsigned();
            $table->foreign('creditcard_id')->references('id')->on('creditcards');
            $table->integer('campaign_id')->unsigned();
            $table->foreign('campaign_id')->references('id')->on('campaigns');
            $table->integer('ip_id')->unsigned();
            $table->foreign('ip_id')->references('id')->on('ips');

            $table->float('amount');
            $table->string('transactionid')->nullable();
            $table->string('response_code')->nullable();
            $table->string('auth_code')->nullable();
            $table->string('response')->nullable();
            $table->string('avsresponse')->nullable();
            $table->string('cvvresponse')->nullable();
            $table->string('orderid_response')->nullable();
            $table->string('type')->nullable();
            $table->dateTimeTz('time_response')->nullable();
            $table->float('amount_response')->unsigned()->nullable();
            $table->float('purshamount')->unsigned()->nullable();
            $table->string('hash')->nullable();
            $table->string('referenceNumber')->nullable();
            $table->string('responseCode')->nullable();
            $table->string('responseCodeDescription')->nullable();
            $table->string('systemTraceNumber')->nullable();
            $table->string('status')->nullable();
            $table->string('comments')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
