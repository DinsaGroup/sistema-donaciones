<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditCardsTable extends Migration
{
    
    public function up()
    {
        Schema::create('creditcards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number');
            $table->string('name');
            $table->integer('month');
            $table->integer('year');
            $table->integer('cvc');
            $table->integer('lastdigit');
            $table->string('ico');
            $table->boolean('default');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('status')->default('active');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    
    public function down()
    {
        Schema::dropIfExists('creditcards');
    }
}
