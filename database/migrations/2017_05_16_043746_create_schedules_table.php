<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('donation_type_id')->unsigned();
            $table->foreign('donation_type_id')->references('id')->on('donation_types');
            $table->integer('creditcard_id')->unsigned();
            $table->foreign('creditcard_id')->references('id')->on('creditcards');
            $table->integer('campaign_id')->unsigned();
            $table->foreign('campaign_id')->references('id')->on('campaigns');
            $table->float('amount')->unsigned()->default(0);
            $table->timestamp('lastProcess')->nullable();
            $table->timestamp("nextProcess")->nullable();
            $table->integer('failedAttempts')->default(0);
            $table->string('status')->default('pendiente');
            $table->string('comments')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
