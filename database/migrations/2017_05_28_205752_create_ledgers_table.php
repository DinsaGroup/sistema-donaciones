<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLedgersTable extends Migration
{
    
    public function up()
    {
        Schema::create('ledgers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('donants')->default(0);
            $table->integer('admons')->default(0);
            $table->integer('managers')->default(0);
            $table->integer('donationtypes')->default(0);
            $table->integer('donationamounts')->default(0);
            $table->integer('countries')->default(0);
            $table->integer('currencies')->default(0);
            $table->integer('creditcards')->default(0);
            $table->integer('creditcard_actives')->default(0);
            $table->integer('creditcard_inactives')->default(0);
            $table->integer('transactions')->default(0);
            $table->integer('transaction_oks')->default(0);
            $table->integer('transaction_errors')->default(0);
            $table->integer('campaigns')->default(0);
            $table->integer('schedules')->default(0);
            $table->integer('schedule_actives')->default(0);
            $table->integer('schedule_pendings')->default(0);
            $table->double('exchange_rate',6,4)->default(30.3293);
            $table->float('lastdonation')->default(0);
            $table->float('month')->default(0);
            $table->float('year')->default(0);
            $table->float('total')->default(0);
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('ledgers');
    }
}
