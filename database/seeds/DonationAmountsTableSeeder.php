<?php

use Illuminate\Database\Seeder;

class DonationAmountsTableSeeder extends Seeder
{
    public function run()
    {
        $donationAmounts = [
        	[
        		'currency_id' => 2,
        		'amount' => 1,
        	],
        	[
        		'currency_id' => 2,
        		'amount' => 5,
        	],
        	[
        		'currency_id' => 2,
        		'amount' => 10,
        	],
        	[
        		'currency_id' => 2,
        		'amount' => 20,
        	],
        	[
        		'currency_id' => 2,
        		'amount' => 30,
        	],
        	[
        		'currency_id' => 2,
        		'amount' => 50,
        	],
        	[
        		'currency_id' => 2,
        		'amount' => 100,
        	],
        	[
        		'currency_id' => 2,
        		'amount' => 150,
        	],
        	[
        		'currency_id' => 2,
        		'amount' => 250,
        	],

        	[
        		'currency_id' => 1,
        		'amount' => 100,
        	],
        	[
        		'currency_id' => 1,
        		'amount' => 250,
        	],
        	[
        		'currency_id' => 1,
        		'amount' => 500,
        	],
        	[
        		'currency_id' => 1,
        		'amount' => 1000,
        	],
        	[
        		'currency_id' => 1,
        		'amount' => 2500,
        	],
        	[
        		'currency_id' => 1,
        		'amount' => 3000,
        	],
        	[
        		'currency_id' => 1,
        		'amount' => 3500,
        	],
        	[
        		'currency_id' => 1,
        		'amount' => 5000,
        	],
        	
        ];

        DB::table('donation_amounts')->insert($donationAmounts);
    }
}
