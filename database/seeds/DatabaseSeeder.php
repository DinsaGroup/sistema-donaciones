<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call(CampaignsTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(IpTableSeeder::class);
        $this->call(DonationTypesTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(DonationAmountsTableSeeder::class);
        $this->call(LedgerTableSeeder::class);
        $this->call(ReportTableSeeder::class);
    }
}
