<?php

use Illuminate\Database\Seeder;

use App\Transaction;


class TransactionsTableSeeder extends Seeder
{

    public function run()
    {

        $transactions =[
        	
        	[
        		'year'		=> $today->year,
                'month'     => $today->month-9,
        		'amount' 	=> 0.00,
        	],
        	
        ];

        // DB::table('reports')->insert($reports);
        Transaction::insert($transactions);

    }
}