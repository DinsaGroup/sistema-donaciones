<?php

use Illuminate\Database\Seeder;

use App\Ip;

use Carbon\Carbon;

class IpTableSeeder extends Seeder
{

    public function run()
    {
        $data = array([
        	'ip' => '127.0.0.1',
        	'user_id' => 1,
        	'banned' => false,
        	'created_at' => Carbon::today(),
        	'updated_at' => Carbon::today(),
        ]);
        Ip::insert($data);
    }
}
