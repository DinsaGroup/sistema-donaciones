<?php

use Illuminate\Database\Seeder;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use App\User;
use App\DonationAmount;
use App\DonationType;
use App\Country;
use App\Currency;
use App\CreditCard;
use App\Transaction;
use App\Campaign;
use App\Schedule;
use App\Ledger;



use Carbon\Carbon;

class LedgerTableSeeder extends Seeder
{

    public function run()
    {
        // $ledgers = [
        // 	[
        // 		'donants' => 0,
	       //      'admons' => 0,
	       //      'managers' => 0,
	       //      'donationtypes' => 0,
	       //      'donationamounts' => 0,
	       //      'countries' => 0,
	       //      'currencies' => 0,
	       //      'creditcards' => 0,
	       //      'creditcard_actives' => 0,
	       //      'creditcard_inactives' => 0,
	       //      'transactions' => 0,
	       //      'transaction_oks' => 0,
	       //      'transaction_errors' => 0,
	       //      'campaigns' => 0,
	       //      'schedules' => 0,
	       //      'schedule_actives' => 0,
	       //      'schedule_pendings' => 0,
	       //      'lastdonation' => 0,
	       //      'month' => 0,
	       //      'year' => 0,
	       //      'total' => 0,
        // 	],
        	
        // ];

        // DB::table('ledgers')->insert($ledgers);


        $donantes = User::whereHas('roles', function($q){$q->where('name', 'donante');})->get();
        $administradores = User::whereHas('roles', function($q){$q->where('name', 'superadmin');})->get();
        $managers = User::whereHas('roles', function($q){$q->where('name', 'manager');})->get();
        $donationtypes = DonationType::all();
        $donationamounts = DonationAmount::all();
        $countries = Country::all();
        $currencies = Currency::all();
        $creditcards = CreditCard::all();
        $creditcards_actives = 0;
        $creditcards_inactives = 0;

        foreach($creditcards as $item){
            if($item->status == 'active'){
                $creditcards_actives++;
            }else{
                $creditcards_inactives++;
            }
        }

        $transactions = Transaction::orderBy('id','asc')->get();
        $transaction_oks = 0;
        $transaction_errors = 0;
        $lastdonation = 0;
        $month = 0;
        $hoy = 0;
        $year = 0;
        $total = 0;
        $today = Carbon::now()->subHour(6);

        foreach($transactions as $item){
            if($item->status == 'Procesado'){
                $transaction_oks++;
                $lastdonation = $item->amount;

                $total += $item->amount;

                if($item->created_at->toDateString() == $today->toDateString()){
                    $hoy += $item->amount;
                }

                if($item->created_at->month == $today->month ){
                    $month += $item->amount;
                }

                if($item->created_at->year == $today->year){
                    $year += $item->amount;
                }

            }else{
                $transaction_errors++;
            }
        }

        $campaigns = Campaign::all();

        $schedules = Schedule::all();
        $schedule_actives = 0;
        $schedule_pendings = 0;

        foreach($schedules as $item){
            if($item->status == 'Procesado'){
                $schedule_actives++;
            }else{
                $schedule_pendings++;
            }
        }

        $ledger = new Ledger;
        $ledger->donants = count($donantes);
        $ledger->admons = count($administradores);
        $ledger->managers = count($managers);
        $ledger->donationtypes = count($donationtypes);
        $ledger->donationamounts = count($donationamounts);
        $ledger->countries = count($countries);
        $ledger->currencies = count($currencies);
        $ledger->creditcards = count($creditcards);
        $ledger->creditcard_actives = $creditcards_actives;
        $ledger->creditcard_inactives = $creditcards_inactives;
        $ledger->transactions = count($transactions);
        $ledger->transaction_oks = $transaction_oks;
        $ledger->transaction_errors = $transaction_errors;
        $ledger->campaigns = count($campaigns);
        $ledger->schedules = count($schedules);
        $ledger->schedule_actives = $schedule_actives;
        $ledger->schedule_pendings = $schedule_pendings;
        $ledger->lastdonation = $lastdonation;
        $ledger->lastdonation = number_format($lastdonation,2);
        $ledger->month = number_format($month,2);
        $ledger->year = number_format($year,2);
        $ledger->total = number_format($total,2); 
        $ledger->save();
    }

    private function host(){

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
                $ip = $_SERVER["HTTP_CLIENT_IP"];
            }
            elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
                $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            }
            elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
                $ip = $_SERVER["HTTP_X_FORWARDED"];
            }
            elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
                $ip = $_SERVER["HTTP_FORWARDED_FOR"];
            }
            elseif (isset($_SERVER["HTTP_FORWARDED"])){
                $ip = $_SERVER["HTTP_FORWARDED"];
            }
            else{
                $ip = $_SERVER["REMOTE_ADDR"];
            }

        
            
        $new_ip = New Ip;
        $new_ip->ip = $ip;
        $new_ip->user_id = Auth::id(); 
        $new_ip->save();

        return $new_ip->id;
    }

    
}
