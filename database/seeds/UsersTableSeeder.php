<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\User;
use App\Permission;

class UsersTableSeeder extends Seeder
{
    
    public function run()
    {
        // Insert a global admin user
        DB::table('users')->delete();
        DB::table('roles')->delete();
        DB::table('permissions')->delete();
        DB::table('role_user')->delete();
        DB::table('permission_role')->delete();
        $users = [
            [
                'name' => 'Super Control', 
                'email' => 'catoruno@dinsagroup.com', 
                'password' => bcrypt('123'),
                'country_id' => 159,
                'house' => '',
                'office' => '2224 3199',
                'phone' => '8787 6093',
                'boletin' => 'No',
            ],
            // [
            //     'name' => 'Mauricio Prueba', 
            //     'email' => 'catoruno@gmail.com', 
            //     'password' => bcrypt('*supercontrol!123*'),
            //     'country_id' => 159,
            //     'house' => '5456 8595',
            //     'office' => '',
            //     'phone' => '6060 2512',
            //     'boletin' => 'Si',
            // ],
            // [
            //     'name' => 'Ross Bustamante', 
            //     'email' => 'catoruno@hssoluciones.com', 
            //     'password' => bcrypt('*supercontrol!123*'),
            //     'country_id' => 159,
            //     'house' => '1234 5678',
            //     'office' => '2224 3199',
            //     'phone' => '',
            //     'boletin' => 'No',
            // ],
            // [
            //     'name' => 'Carlos Ortega', 
            //     'email' => 'cortega@dinsagroup.com', 
            //     'password' => bcrypt('*supercontrol!123*'),
            //     'country_id' => 159,
            //     'house' => '5555 5555',
            //     'office' => '2224 3199',
            //     'phone' => '8787 6093',
            //     'boletin' => 'Si',
            // ]
        ];
        DB::table('users')->insert($users);

        // Create Global User Roles
        $super_admin_role = new Role();
        $super_admin_role->name         		= 'superadmin';
        $super_admin_role->display_name 		= 'Super Administrador'; // optional
        $super_admin_role->description  		= 'Este usuario puede crear administradores del portal, asi como todos los permisos del sistema.'; // optional
        $super_admin_role->acceso = true;
        $super_admin_role->save();

        $manager = new Role();
        $manager->name 				= 'manager';
        $manager->display_name 		= 'Administrador general';
        $manager->description  		= 'Este usuario puede crear nuevas cuentas, y asignar ciertos permisos dentro de la plataforma.';
        $manager->acceso = true;
        $manager->save();

        $donante = new Role();
        $donante->name              = 'donante';
        $donante->display_name      = 'donante';
        $donante->description       = 'Este usuario es un donador del sistema, ha donado una o varias veces a la organización.';
        $donante->save();

        // Create Global user permission

        /*Donante*/
            $crearDonante = new Permission();
            $crearDonante->name         = 'crear-donante';
            $crearDonante->display_name = 'Crear Donante';
            $crearDonante->description  = 'Este permiso habilita al usuario para crear un nuevo donante.';
            $crearDonante->save();

            $editarDonante = new Permission();
            $editarDonante->name        = 'editar-donante';
            $editarDonante->display_name = 'Editar Donante';
            $editarDonante->description = 'Este permiso habilita al usuario para editar un donante existente.';
            $editarDonante->save();

            // $eliminarDonante = new Permission();
            // $eliminarDonante->name        = 'eliminar-donante';
            // $eliminarDonante->display_name = 'Eliminar Donante';
            // $eliminarDonante->description = 'Este permiso habilita al usuario para eliminar un donante existente.';
            // $eliminarDonante->save();

        /*Tarjeta*/
            $crearTarjeta = new Permission();
            $crearTarjeta->name         = 'crear-tarjeta';
            $crearTarjeta->display_name = 'Crear Tarjeta';
            $crearTarjeta->description  = 'Este permiso habilita al usuario para crear una nueva tarjeta.';
            $crearTarjeta->save();

            $editarTarjeta = new Permission();
            $editarTarjeta->name        = 'editar-tarjeta';
            $editarTarjeta->display_name = 'Editar Tarjeta';
            $editarTarjeta->description = 'Este permiso habilita al usuario para editar una tarjeta existente.';
            $editarTarjeta->save();

            // $eliminarTarjeta = new Permission();
            // $eliminarTarjeta->name        = 'eliminar-tarjeta'; 
            // $eliminarTarjeta->display_name = 'Eliminar Tarjeta';
            // $eliminarTarjeta->description = 'Este permiso habilita al usuario para eliminar una tarjeta existente.';
            // $eliminarTarjeta->save();

        /*Transacción*/
            $crearTransaccion = new Permission();
            $crearTransaccion->name         = 'crear-transaccion';
            $crearTransaccion->display_name = 'Crear Transacción';
            $crearTransaccion->description  = 'Este permiso habilita al usuario para crear una nueva transacción.';
            $crearTransaccion->save();

        /*Campaña*/
            $crearCampaign = new Permission();
            $crearCampaign->name         = 'crear-campaña';
            $crearCampaign->display_name = 'Crear Campaña';
            $crearCampaign->description  = 'Este permiso habilita al usuario para crear una nueva campaña.';
            $crearCampaign->save(); 

            $editarCampaign = new Permission();
            $editarCampaign->name        = 'editar-campaña';
            $editarCampaign->display_name = 'Editar Campaña';
            $editarCampaign->description = 'Este permiso habilita al usuario para editar una campaña existente.';
            $editarCampaign->save(); 

            // $eliminarCampaign = new Permission();
            // $eliminarCampaign->name        = 'eliminar-campaña'; 
            // $eliminarCampaign->display_name = 'Eliminar Campaña';
            // $eliminarCampaign->description = 'Este permiso habilita al usuario para eliminar una campaña existente.';
            // $eliminarCampaign->save(); 

        /*Donación*/
            $crearDonacion = new Permission();
            $crearDonacion->name         = 'crear-donación';
            $crearDonacion->display_name = 'Crear Donación';
            $crearDonacion->description  = 'Este permiso habilita al usuario para crear una nueva donación.';
            $crearDonacion->save();

            $editarDonacion = new Permission();
            $editarDonacion->name         = 'editar-donación';
            $editarDonacion->display_name = 'Editar Donación';
            $editarDonacion->description  = 'Este permiso habilita al usuario para editar una nueva donación.';
            $crearDonacion->save();

        /*Tipo de Donación*/
            $crearTipoDonacion = new Permission();
            $crearTipoDonacion->name         = 'crear-tipo-donación';
            $crearTipoDonacion->display_name = 'Crear Tipo de Donación';
            $crearTipoDonacion->description  = 'Este permiso habilita al usuario para crear un nuevo tipo de donación.';
            $crearTipoDonacion->save();

            $editarTipoDonacion = new Permission();
            $editarTipoDonacion->name        = 'editar-tipo-donación';
            $editarTipoDonacion->display_name = 'Editar Tipo de Donación';
            $editarTipoDonacion->description = 'Este permiso habilita al usuario para editar un tipo de donación existente.';
            $editarTipoDonacion->save();

            // $eliminarTipoDonacion = new Permission();
            // $eliminarTipoDonacion->name        = 'eliminar-tipo-donación';
            // $eliminarTipoDonacion->display_name = 'Eliminar Tipo de Donación';
            // $eliminarTipoDonacion->description = 'Este permiso habilita al usuario para eliminar un tipo donación existente.';
            // $eliminarTipoDonacion->save();

        /*Monto de Donación*/
            $crearMontoDonacion = new Permission();
            $crearMontoDonacion->name         = 'crear-monto-donación';
            $crearMontoDonacion->display_name = 'Crear Monto de Donación';
            $crearMontoDonacion->description  = 'Este permiso habilita al usuario para crear un nuevo monto de donación.';
            $crearMontoDonacion->save();

            $editarMontoDonacion = new Permission();
            $editarMontoDonacion->name        = 'editar-monto-donación';
            $editarMontoDonacion->display_name = 'Editar Monto de Donación';
            $editarMontoDonacion->description = 'Este permiso habilita al usuario para editar un monto de donación existente.';
            $editarMontoDonacion->save();

            // $eliminarMontoDonacion = new Permission();
            // $eliminarMontoDonacion->name        = 'eliminar-monto-donación';
            // $eliminarMontoDonacion->display_name = 'Eliminar Monto de Donación';
            // $eliminarMontoDonacion->description = 'Este permiso habilita al usuario para eliminar un monto donación existente.';
            // $eliminarMontoDonacion->save();

        /*Admin*/
            $crearAdmin = new Permission();
            $crearAdmin->name         = 'crear-administrador';
            $crearAdmin->display_name = 'Crear Administrador';
            $crearAdmin->description  = 'Este permiso habilita al usuario para crear un nuevo administrador.';
            $crearAdmin->save();

            $editarAdmin = new Permission();
            $editarAdmin->name        = 'editar-administrador';
            $editarAdmin->display_name = 'Editar Administrador';
            $editarAdmin->description = 'Este permiso habilita al usuario para editar un administrador existente.';
            $editarAdmin->save();

            // $eliminarAdmin = new Permission();
            // $eliminarAdmin->name        = 'eliminar-administrador';
            // $eliminarAdmin->display_name = 'Eliminar Administrador';
            // $eliminarAdmin->description = 'Este permiso habilita al usuario para eliminar un administrador existente.';
            // $eliminarAdmin->save();

        /*Rol*/
            $crearRol = new Permission();
            $crearRol->name         = 'crear-rol';
            $crearRol->display_name = 'Crear Rol';
            $crearRol->description  = 'Este permiso habilita al usuario para crear un nuevo rol.';
            $crearRol->save();

            $editarRol = new Permission();
            $editarRol->name        = 'editar-rol';
            $editarRol->display_name = 'Editar Rol';
            $editarRol->description = 'Este permiso habilita al usuario para editar un rol existente.';
            $editarRol->save();

            // $eliminarRol = new Permission();
            // $eliminarRol->name        = 'eliminar-rol';
            // $eliminarRol->display_name = 'Eliminar Rol';
            // $eliminarRol->description = 'Este permiso habilita al usuario para eliminar un rol existente.';
            // $eliminarRol->save();

        /*Moneda*/
            $crearMoneda = new Permission();
            $crearMoneda->name         = 'crear-moneda';
            $crearMoneda->display_name = 'Crear Moneda';
            $crearMoneda->description  = 'Este permiso habilita al usuario para crear una nueva moneda.';
            $crearMoneda->save(); 

            $editarMoneda = new Permission();
            $editarMoneda->name        = 'editar-moneda';
            $editarMoneda->display_name = 'Editar Moneda';
            $editarMoneda->description = 'Este permiso habilita al usuario para editar una moneda existente.';
            $editarMoneda->save(); 

            // $eliminarMoneda = new Permission();
            // $eliminarMoneda->name        = 'eliminar-moneda'; 
            // $eliminarMoneda->display_name = 'Eliminar Moneda';
            // $eliminarMoneda->description = 'Este permiso habilita al usuario para eliminar una moneda existente.';
            // $eliminarMoneda->save();

            $crearSuperAdmin = new Permission();
            $crearSuperAdmin->name         = 'crear-super-administrador';
            $crearSuperAdmin->display_name = 'Crear Super Administrador';
            $crearSuperAdmin->description  = 'Este permiso habilita al usuario para crear un nuevo super administrador.';
            $crearSuperAdmin->save();

            $editarSuperAdmin = new Permission();
            $editarSuperAdmin->name        = 'editar-super-administrador';
            $editarSuperAdmin->display_name = 'Editar Super Administrador';
            $editarSuperAdmin->description = 'Este permiso habilita al usuario para editar un super administrador existente.';
            $editarSuperAdmin->save();

            $crearPais = new Permission();
            $crearPais->name         = 'crear-pais';
            $crearPais->display_name = 'Crear País';
            $crearPais->description  = 'Este permiso habilita al usuario para crear un nuevo país.';
            $crearPais->save();

            $editarPais = new Permission();
            $editarPais->name        = 'editar-pais';
            $editarPais->display_name = 'Editar Pais';
            $editarPais->description = 'Este permiso habilita al usuario para editar un país existente.';
            $editarPais->save();

            $processCard = new Permission();
            $processCard->name        = 'procesar-tarjetas';
            $processCard->display_name = 'Procesar tarjetas';
            $processCard->description = 'Solo el super administrador puede procesar tarjetas por fuera del cron.';
            $processCard->save();
        
        // Add role to Admin User
        $user = User::where('email', '=', 'catoruno@dinsagroup.com')->first();
        $user->attachRole($super_admin_role);

        // $user = User::where('email', '=', 'catoruno@gmail.com')->first();
        // $user->attachRole($donante);

        // $user = User::where('email', '=', 'catoruno@hssoluciones.com')->first();
        // $user->attachRole($donante);

        // $user = User::where('email', '=', 'cortega@dinsagroup.com')->first();
        // $user->attachRole($manager);

        $super_admin_role->attachPermissions(array($crearDonante, $editarDonante, $crearTarjeta, $editarTarjeta, $crearTransaccion, $crearCampaign, $editarCampaign, $crearDonacion, $editarDonacion, $crearTipoDonacion, $editarTipoDonacion, $crearMontoDonacion, $editarMontoDonacion, $crearAdmin, $editarAdmin, $crearRol, $editarRol, $crearMoneda, $editarMoneda, $crearSuperAdmin, $editarSuperAdmin, $crearPais, $editarPais, $processCard));

        $manager->attachPermissions(array($crearDonante, $editarDonante, $crearTarjeta, $editarTarjeta, $crearTransaccion, $crearCampaign, $editarCampaign, $crearDonacion, $editarDonacion, $crearTipoDonacion, $editarTipoDonacion, $crearMontoDonacion, $editarMontoDonacion, $crearMoneda, $editarMoneda, $crearPais, $editarPais));

        // $super_admin_role->attachPermissions(array($crearRol, $editarRol));
    }
}
