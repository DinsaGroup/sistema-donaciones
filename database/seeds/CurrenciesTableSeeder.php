<?php

use Illuminate\Database\Seeder;

use App\Currency;

class CurrenciesTableSeeder extends Seeder
{

    public function run()
    {
        $currencies = [
        	[
        		'symbol' => 'C$',
                'symbol2' => 'CORD',
        		'name' => 'CORDOBAS',
        		'description' => 'Córdobas de Nicaragua',
        	],
        	[
        		'symbol' => 'U$',
                'symbol2' => 'USD',
        		'name' => 'DOLARES',
        		'description' => 'Dólares estadounidenses',
        	],
        	
        ];

        DB::table('currencies')->insert($currencies);
    }
}
