<?php

use Illuminate\Database\Seeder;

use App\DonationType;

class DonationTypesTableSeeder extends Seeder
{

    public function run()
    {
        $donationtypes = [
        	[
        		'name' => 'Puntual',
        		'description' => 'El usuario realiza una donación puntual.',
        		'days' => 0,
        	],
        	[
        		'name' => 'Mensual',
        		'description' => 'El usuario es recurrente, realiza donaciones mensuales.',
        		'days' => 1,
        	],
        	[
        		'name' => 'Bimensual',
        		'description' => 'El usuario es recurrente, realiza donanciones cada dos meses.',
        		'days' => 2,
        	],
        	[
        		'name' => 'Trimestral',
        		'description' => 'El usuario es recurrente, realiza donanciones cada tres meses.',
        		'days' => 3,
        	],
        	[
        		'name' => 'Cuatrimestral',
        		'description' => 'El usuario es recurrente, realiza donanciones cada cuatro meses.',
        		'days' => 4,
        	],
        	[
        		'name' => 'Semestral',
        		'description' => 'El usuario es recurrente, realiza donanciones cada seis meses.',
        		'days' => 6,
        	],
        	[
        		'name' => 'Anual',
        		'description' => 'El usuario es recurrente, realiza donanciones anualmente.',
        		'days' => 12,
        	],
        ];

        DB::table('donation_types')->insert($donationtypes);
    }
}
