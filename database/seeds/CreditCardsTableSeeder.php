<?php

use Illuminate\Database\Seeder;

use App\CreditCard;
use Carbon\Carbon;

class CreditCardsTableSeeder extends Seeder
{

    public function run()
    {
        $creditcards = [
        	[
        		'name' => 'TEST CREDIT CARD',
                'number' => encrypt('4111111111111111'),
        		'month' => 9,
                'year' => 2015,
                'cvc' => 456,
                'lastdigit' => 4111,
                'ico' => 'visa.png',
                'user_id' => 2,
                'default' => false,
                'status' => 'inactive',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
        	],
            [
                'name' => 'TEST CREDIT CARD',
                'number' => encrypt('4012888888881881'),
                'month' => 9,
                'year' => 2016,
                'cvc' => 456,
                'lastdigit' => 4123,
                'ico' => 'visa.png',
                'user_id' => 2,
                'default' => false,
                'status' => 'inactive',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'AMERICAN EXPRESS',
                'number' => encrypt('371449635398431'),
                'month' => 9,
                'year' => 2019,
                'cvc' => 456,
                'lastdigit' => 8431,
                'ico' => 'american-express.png',
                'user_id' => 2,
                'default' => true,
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'name' => 'MASTERCARD CREDIT CARD',
                'number' => encrypt('4000056655665556'),
                'month' => 8,
                'year' => 2017,
                'cvc' => 456,
                'lastdigit' => 5556,
                'ico' => 'mastercard.png',
                'user_id' => 3,
                'default' => false,
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'AMERICAN EXPRESS',
                'number' => encrypt('378282246310005'),
                'month' => 9,
                'year' => 2019,
                'cvc' => 456,
                'lastdigit' => 8431,
                'ico' => 'american-express.png',
                'user_id' => 3,
                'default' => true,
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'name' => 'TEST CREDIT CARD',
                'number' => encrypt('4242424242424242'),
                'month' => 7,
                'year' => 2022,
                'cvc' => 456,
                'lastdigit' => 4242,
                'ico' => 'visa.png',
                'user_id' => 4,
                'default' => true,
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'TEST CREDIT CARD',
                'number' => encrypt('4012888888881881'),
                'month' => 3,
                'year' => 2015,
                'cvc' => 456,
                'lastdigit' => 1881,
                'ico' => 'visa.png',
                'user_id' => 4,
                'default' => false,
                'status' => 'inactive',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'AMERICAN EXPRESS',
                'number' => encrypt('371449635398431'),
                'month' => 9,
                'year' => 2014,
                'cvc' => 456,
                'lastdigit' => 8431,
                'ico' => 'american-express.png',
                'user_id' => 4,
                'default' => false,
                'status' => 'inactive',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        	
        ];

        DB::table('creditcards')->insert($creditcards);
    }
}
