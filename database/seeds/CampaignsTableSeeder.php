<?php

use Illuminate\Database\Seeder;

use App\Campaign;

class CampaignsTableSeeder extends Seeder
{

    public function run()
    {
        $campaigns = [
        	[
        		'name' => 'Todas las campañas',
                'description' => 'El donante elije apoyar todas las campañas que el FCAM ejecuta.',
        		'active' => true,
        	],
        	[
        		'name' => 'Mujeres del campo',
                'description' => 'Projecto ejecutado por organización aleada al fcam.',
        		'active' => true,
        	],
        	[
        		'name' => 'Emprendamos juntas',
                'description' => 'El programa ayuda a las mujeres a crear fondos para financiar emprendimientos de mujeres organizadas.',
        		'active' => true,
        	],
        	[
        		'name' => 'Aborto terapeutico',
                'description' => 'Apoyo a la despenalización del aborto terapeutico.',
        		'active' => true,
        	],
        	[
        		'name' => 'Por una causa',
                'description' => 'Demo para probar si funciona.',
        		'active' => false,
        	],	
        	
        ];

        DB::table('campaigns')->insert($campaigns);
    }
}
