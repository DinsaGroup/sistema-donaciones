<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class ReportTableSeeder extends Seeder
{

    public function run()
    {
        $today = Carbon::now();

        $reports =[
        	
        	[
        		'year'		=> $today->year,
                'month'     => $today->month-9,
        		'amount' 	=> 0.00,
        	],
        	[
        		'year'		=> $today->year,
                'month'     => $today->month-8,
        		'amount' 	=> 0.00,
        	],
        	[
        		'year'		=> $today->year,
                'month'     => $today->month-7,
        		'amount' 	=> 0.00,
        	],
        	[
        		'year'		=> $today->year,
                'month'     => $today->month-6,
        		'amount' 	=> 0.00,
        	],
        	[
        		'year'		=> $today->year,
                'month'     => $today->month-5,
        		'amount' 	=> 0.00,
        	],
        	[
        		'year'		=> $today->year,
                'month'     => $today->month-4,
        		'amount' 	=> 0.00,
        	],
        	[
        		'year'		=> $today->year,
                'month'     => $today->month-3,
        		'amount' 	=> 0.00,
        	],
        	[
        		'year'		=> $today->year,
                'month'     => $today->month-2,
        		'amount' 	=> 0.00,
        	],
        	[
        		'year'		=> $today->year,
                'month'     => $today->month-1,
        		'amount' 	=> 0.00,
        	],
            [
                'year'      => $today->year,
                'month'     => $today->month,
                'amount'    => 0.00,
            ],
            [
                'year'      => $today->year,
                'month'     => $today->month+1,
                'amount'    => 0.00,
            ],
            [
                'year'      => $today->year,
                'month'     => $today->month+2,
                'amount'    => 0.00,
            ],
        ];

        DB::table('reports')->insert($reports);

    }
}
