// Main javascript project for DINAMICDIN
$(document).ready(function () {	
	// Ocultar mensajes de alerta
    $('.alert').delay(3000).fadeOut('slow');

    //Validate input number quantity
	$('.only_numbers').keydown(function(e){

		// Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [ 8, 46, 9, 27, 13,188,110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
		// Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }

	});

    //Validate only integer input
    $('.only_integer').keydown(function(e){

        // Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [ 8, 46, 9, 27, 13]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }

    });

    //Validate input phone number
    $('.phone_number').keydown(function(e){
        console.log(e.keyCode);
        // Allow: backspace, delete, tab, escape, enter, ( , ) , +
        if ($.inArray(e.keyCode, [8, 32, 46, 9, 27, 13, 57, 48, 107]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }

    });
})