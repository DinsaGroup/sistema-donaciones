<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();



Route::get('/', function () {
    return view('welcome');
});

Route::get('/server', function(){
	return $_SERVER['SERVER_NAME'];
});

Route::group(['middleware'=> 'revalidate'], function(){ 

	// $a = DB::table('roles')->where('acceso', true)->get();
 //            foreach ($a as $item) {
 //                $array[] = $item->name;
 //            }
 //            list($keys,$values) = array_divide($array);
 //            $b = implode('|', $values);
	$b = "superadmin|manager";
	Route::group(['prefix' => 'ControlPanel', 'middleware' => ['auth', 'role:' . $b], 'namespace' => 'controlpanel'], function () {

		// Dashboard
		Route::resource('/', 'DashboardController');

		// Donantes
		Route::resource('/donantes','DonanteController');

		// Tarjetas de creditos donantes
		Route::resource('/tarjetas','TarjetaController');
		
		// Paises
		Route::resource('/paises','PaisController');

		// Tipos de donantes
		Route::resource('/tipo-donantes','TipoDonanteController');

		// Montos de donaciones
		Route::resource('/monto-donaciones','MontoDonacionController');

		// Roles administrativos
		Route::resource('/roles-administrativos','RoleController');

		// Administrativo
		Route::resource('/administradores','UserController');

		// Administrativo
		Route::resource('/configuraciones','ConfigController');

		// Administrativo
		Route::resource('/monedas','MonedaController');

		// Transacciones
		Route::resource('/transacciones','TransaccionController'); 
		Route::get('/donaciones/{status}','TransaccionController@index');


		// Revertir pago
		Route::get('/revertir-cobro/{id}','TransaccionController@revertir');

		// Emails
		Route::resource('/emails','EmailController');

		// Calendario
		Route::resource('/calendario','CalendarioController');

		// Calendario
		Route::resource('/campaigns','CampaignController');

		// User information
		Route::any('/user/{id}','QueryController@user');
		Route::any('/email/{email}','QueryController@email');
		Route::any('/amounts/{currency}','QueryController@amounts');

		// Actualizar informacion dashboard
		Route::get('/update-data/ledger','UpdateDataController@ledger');

		// Processar tarjetas programadas
		Route::get('/processcards','UpdateDataController@processcards');

		// Test webservice from bac conection
		Route::get('/webservice','WebServiceController@index');

		// ipaccess
		Route::resource('/ipaccess','IpController');

		// Log files
		Route::resource('/logfiles','LogfileController');

		// Reportes
		Route::get('reportes','ReportController@index');
		Route::get('reporte/donante/{id}','ReportController@donante');
		Route::get('reporte/donantes','ReportController@todosLosDonantes');
		Route::get('reporte/tarjetas','ReportController@tarjetas');
		Route::post('reporte/donaciones','ReportController@donaciones');
		Route::post('reporte/transacciones','ReportController@transacciones');
		Route::post('reporte/{seccion}/{action}', 'ReportController@createPDF');


		// Import transactions
		Route::get('importar', 'ImportController@index');

		Route::post('importdonantes','ImportController@donantes');

		Route::post('importtarjetas','ImportController@tarjetas');

		Route::post('importips','ImportController@ips');

		Route::post('importtransacciones','ImportController@transacciones');

		Route::post('importcalendario','ImportController@calendario');


	});


});
