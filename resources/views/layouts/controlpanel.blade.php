<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>FCAM | @yield('title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{{ asset('bootstrap/css/bootstrap.min.css') }}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="{{{ asset('plugins/morris/morris.css') }}}" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="{{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}}" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="{{{ asset('plugins/daterangepicker/daterangepicker-bs3.css') }}}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{{ asset('css/AdminLTE.css') }}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="{{{ asset('css/skins/_all-skins.min.css') }}}" rel="stylesheet" type="text/css" />

    <!-- DATA TABLES -->
    <link href="{{{ asset('plugins/datatables/dataTables.bootstrap.css') }}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue sidebar-collapse sidebar-open">
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <i class="fa fa-check"></i>
            {{ Session::get('success_message') }}
        </div>
    @endif

    @if(Session::has('error_message'))
        <div class="alert alert-danger">
            <i class="fa fa-belt"></i>
            {{ Session::get('error_message') }}
        </div>
    @endif

    <div class="wrapper">
      
      @include('controlpanel.header')
      
      @include('controlpanel.sidebar')

      @yield('content')

      @include('controlpanel.footer')

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.3 -->
    <script src="{{{ asset('plugins/jQuery/jQuery-2.1.3.min.js') }}}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{{ asset('bootstrap/js/bootstrap.min.js') }}}" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="{{{ asset('plugins/fastclick/fastclick.min.js') }}}"></script>
    <!-- AdminLTE App -->
    <script src="{{{ asset('js/app.min.js') }}}" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="{{{ asset('plugins/sparkline/jquery.sparkline.min.js') }}}" type="text/javascript"></script>
    
    <!-- daterangepicker -->
    <script src="{{{ asset('plugins/daterangepicker/daterangepicker.js') }}}" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="{{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}}" type="text/javascript"></script>
    <!-- iCheck -->
    {{-- <script src="{{{ asset('plugins/iCheck/icheck.min.js') }}}" type="text/javascript"></script> --}}
    <!-- SlimScroll 1.3.0 -->
    <script src="{{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}}" type="text/javascript"></script>
    
    <script src="{{{ asset('plugins/datatables/jquery.dataTables.js') }}}" type="text/javascript"></script>
    <script src="{{{ asset('plugins/datatables/dataTables.bootstrap.js') }}}" type="text/javascript"></script>

    <!-- Personal purposes -->
    <script src="{{{ asset('js/main.js') }}}" type="text/javascript"></script>

    @yield('javascript')

  </body>
</html>