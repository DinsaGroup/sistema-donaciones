@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }}</h1>
            <small>Datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
          
        </section>

        <section class="content">

            <div class="row">
                {{-- Tipo de donantes --}}
                <div class="col-md-4">
                  <!-- TABLE: LATEST ORDERS -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title"><i class="fa fa-calendar"></i> Tipo de donaciones</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="direct-chat-messages">
                        @if(Auth::user()->can('editar-tipo-donación'))
                          <div class="table-responsive">
                              <table class="table no-margin">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>TIPO</th>
                                    <th>DIAS</th>
                                    <th><i class="fa fa-sort-desc"></i></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach($donante_types as $item)
                                      <tr>
                                          <td><a href="{{ url('ControlPanel/tipo-donantes/'.$item->id.'/edit') }}">{{ $item->id }}</a></td>
                                          <td>{{ $item->name }}</td>
                                          <td>{{ $item->days }}</td>
                                          <td>
                                              <a href="{{ url('ControlPanel/tipo-donantes/' . $item->id . '/edit') }}" class="details">
                                                  <i class="fa fa-pencil"></i>
                                              </a>
                                          </td>
                                      </tr>
                                  @endforeach
                                </tbody>
                              </table>
                          </div><!-- /.table-responsive -->
                        @else
                        <div class="table-responsive">
                              <table class="table no-margin">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>TIPO</th>
                                    <th>DIAS</th>
                                    {{-- <th><i class="fa fa-sort-desc"></i></th> --}}
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach($donante_types as $item)
                                      <tr>
                                          <td><a href="{{ url('ControlPanel/tipo-donantes/'.$item->id.'/edit') }}">{{ $item->id }}</a></td>
                                          <td>{{ $item->name }}</td>
                                          <td>{{ $item->days }}</td>
                                          {{-- <td>
                                              <a href="{{ url('ControlPanel/tipo-donantes/' . $item->id . '/edit') }}" class="details">
                                                  <i class="fa fa-pencil"></i>
                                              </a>
                                          </td> --}}
                                      </tr>
                                  @endforeach
                                </tbody>
                              </table>
                          </div><!-- /.table-responsive -->
                        @endif
                        </div>
                    </div><!-- /.box-body -->
                    @if(Auth::user()->can('crear-tipo-donación'))
                    <div class="box-footer clearfix">
                      <a href="{{ url('ControlPanel/tipo-donantes/create') }}" class="btn btn-sm btn-info btn-flat pull-left">Agregar nuevo</a>
                    </div><!-- /.box-footer -->
                    @else
                    <div class="box-footer clearfix">
                      
                    </div><!-- /.box-footer -->
                    @endif
                  </div><!-- /.box -->
                </div>
                
                {{-- Monto de las donaciones --}}
                <div class="col-md-4">
                  <!-- TABLE: LATEST ORDERS -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title"><i class="fa fa-money"></i> Monto donaciones</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="direct-chat-messages">
                        @if(Auth::user()->can('editar-monto-donación'))
                          <div class="table-responsive">
                            <table class="table no-margin">
                              <thead>
                                <tr>
                                  <th>ID</th>
                                  <th>SIMB.</th>
                                  <th>CANTID.</th>
                                  <th><i class="fa fa-sort-desc"></i></th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($donationAmounts as $item)
                                    <tr>
                                        <td><a href="{{ url('ControlPanel/monto-donaciones/'.$item->id.'/edit') }}">{{ $item->id }}</a></td>
                                        <td>{{ $item->currency->symbol }}</td>
                                        <td>{{ number_format($item->amount,2,'.',',') }} {{ $item->currency->symbol2 }}</td>
                                        <td>
                                            <a href="{{ url('ControlPanel/monto-donaciones/' . $item->id . '/edit') }}" class="details">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div><!-- /.table-responsive -->
                        @else
                          <div class="table-responsive">
                            <table class="table no-margin">
                              <thead>
                                <tr>
                                  <th>ID</th>
                                  <th>SIMB.</th>
                                  <th>CANTID.</th>
                                  {{-- <th><i class="fa fa-sort-desc"></i></th> --}}
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($donationAmounts as $item)
                                    <tr>
                                        <td><a href="{{ url('ControlPanel/monto-donaciones/'.$item->id.'/edit') }}">{{ $item->id }}</a></td>
                                        <td>{{ $item->currency->symbol }}</td>
                                        <td>{{ number_format($item->amount,2,'.',',') }} {{ $item->currency->symbol2 }}</td>
                                        {{-- <td>
                                            <a href="{{ url('ControlPanel/monto-donaciones/' . $item->id . '/edit') }}" class="details">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </td> --}}
                                    </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div><!-- /.table-responsive -->
                        @endif
                        </div>
                    </div><!-- /.box-body -->
                    @if(Auth::user()->can('crear-monto-donación'))
                    <div class="box-footer clearfix">
                      <a href="{{ url('ControlPanel/monto-donaciones/create') }}" class="btn btn-sm btn-info btn-flat pull-left">Agregar nuevo</a>
                    </div><!-- /.box-footer -->
                    @else
                    <div class="box-footer clearfix">

                    </div><!-- /.box-footer -->
                    @endif
                  </div><!-- /.box -->
                </div>

                {{-- Paises del sistema --}}
                <div class="col-md-4">
                  <!-- TABLE: LATEST ORDERS -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title"><i class="fa fa-globe"></i> Paises del sistema</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="direct-chat-messages">
                        @if(Auth::user()->can('crear-pais'))
                          <div class="table-responsive">
                            <table class="table no-margin">
                              <thead>
                                <tr>
                                  <th>CODE</th>
                                  <th>AREA</th>
                                  <th>PAIS</th>
                                  <th><i class="fa fa-sort-desc"></i></th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($countries as $item)
                                    <tr>
                                        <td><a href="{{ url('ControlPanel/paises/'.$item->id.'/edit') }}">{{ $item->code1 }}</a></td>
                                        <td>{{ $item->ext }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>
                                            <a href="{{ url('ControlPanel/paises/' . $item->id . '/edit') }}" class="details">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div><!-- /.table-responsive -->
                        @else
                          <div class="table-responsive">
                            <table class="table no-margin">
                              <thead>
                                <tr>
                                  <th>CODE</th>
                                  <th>AREA</th>
                                  <th>PAIS</th>
                                  {{-- <th><i class="fa fa-sort-desc"></i></th> --}}
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($countries as $item)
                                    <tr>
                                        <td><a href="{{ url('ControlPanel/paises/'.$item->id.'/edit') }}">{{ $item->code1 }}</a></td>
                                        <td>{{ $item->ext }}</td>
                                        <td>{{ $item->name }}</td>
                                        {{-- <td>
                                            <a href="{{ url('ControlPanel/paises/' . $item->id . '/edit') }}" class="details">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </td> --}}
                                    </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div><!-- /.table-responsive -->
                        @endif
                      </div>
                    </div><!-- /.box-body -->
                    @if(Auth::user()->can('editar-pais'))
                    <div class="box-footer clearfix">
                      <a href="{{ url('ControlPanel/paises/create') }}" class="btn btn-sm btn-info btn-flat pull-left">Agregar nuevo</a>
                    </div><!-- /.box-footer -->
                    @else
                    <div class="box-footer clearfix">
                      
                    @endif
                  </div><!-- /.box -->
                </div>
            </div><!-- /.row -->

            <div class="row">
                {{-- Paises del sistema --}}
                <div class="col-md-4">
                  <!-- TABLE: LATEST ORDERS -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title"><i class="fa fa-usd"></i> Monedas</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <div class="direct-chat-messages">
                      @if(Auth::user()->can('editar-moneda'))
                        <div class="table-responsive">
                          <table class="table no-margin">
                            <thead>
                              <tr>
                                <th>SIMB.</th>
                                <th>COD.</th>
                                <th>DESC.</th>
                                <th><i class="fa fa-sort-desc"></i></th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($currencies as $item)
                                  <tr>
                                      <td><a href="{{ url('ControlPanel/monedas/'.$item->id.'/edit') }}">{{ $item->symbol }}</a></td>
                                      <td>{{ $item->symbol2 }}</td>
                                      <td>{{ $item->name }}</td>
                                      <td>
                                          <a href="{{ url('ControlPanel/monedas/' . $item->id . '/edit') }}" class="details">
                                              <i class="fa fa-pencil"></i>
                                          </a>
                                      </td>
                                  </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div><!-- /.table-responsive -->
                      @else
                        <div class="table-responsive">
                          <table class="table no-margin">
                            <thead>
                              <tr>
                                <th>SIMB.</th>
                                <th>COD.</th>
                                <th>DESC.</th>
                                {{-- <th><i class="fa fa-sort-desc"></i></th> --}}
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($currencies as $item)
                                  <tr>
                                      <td><a href="{{ url('ControlPanel/monedas/'.$item->id.'/edit') }}">{{ $item->symbol }}</a></td>
                                      <td>{{ $item->symbol2 }}</td>
                                      <td>{{ $item->name }}</td>
                                      {{-- <td>
                                          <a href="{{ url('ControlPanel/monedas/' . $item->id . '/edit') }}" class="details">
                                              <i class="fa fa-pencil"></i>
                                          </a>
                                      </td> --}}
                                  </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div><!-- /.table-responsive -->
                      @endif
                      </div>
                    </div><!-- /.box-body -->
                    @if(Auth::user()->can('crear-moneda'))
                    <div class="box-footer clearfix">
                      <a href="{{ url('ControlPanel/monedas/create') }}" class="btn btn-sm btn-info btn-flat pull-left">Agregar nuevo</a>
                    </div><!-- /.box-footer -->
                    @else
                    <div class="box-footer clearfix">
                      
                    @endif
                  </div><!-- /.box -->
                </div>
            </div>
            
        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection