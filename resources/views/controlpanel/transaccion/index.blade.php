@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
        @if(Auth::user()->can('crear-transaccion'))
          <h1>{{ $title }} <a href="{{ url('ControlPanel/transacciones/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a></h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
        @else
          <h1>{{ $title }}</h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
        @endif
        </section>

        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-exchange"></i> Historial de transacciones</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="transaction_history" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th width="50">DONACION</th>
                        <th>TIPO</th>
                        <th width="85">TRANS. ID</th>
                        <th>APLICADO A</th>
                        <th>DONANTE</th>
                        <th>TARJETA</th>
                        <th>MONTO U$</th>
                        <th>FECHA</th>
                        <th>ESTATUS</th>
                        <th>OBSERVACION</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($transactions as $item)
                        <tr>
                          <td class="text-center">
                            <a href="{{ url('ControlPanel/transacciones/'.$item->id) }}" class="" >
                              {{ $item->id }}
                            </a>
                          </td>
                          <td class="text-center">{{ $item->donationtype->name }}</td>
                          <td class="text-center">{{ $item->transactionid }}</td>
                          <td>{{ $item->campaign->name }}</td>
                          <td class="text-center">
                            <a href="{{ url('ControlPanel/donantes/'.$item->creditcard->user->id) }}" >
                              <img src="{{ url('img/user/'.$item->creditcard->user->photo )}}" class="user-image"> {{ $item->creditcard->user->name }}
                            </a>
                          </td>
                          <td class="text-center"><img src="{{ url('img/credit/'. $item->creditcard->ico) }}" class="ccard"> **** {{ $item->creditcard->lastdigit }}</td>
                          <td class="text-center">U$ {{ number_format($item->amount,2,'.',',') }}</td>
                          <td class="text-center">{{ $item->created_at }}</td>
                          <td class="text-center">{{ $item->status }}</td>
                          <td>{{ $item->comments }}</td>
                        </tr>
                      @endforeach
                      
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>ID</th>
                        <th>TIPO</th>
                        <th>TRANS. ID</th>
                        <th>APLICADO A</th>
                        <th>DONANTE</th>
                        <th>TARJETA</th>
                        <th>MONTO U$</th>
                        <th>FECHA</th>
                        <th>ESTATUS</th>
                        <th>OBSERVACION</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                </div> {{-- col-md-12 --}}
            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')
<!-- page script -->
    <script type="text/javascript">
      $(function () {
        // $("#transaction_history").dataTable();
        $('#transaction_history').dataTable({
          "bSortClasses": 'sorting_3',
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "aaSorting": [[7,'desc']],
          "iDisplayLength": 100,
          "aLengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]]
        });
      });
    </script>
@endsection