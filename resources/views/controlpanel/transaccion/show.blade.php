@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} {{ $transaction->id }}<a href="{{ url('ControlPanel/transacciones') }}" class="btn btn-danger pull-right btn-sm"><i class="fa fa-times"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

                <div class="row">

                    <div class="col-md-12">
                        <div class="col-md-2 txt-right">
                            Transacción no.
                        </div>
                        <div class="col-md-6">
                            : {{ $transaction->id }}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 txt-right">
                            Tipo de transacción
                        </div>
                        <div class="col-md-6">
                            : {{ $transaction->process_type }}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 txt-right">
                            Tipo de donación
                        </div>
                        <div class="col-md-6">
                            : {{ $transaction->donationtype->name }}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 txt-right">
                            Donante
                        </div>
                        <div class="col-md-6">
                            : {{ $transaction->creditcard->user->name }}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 txt-right">
                            Tarjeta de crédito
                        </div>
                        <div class="col-md-6">
                            : {{ decrypt($transaction->creditcard->number) }}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 txt-right">
                            Monto
                        </div>
                        <div class="col-md-6">
                            : U$ {{ number_format($transaction->amount,2,'.',',') }}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 txt-right">
                            Transacción BAC
                        </div>
                        <div class="col-md-6">
                            : {{ $transaction->transactionid }}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 txt-right">
                            Autorización
                        </div>
                        <div class="col-md-6">
                            : {{ $transaction->auth_code }}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2 txt-right">
                            Fecha
                        </div>
                        <div class="col-md-6">
                            : {{ $transaction->created_at }}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2 txt-right">
                            Estatus
                        </div>
                        <div class="col-md-6">
                            : {{ $transaction->status }}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 txt-right">
                            Descripción
                        </div>
                        <div class="col-md-6">
                            : {{ $transaction->comments }}
                        </div>
                    </div>

                    @if(trim($transaction->status) === 'APROBADA' && trim($transaction->process_type) !=='REVERSION' )
                    <div class="col-md-12">
                        <div class="col-md-2 txt-right">
                            Acciones
                        </div>
                        <div class="col-md-6">
                            : <a href="{{ url('ControlPanel/revertir-cobro/'.$transaction->id) }}" class="" >Revertir cobro</a>
                        </div>
                    </div>
                    @endif

                </div>
                 

                

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')
<script>
    $(document).ready(function(){

    })

</script>
@endsection