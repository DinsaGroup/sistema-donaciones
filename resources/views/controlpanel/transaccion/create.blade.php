@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ url('ControlPanel/transacciones') }}" class="btn btn-danger pull-right btn-sm"><i class="fa fa-times"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">
            {!! Form::open(['url' => 'ControlPanel/transacciones', 'class' => 'form-horizontal', 'method'=>'POST']) !!}
            {{ csrf_field() }}
                <div class="row">
                    <input type="hidden" name="option">
                </div>

                <div class="row">

                    <div class="col-md-3">
                        {!! Form::label('donante_id', 'DONANTE : ', ['class' => ' control-label']) !!}
                        <div class="">
                            {!! Form::select('donante_id',$donantes, null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar donante', 'id'=>'donante']) !!}
                            @if(Auth::user()->can('crear-donante'))
                                <a href="{{ url('ControlPanel/donantes/create') }}"><i class="fa fa-plus"></i> Crear nuevo</a>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('donationType_id', 'TIPO DE DONACIÓN : ', ['class' => ' control-label']) !!}
                        <div class="">
                            {!! Form::select('donationType_id',$donationTypes, null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar tipo donación', 'id'=>'donationType']) !!}
                        </div>
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('donationType_id', 'MONEDA : ', ['class' => ' control-label']) !!}
                        <div class="">
                            {!! Form::select('currency',$currencies, null, [ 'class'=>'form-control', 'required'=>'required', 'placeholder'=>'Seleccionar moneda', 'id'=>'currency']) !!}
                        </div>
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('amount_id', 'MONTO A DONAR: ', ['class' => ' control-label']) !!}
                        <div class="">
                            {!! Form::select('amount_id',[] ,null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar monto', 'id'=>'amount_id']) !!}
                        </div>
                    </div>

                    <div class="col-md-3">
                        {!! Form::label('campaign_id', 'APLICAR A: ', ['class' => ' control-label']) !!}
                        <div class="">
                            {!! Form::select('campaign_id', $campaigns,null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar campaña', 'id'=>'campaign']) !!}
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div id="new_card" class="col-md-12" style="display: none;">
                        <h3>Registrar tarjeta</h3>
                        <p>Automáticamente se guardará la tarjeta y se pondrá como predeterminada.</p>
                        <div class="form-group">
                            {!! Form::label('type', 'TIPO TARJETA : ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-4">
                                {!! Form::select('type',['visa.png'=>'VISA','mastercard.png'=>'MASTERCARD','american-express.png'=>'AMERICAN EXPRESS'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'SELECCIONAR']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('number', 'NUMERO DE TARJETA : ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-4">
                                {!! Form::text('number', null, ['class' => 'form-control only_integer', 'required' => 'required', 'placeholder'=>'4111 1111 1111 1111']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('name', 'NOMBRE SOBRE LA TARJETA : ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-4">
                                {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'NOMBRE SOBRE LA TARJETA', 'style'=>'text-transform:uppercase !important']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('month', 'VENCE : ', ['class' => 'control-label col-md-3']) !!}

                            <div class="col-md-2">
                                {!! Form::select('month',['01'=>'01 Enero', '02'=>'02 Febrero', '03'=>'03 Marzo', '04'=>'04 Abril', '05'=>'05 Mayo', '06'=>'06 Junio', '07'=>'07 Julio', '08'=>'08 Agosto', '09'=>'09 Septiembre', '10'=>'10 Octubre', '11'=>'11 Noviembre', '12'=>'12 Diciembre'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'MES']) !!}
                            </div>
                            <div class="col-md-2">
                                {!! Form::select('year',['2022'=>'2022', '2021'=>'2021', '2020'=>'2020', '2019'=>'2019', '2018'=>'2018', '2017'=>'2017'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'AÑO']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('cvc', 'CVC : ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-1">
                                {!! Form::text('cvc', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'421']) !!}
                            </div>
                        </div>
                    </div>

                    <div id="saved_card" class="col-md-12" style="display: none;">
                        <h3>Tarjetas registradas <button id="addcard" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Crear nueva tarjeta</button></h3>
                        <p>Seleccionar a que tarjeta se utilizará para la transacción.</p>

                        <div class="table-responsive"> {{-- update --}}
                            <table class="table table-striped table-bordered table-hover dataTables"> {{-- update --}}
                                <thead>
                                    <tr>
                                        <th class="text-center">NUMERO</th>
                                        <th class="text-center">NOMBRE SOBRE LA TARJETA</th>
                                        <th class="text-center">VALIDA HASTA</th>
                                        <th class="text-center">POR DEFECTO</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                            <div class="pagination"></div>
                        </div>
                    </div>   
                </div>    

                <div class="row" id="process_button" style="display: none;">
                        <div class="col-sm-2">
                            {!! Form::submit('Procesar donativo', ['class' => 'btn btn-primary form-control create']) !!}
                        </div>
                </div>

            {!! Form::close() !!}

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')
<script>
    $(document).ready(function(){

        $('#donante').change(function(){
            var $value = $(this).val();
            var $url = '{{ url('ControlPanel/user') }}';
            $url = $url + '/' + $value;

            $.getJSON($url,'',function(resp) {

                if(resp.length>0){
                    $('input[name=option]').val('savedcard');
                    $('#new_card').fadeOut('slow',function(){
                        $('#saved_card').fadeIn('slow');
                    });

                    $('tbody tr').remove();

                    $.each(resp,function(key, ccard){

                        $('tbody').append('<tr class="gradeA"><td class="text-center">' + optionBox(ccard.default, ccard.id) + ' &nbsp;&nbsp;<img src="http://portal.fcmujeres.local/img/credit/'+ ccard.ico +'" class="ccard" /> **** **** **** '+ ccard.lastdigit +'</td><td class="text-center">'+ ccard.name +'</td><td class="text-center"> '+ ccard.month+' / '+ ccard.year +' </td><td class="text-center">'+ printvalor(ccard.default) +'</td></tr>');
                    })
                    
                }else{
                    $('#saved_card').fadeOut('slow',function(){
                        $('#new_card').fadeIn('slow');
                    })

                    $('input[name=option]').val('newcard');
                }
                
            });


            $('#process_button').fadeIn('slow');
        });

        $('#currency').change(function(){
            var $value = $(this).val();
            var $url = '{{ url('ControlPanel/amounts') }}';
            $url = $url + '/' + $value;

            $.getJSON($url,'',function(resp) {
                $('#amount_id').empty();
                $('#amount_id').append('<option selected="selected" disabled="disabled" value="" hidden="hidden">Seleccionar monto</option>');
                $.each(resp, function(key, value){
                    $('#amount_id').append('<option value="'+key+'">'+value+'</option>');
                })

            });


            $('#process_button').fadeIn('slow');
        });

        $('#addcard').click(function(e){
            e.preventDefault();
            $('#saved_card').fadeOut('slow',function(){
                $('#new_card').fadeIn('slow');
            })
            $('input[name=option]').val('newcard');
        });

        function printvalor($value){
            if($value){

                return '<i class="fa fa-star"></i>';
            }
            return '';
        };

        function optionBox($value, $id){
            if($value){

                return '<input type="radio" name="ccard_id" value="' + $id + '" checked="true" >';
            }else{
                return '<input type="radio" name="ccard_id" value="' + $id + '">';
            }

        }

        $('input[type=submit]').click(function(e){

            if($( "#donationType option:selected" ).text()!='Seleccionar tipo donación' ){

                if($( "#amount option:selected" ).text()!='Seleccionar monto'){

                    if($( "#campaign option:selected" ).text()!='Seleccionar campaña'){

                        if($('input[name=option]').val()=='savedcard' ){
                            e.preventDefault();
                            $form = $('body').find('form');
                            $form.submit();
                        } /* if($('input[name=option]').val()=='savedcard' ){ */

                    } /* if($( "#campaign option:selected" ).text()!='Seleccionar campaña'){ */

                } /*if($( "#amount option:selected" ).text()!='Seleccionar monto'){*/

            } /*if($( "#donationType option:selected" ).text()!='Seleccionar tipo donación' ){*/

        })

    })

</script>
@endsection