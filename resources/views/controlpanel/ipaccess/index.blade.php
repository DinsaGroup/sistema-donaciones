@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }}</h1>
            <small>Listado de conexiones, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
          
        </section>

        <section class="content">

                <div class="ibox float-e-margins">
                    <div class="ibox-content"> <!-- comienza el contenido de la seccion -->

            
                        <p>Se encontraron {{ count($ips) }} currency registrados en la última consulta.</p>

                        <div class="table-responsive"> {{-- update --}}
                            <table class="table table-striped table-bordered table-hover dataTables"> {{-- update --}}
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">HOST</th>
                                        <th class="text-center">USUARIO</th>
                                        <th class="text-center">ACCEDIO</th>
                                        <th class="text-center">ESTATUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- */$x=0;/* --}}
                                    @foreach($ips as $item)
                                        {{-- */$x++;/* --}}
                                        <tr class="gradeA"> {{-- update --}}
                                            <td class="text-center">{{ $item->id }}</td>
                                            <td class="text-center">{{ $item->ip }}</td>
                                            <td class="text-center">{{ $item->user->name }}</td>
                                            <td class="text-center">{{ $item->created_at }}</td>
                                            <td class="text-center">
                                                @if($item->banned) <spam style="color:red">Blockeado</spam> @else - @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination"></div>
                        </div>

                    </div> <!-- Termina el contenido de la seccion --> 
                </div> <!-- Termina el ibox --> 
            

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection