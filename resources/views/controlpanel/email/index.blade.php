@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
        @if(Auth::user()->can('crear-transaccion'))
          <h1>{{ $title }} <a href="{{ url('ControlPanel/emails/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a></h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
        @else
          <h1>{{ $title }}</h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
        @endif
        </section>

        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-exchange"></i> Historial de emails</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="transaction_history" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th width="50">ID</th>
                        <th>USER</th>
                        <th>SUBJECT</th>
                        <th>MENSAJE</th>
                        <th width="35"></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($emails as $item)
                        <tr>
                          <td class="text-center">{{ $item->id }}</td>
                          <td class="text-center"><img src="/img/user/"{{ $item->user->photo }} class="user-image"> {{ $item->user->name }}</td>
                          <td class="text-center">{{ $item->subject }}</td>
                          <td class="text-center">{{ $item->body }}</td>
                          <td><i class="fa fa-eye"></i></td>
                        </tr>
                      @endforeach
                      
                    </tbody>
                    <tfoot>
                      <tr>
                        <th width="50">ID</th>
                        <th>USER</th>
                        <th>SUBJECT</th>
                        <th>MENSAJE</th>
                        <th></th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                </div> {{-- col-md-12 --}}
            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')
<!-- page script -->
    <script type="text/javascript">
      $(function () {
        // $("#transaction_history").dataTable();
        $('#transaction_history').dataTable({
          "bSortClasses": 'sorting_3',
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "aaSorting": [[0,'desc']],
          "iDisplayLength": 100,
          "aLengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]]
        });
      });
    </script>
@endsection