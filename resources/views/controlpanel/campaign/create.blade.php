@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ url('ControlPanel/campaigns') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

            {!! Form::open(['url' => 'ControlPanel/campaigns', 'class' => 'form-horizontal', 'method'=>'POST']) !!}
            {{ csrf_field() }}

            <div class="">
            
                    <div class="form-group">
                        {!! Form::label('name', 'Nombre de la campaña : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: Mujeres emprendedoras']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'Descripción : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-6">
                            {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Escribir breve descripción de la campaña.']) !!}
                        </div>
                    </div>
                
                    
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>
                </div>

            {!! Form::close() !!}


            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection