@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
        @if(Auth::user()->can('crear-campaña'))
            <h1>{{ $title }} <a href="{{ url('ControlPanel/campaigns/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a></h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
        @else
            <h1>{{ $title }}</h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
        @endif
        </section>

        <section class="content">
            <div class="row">
                
            
                <div class="col-md-12">
                    <div class="box"> <!-- comienza el contenido de la seccion -->
                        <div class="box-header">
                          <h3 class="box-title"><i class="fa fa-bullhorn"></i> Campañas</h3>
                        </div><!-- /.box-header -->
                        
                        <div class="box-body">
                        
                            @if(Auth::user()->can('editar-campaña'))
                            
                                <table id="transaction_history" class="table table-striped table-bordered table-hover dataTables"> {{-- update --}}
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>CAMPAÑA</th>
                                            <th>DESCRIPCIÓN</th>
                                            <th>ESTATUS</th>
                                            <th><i class="fa fa-sort-desc"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- */$x=0;/* --}}
                                        @foreach($campaigns as $item)
                                            {{-- */$x++;/* --}}
                                            <tr class="gradeA"> {{-- update --}}
                                                <td>{{ $item->id }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->description }}</td>
                                                <td>@if($item->active) Activa @else Desactiva @endif</td>
                                                
                                                <td>
                                                    <a href="{{ url('ControlPanel/campaigns/' . $item->id . '/edit') }}" class="details">
                                                        <i class="fa fa-pencil"></i>
                                                    </a> 
                                                    
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>CAMPAÑA</th>
                                            <th>DESCRIPCIÓN</th>
                                            <th>ESTATUS</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            @else
                                <table id="transaction_history" class="table table-striped table-bordered table-hover dataTables"> {{-- update --}}
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>CAMPAÑA</th>
                                            <th>DESCRIPCIÓN</th>
                                            <th>ESTATUS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- */$x=0;/* --}}
                                        @foreach($campaigns as $item)
                                            {{-- */$x++;/* --}}
                                            <tr class="gradeA"> {{-- update --}}
                                                <td>{{ $item->id }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->description }}</td>
                                                <td>@if($item->active) Activa @else Desactiva @endif</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>CAMPAÑA</th>
                                            <th>DESCRIPCIÓN</th>
                                            <th>ESTATUS</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="pagination"></div>
                            @endif
                        </div>
                    </div> <!-- Termina el contenido de la seccion --> 
                </div> <!-- Termina el ibox --> 
            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')
<!-- page script -->
    <script type="text/javascript">
      $(function () {
        // $("#transaction_history").dataTable();
        $('#transaction_history').dataTable({
          "bSortClasses": 'sorting_3',
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "aaSorting": [[0,'desc']],
          "iDisplayLength": 100,
          "aLengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]]
        });
      });
    </script>
@endsection