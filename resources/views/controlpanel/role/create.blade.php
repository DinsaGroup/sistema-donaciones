@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ url('ControlPanel/roles-administrativos') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

            {!! Form::open(['url' => 'ControlPanel/roles-administrativos', 'class' => 'form-horizontal', 'method'=>'POST']) !!}
            {{ csrf_field() }}
            <div class="">
            
            

                    <div class="form-group">
                        {!! Form::label('name', 'Nombre : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'superadmin']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('display_name', 'Alias : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::text('display_name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Super Administrador']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('description', 'Descripción : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-9">
                            {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Escribir breve descripción del rol.']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('acceso', 'Permiso de Acceso : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::select('acceso', [1 => 'Sí', 0 => 'No'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar permiso']) !!}
                        </div>
                    </div>

                    <div class="row">
                        <section class="content-header">
                            <h4>Permisos del Rol</h4>
                            <small>Seleccione los permisos que el rol tendrá, puede escoger más de uno.</small>
                        </section>
                       
                        <?php $pos = 0; ?>

                            <div class="col-md-4"> 
                            @foreach($permissions as $item)
                                
                                @if($pos>$count)
                                    </div>
                                    <div class="col-md-4">
                                    <?php $pos=0; ?>
                                @else
                                    
                                    <?php $pos++; ?>
                                @endif

                                {{ Form::checkbox('perm['.$item->id.']', $item->name, null, ['class' => 'col-md-1']) }}
                                {!! Form::label($item->id, $item->display_name, ['class' => 'col-md-11']) !!}

                            @endforeach
                            </div>
                        
                    </div>
                    
                <div class="form-group">
                    <div class="col-sm-3">
                        {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>
                </div>

            {!! Form::close() !!}

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection