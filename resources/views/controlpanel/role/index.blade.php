@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
        @if(Auth::user()->can('crear-rol'))
            <h1>{{ $title }} <a href="{{ url('ControlPanel/roles-administrativos/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a></h1>
        @else
            <h1>{{ $title }}</h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
        @endif
        </section>

        <section class="content">

                <div class="ibox float-e-margins">
                    <div class="ibox-content"> <!-- comienza el contenido de la seccion -->

            
                        <p>Se encontraron {{ count($roles) }} roles administrativos en la última consulta.</p>

                        @if(Auth::user()->can('editar-rol'))
                            <div class="table-responsive"> {{-- update --}}
                                <table class="table table-striped table-bordered table-hover dataTables"> {{-- update --}}
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>Alias</th>
                                            <th>Descripción</th>
                                            <th>Acceso</th>
                                            <th>Estatus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- */$x=0;/* --}}
                                        @foreach($roles as $item)
                                            {{-- */$x++;/* --}}
                                            <tr class="gradeA"> {{-- update --}}
                                                <td>{{ $item->id }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->display_name }}</td>
                                                <td>{{ $item->description }}</td>
                                                @if($item->acceso == 1)
                                                <td>Sí</td>
                                                @else
                                                <td>No</td>
                                                @endif
                                                <td>
                                                    <a href="{{ url('ControlPanel/roles-administrativos/' . $item->id . '/edit') }}" class="details">
                                                        <i class="fa fa-pencil"></i>
                                                    </a> 
                                                    
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination"></div>
                            </div>
                        @else
                            <div class="table-responsive"> {{-- update --}}
                                <table class="table table-striped table-bordered table-hover dataTables"> {{-- update --}}
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>Alias</th>
                                            <th>Descripción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- */$x=0;/* --}}
                                        @foreach($roles as $item)
                                            {{-- */$x++;/* --}}
                                            <tr class="gradeA"> {{-- update --}}
                                                <td>{{ $item->id }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->display_name }}</td>
                                                <td>{{ $item->description }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination"></div>
                            </div>
                        @endif
                    </div> <!-- Termina el contenido de la seccion --> 
                </div> <!-- Termina el ibox --> 
        </section>
    </div>
      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection