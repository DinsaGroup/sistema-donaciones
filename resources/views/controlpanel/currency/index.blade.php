@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ url('ControlPanel/monedas/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a></h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
          
        </section>

        <section class="content">

                <div class="ibox float-e-margins">
                    <div class="ibox-content"> <!-- comienza el contenido de la seccion -->

            
                        <p>Se encontraron {{ count($currencies) }} currency registrados en la última consulta.</p>

                        <div class="table-responsive"> {{-- update --}}
                            <table class="table table-striped table-bordered table-hover dataTables"> {{-- update --}}
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">SIMBOLO</th>
                                        <th class="text-center">CÓDIGO</th>
                                        <th class="text-center">DENOMINACIÓN</th>
                                        <th class="text-center">DESCRIPCIÓN</th>
                                        <th class="text-center"><i class="fa fa-sort-desc"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- */$x=0;/* --}}
                                    @foreach($currencies as $item)
                                        {{-- */$x++;/* --}}
                                        <tr class="gradeA"> {{-- update --}}
                                            <td class="text-center">{{ $item->id }}</td>
                                            <td class="text-center">{{ $item->symbol }}</td>
                                            <td class="text-center">{{ $item->symbol2 }}</td>
                                            <td class="text-center">{{ $item->name }}</td>
                                            <td class="text-center">{{ $item->description }}</td>
                                            
                                            <td  class="text-center">
                                                <a href="{{ url('ControlPanel/monedas/' . $item->id . '/edit') }}" class="details">
                                                    <i class="fa fa-pencil"></i>
                                                </a> 
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination"></div>
                        </div>

                    </div> <!-- Termina el contenido de la seccion --> 
                </div> <!-- Termina el ibox --> 
            

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection