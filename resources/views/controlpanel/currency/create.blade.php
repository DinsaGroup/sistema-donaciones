@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ url('ControlPanel/monedas') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

            {!! Form::open(['url' => 'ControlPanel/monedas', 'class' => 'form-horizontal', 'method'=>'POST']) !!}
            {{ csrf_field() }}
            <div class="col-xs-12">

                    <div class="form-group">
                        {!! Form::label('symbol', 'SIMBOLO : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::text('symbol', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: U$, C$, €, L$']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('symbol2', 'CÓDIGO : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::text('symbol2', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: USD, NIC, EUR']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('name', 'NOMBRE : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: DOLARES, CORDOBAS']) !!}
                        </div>
                    </div>

                     <div class="form-group">
                        {!! Form::label('description', 'DESCRIPCIÓN : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Dolares estadounidences']) !!}
                        </div>
                    </div>
                    
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>
                </div>

            {!! Form::close() !!}

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection