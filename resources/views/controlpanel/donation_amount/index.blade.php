@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
        @if(Auth::user()->can('crear-monto-donación'))
            <h1>{{ $title }} <a href="{{ url('ControlPanel/monto-donaciones/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a></h1>
            <small>Listado actualizado al {{ date('j.m.o h:i:s A') }}</small>
        @else
            <h1>{{ $title }}</h1>
            <small>Listado actualizado al {{ date('j.m.o h:i:s A') }}</small>
        @endif          
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box"> <!-- comienza el contenido de la seccion -->

                        <div class="box-header">
                          <h3 class="box-title"><i class="fa fa-money"></i> Listado de monto donaciones</h3>
                        </div><!-- /.box-header -->
                        
                        <div class="box-body">
                            @if(Auth::user()->can('editar-monto-donación'))
                            
                                <table id="transaction_history" class="table table-striped table-bordered table-hover dataTables"> {{-- update --}}
                                    <thead>
                                        <tr>
                                            <th class="text-center">ID</th>
                                            <th class="text-center">SIMBOLO</th>
                                            <th class="text-center">CANTIDAD</th>
                                            <th class="text-left">DESCRIPCIÓN</th>
                                            <th class="text-center"><i class="fa fa-sort-desc"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- */$x=0;/* --}}
                                        @foreach($donationAmounts as $item)
                                            {{-- */$x++;/* --}}
                                            <tr class="gradeA"> {{-- update --}}
                                                <td class="text-center">{{ $item->id }}</td>
                                                <td class="text-center">{{ $item->currency->symbol }}</td>
                                                <td class="text-right">{{ number_format($item->amount,2,'.',',') }} {{ $item->currency->symbol2 }}</td>
                                                <td class="">{{ $item->currency->description }}</td>
                                                <td  class="text-center">
                                                    <a href="{{ url('ControlPanel/monto-donaciones/' . $item->id . '/edit') }}" class="details">
                                                        <i class="fa fa-pencil"></i>
                                                    </a> 
                                                    
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination"></div>
                            @else
                            
                                <table id="transaction_history" class="table table-striped table-bordered table-hover dataTables"> {{-- update --}}
                                    <thead>
                                        <tr>
                                            <th class="text-center">ID</th>
                                            <th class="text-center">SIMBOLO</th>
                                            <th class="text-center">CANTIDAD</th>
                                            <th class="text-left">DESCRIPCIÓN</th>
                                            {{-- <th class="text-center"><i class="fa fa-sort-desc"></i></th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- */$x=0;/* --}}
                                        @foreach($donationAmounts as $item)
                                            {{-- */$x++;/* --}}
                                            <tr class="gradeA"> {{-- update --}}
                                                <td class="text-center">{{ $item->id }}</td>
                                                <td class="text-center">{{ $item->currency->symbol }}</td>
                                                <td class="text-right">{{ number_format($item->amount,2,'.',',') }} {{ $item->currency->symbol2 }}</td>
                                                <td class="">{{ $item->currency->description }}</td>
                                                {{-- <td  class="text-center">
                                                    <a href="{{ url('ControlPanel/monto-donaciones/' . $item->id . '/edit') }}" class="details">
                                                        <i class="fa fa-pencil"></i>
                                                    </a> 
                                                    
                                                </td> --}}
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination"></div>
                            @endif
                        </div>
                    </div> <!-- Termina el contenido de la seccion --> 
                </div> <!-- Termina el ibox --> 
            </div>
        </section>

    </div>

<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')
<!-- page script -->
    <script type="text/javascript">
      $(function () {
        // $("#transaction_history").dataTable();
        $('#transaction_history').dataTable({
          "bSortClasses": 'sorting_3',
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "aaSorting": [[0,'desc']],
          "iDisplayLength": 100,
          "aLengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]]
        });
      });
    </script>
@endsection