@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
                

            <h1>{{ $title }} <a href="{{ url('ControlPanel/monto-donaciones') }}" class="btn btn-danger pull-right btn-sm"><i class="fa fa-times"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

            {!! Form::model($donationAmount, [
                    'method' => 'PATCH',
                    'url' => ['ControlPanel/monto-donaciones', $donationAmount->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}
                {{ csrf_field() }}
            <div class="col-xs-12">
            
                    <<div class="form-group">
                        {!! Form::label('currency_id', 'MONEDA : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-2">
                            {!! Form::select('currency_id', $currencies, null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'SELECCIONAR MONEDA']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('amount', 'CANTIDAD : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-2">
                            {!! Form::text('amount', null, ['class' => 'form-control only_numbers', 'required' => 'required', 'placeholder'=>'Ej: Descripción del periodo']) !!}
                        </div>
                    </div>
                
                    
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>
                </div>

            {!! Form::close() !!}

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection