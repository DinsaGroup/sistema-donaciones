@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
        @if(Auth::user()->can('crear-administrador'))
            <h1>{{ $title }} <a href="{{ url('ControlPanel/administradores/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a></h1>
        @else
            <h1>{{ $title }} <a href="{{ url('ControlPanel/administradores/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a></h1>
        @endif
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
          
        </section>

        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-users"></i> Listado de donantes</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="transaction_history" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>País</th>
                            <th>Casa</th>
                            <th>Oficina</th>
                            <th>Celular</th>
                            <th>Tipo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                      @if(Auth::user()->can('editar-super-administrador'))
                                    @foreach($users as $item)
                                        <tr class="gradeA"> 
                                            <td>{{ $item->id }}</td>
                                            <td><img src="{{ url('img/user/'. $item->photo) }}" class="user-image"/> {{ $item->name }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ $item->country->name }} + ( {{ $item->country->ext }} )</td>
                                            <td>{{ $item->phone }}</td>
                                            <td>{{ $item->office }}</td>
                                            <td>{{ $item->cell }}</td>
                                            <td>{{ array_get($item->role,'0.name') }}</td>
                                            <td>
                                                <a href="{{ url('ControlPanel/administradores/' . $item->id . '/edit') }}" class="details">
                                                    <i class="fa fa-pencil"></i>
                                                </a> 
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    @foreach($users as $item)
                                        @if($item->hasRole('superadmin'))
                                            <tr class="gradeA"> 
                                                <td>{{ $item->id }}</td>
                                                <td><img src="{{ url('img/user/'. $item->photo) }}" class="user-image"/> {{ $item->name }}</td>
                                                <td>{{ $item->email }}</td>
                                                <td>{{ $item->country->name }} + ( {{ $item->country->ext }} )</td>
                                                <td>{{ $item->phone }}</td>
                                                <td>{{ $item->office }}</td>
                                                <td>{{ $item->cell }}</td>
                                                <td>{{ array_get($item->role,'0.name') }}</td>
                                                {{-- <td>
                                                    <a href="{{ url('ControlPanel/administradores/' . $item->id . '/edit') }}" class="details">
                                                        <i class="fa fa-pencil"></i>
                                                    </a> 
                                                    
                                                </td> --}}
                                            </tr>
                                        @else
                                            <tr class="gradeA"> 
                                                <td>{{ $item->id }}</td>
                                                <td><img src="{{ url('img/user/'. $item->photo) }}" class="user-image"/> {{ $item->name }}</td>
                                                <td>{{ $item->email }}</td>
                                                <td>{{ $item->country->name }} + ( {{ $item->country->ext }} )</td>
                                                <td>{{ $item->phone }}</td>
                                                <td>{{ $item->office }}</td>
                                                <td>{{ $item->cell }}</td>
                                                <td>{{ array_get($item->role,'0.name') }}</td>
                                                <td>
                                                    <a href="{{ url('ControlPanel/administradores/' . $item->id . '/edit') }}" class="details">
                                                        <i class="fa fa-pencil"></i>
                                                    </a> 
                                                    
                                                </td>
                                            </tr>
                                        
                                        @endif
                                    @endforeach
                                @endif
                      
                    </tbody>
                    <tfoot>
                      <tr>
                          <th>ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>País</th>
                            <th>Casa</th>
                            <th>Oficina</th>
                            <th>Celular</th>
                            <th>Tipo</th>
                            <th></th>
                        </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                </div> {{-- col-md-12 --}}
            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')
<!-- page script -->
    <script type="text/javascript">
      $(function () {
        $("#transaction_history").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
    </script>
@endsection