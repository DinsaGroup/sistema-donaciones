<header class="main-header">
  <!-- Logo -->
  <a href="{{url('ControlPanel')}}" class="logo">Sistema <b>Donación</b></a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        {{-- <li class="dropdown messages-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-envelope-o"></i>
            <span class="label label-success">4</span>
          </a>
          <ul class="dropdown-menu">
            <li class="header">You have 4 messages</li>
            <li>
              <!-- inner menu: contains the actual data -->
              <ul class="menu">
                <li><!-- start message -->
                  <a href="#">
                    <div class="pull-left">
                      <img src="img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
                    </div>
                    <h4>
                      Support Team
                      <small><i class="fa fa-clock-o"></i> 5 mins</small>
                    </h4>
                    <p>Why not buy a new awesome theme?</p>
                  </a>
                </li><!-- end message -->
                <li>
                  <a href="#">
                    <div class="pull-left">
                      <img src="img/user3-128x128.jpg" class="img-circle" alt="user image"/>
                    </div>
                    <h4>
                      AdminLTE Design Team
                      <small><i class="fa fa-clock-o"></i> 2 hours</small>
                    </h4>
                    <p>Why not buy a new awesome theme?</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div class="pull-left">
                      <img src="img/user4-128x128.jpg" class="img-circle" alt="user image"/>
                    </div>
                    <h4>
                      Developers
                      <small><i class="fa fa-clock-o"></i> Today</small>
                    </h4>
                    <p>Why not buy a new awesome theme?</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div class="pull-left">
                      <img src="img/user3-128x128.jpg" class="img-circle" alt="user image"/>
                    </div>
                    <h4>
                      Sales Department
                      <small><i class="fa fa-clock-o"></i> Yesterday</small>
                    </h4>
                    <p>Why not buy a new awesome theme?</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div class="pull-left">
                      <img src="img/user4-128x128.jpg" class="img-circle" alt="user image"/>
                    </div>
                    <h4>
                      Reviewers
                      <small><i class="fa fa-clock-o"></i> 2 days</small>
                    </h4>
                    <p>Why not buy a new awesome theme?</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="footer"><a href="#">See All Messages</a></li>
          </ul>
        </li> --}}
        <!-- Notifications: style can be found in dropdown.less -->
        {{-- <li class="dropdown notifications-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bell-o"></i>
            <span class="label label-warning">10</span>
          </a>
          <ul class="dropdown-menu">
            <li class="header">You have 10 notifications</li>
            <li>
              <!-- inner menu: contains the actual data -->
              <ul class="menu">
                <li>
                  <a href="#">
                    <i class="fa fa-users text-aqua"></i> 5 new members joined today
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fa fa-users text-red"></i> 5 new members joined
                  </a>
                </li>

                <li>
                  <a href="#">
                    <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fa fa-user text-red"></i> You changed your username
                  </a>
                </li>
              </ul>
            </li>
            <li class="footer"><a href="#">View all</a></li>
          </ul>
        </li> --}}
        <!-- Tasks: style can be found in dropdown.less -->
         <li class="dropdown tasks-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-plus"></i>
            
          </a>
          <ul class="dropdown-menu">
            <li class="header">Acciones directas</li>
            <li>
              <!-- inner menu: contains the actual data -->
              <ul class="menu">
                <li><a href="{{ url('ControlPanel/transacciones/create') }}"><i class="fa fa-exchange" style="margin-right:10px"></i> Realizar un donativo</a></li>
                <li><a href="{{ url('ControlPanel/donantes/create') }}"><i class="fa fa-user-plus" style="margin-right:10px"></i> Crear nuevo donante</a></li>
                <li><a href="{{ url('ControlPanel/tarjetas/create') }}"><i class="fa fa-cc" style="margin-right:10px"></i> Agregar nueva tarjeta</a></li>
                <li><a href="{{ url('ControlPanel/campaigns/create') }}"><i class="fa fa-bullhorn" style="margin-right:10px"></i> Crear una campaña</a></li>
              </ul>
            </li>
          </ul>
        </li> 
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{ url('img/user/'. Auth::user()->photo ) }}" class="user-image" alt="User Image"/>
            <span class="hidden-xs">{{ Auth::user()->name }}</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="{{ url('img/user/'. Auth::user()->photo ) }}" class="img-circle" alt="User Image" />
              <p>
                {{ Auth::user()->name }} 
                <small>{{ array_get(Auth::user()->roles,'0.display_name') }}</small>
              </p>
            </li>
            <!-- Menu Body -->
            {{-- <li class="user-body">
              <div class="col-xs-4 text-center">
                <a href="#">Followers</a>
              </div>
              <div class="col-xs-4 text-center">
                <a href="#">Sales</a>
              </div>
              <div class="col-xs-4 text-center">
                <a href="#">Friends</a>
              </div>
            </li> --}}
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="{{ url('ControlPanel/administradores/'.Auth::user()->id.'/edit') }}" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                {{-- <a href="#" class="btn btn-default btn-flat">Sign out</a> --}}
                <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i> Logout
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>