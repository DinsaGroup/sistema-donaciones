@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">

        <h1>
          {{ $title }} 
          @if(Auth::user()->can('crear-donación'))
            <a href="{{ url('ControlPanel/donantes/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-user-plus" style="margin-right: 10px;"></i> Nuevo donante</a>
          @endif
          @if(Auth::user()->can('crear-donante'))
            <a href="{{ url('ControlPanel/transacciones/create') }}" class="btn btn-primary pull-right btn-sm" style="margin-right: 20px"><i class="fa fa-exchange" style="margin-right: 10px;"></i> Nueva donación</a>
          @endif
          {{-- @if(Auth::user()->can('procesar-tarjetas')) --}}
            <a href="{{ url('ControlPanel/processcards') }}" class="btn btn-primary pull-right btn-sm" style="margin-right: 20px"><i class="fa fa-credit-card" style="margin-right: 10px;"></i> Procesar tarjetas</a>
          {{-- @endif --}}
        </h1>
        <small>Listado de donaciones agendadas, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
          
        </section>

        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-calendar"></i> Historial</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="transaction_history" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        {{-- <th>ID</th> --}}
                        <th>DONANTE</th>
                        {{-- <th>APLICAR A</th> --}}
                        <th>TARJETA</th>
                        <th>MONTO U$</th>
                        <th>DONACION</th>
                        {{-- <th>DESDE</th> --}}
                        <th>ULT. COBRO</th>
                        <th>PROX. COBRO</th>
                        <th>ESTATUS</th>  
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($schedules as $item)
                        <tr>
                          {{-- <td><a href="{{ url('ControlPanel/calendario/'.$item->id.'/edit') }}">{{ $item->id }}</a></td> --}}
                          <td>
                            <a href="{{ url('ControlPanel/donantes/'.$item->creditcard->user->id) }}" >
                              <img src="{{ url('img/user/'.$item->creditcard->user->photo )}}" class="user-image"> {{ $item->creditcard->user->name }}
                            </a>
                          </td>
                          {{-- <td><a href="{{ url('ControlPanel/calendario/'.$item->id.'/edit') }}">{{ $item->campaign->name }}</a></td> --}}
                          <td class="text-center">
                            <a href="{{ url('ControlPanel/calendario/'.$item->id.'/edit') }}"><img src="{{ url('img/credit/'. $item->creditcard->ico) }}" class="ccard"> **** {{ $item->creditcard->lastdigit }}</a>
                          </td>
                          <td class="text-center" ><a href="{{ url('ControlPanel/calendario/'.$item->id.'/edit') }}">U$ {{ number_format($item->amount,2,'.',',') }}</a></td>
                          <td class="text-center"><a href="{{ url('ControlPanel/calendario/'.$item->id.'/edit') }}">{{ $item->donationtype->name  }}</a></td>
                          {{-- <td class="text-center"><a href="{{ url('ControlPanel/calendario/'.$item->id.'/edit') }}">{{ date('d/m/Y', strtotime($item->created_at)) }}</a></td> --}}
                          <td class="text-center"><a href="{{ url('ControlPanel/calendario/'.$item->id.'/edit') }}">{{ date('d/m/Y', strtotime($item->lastProcess)) }}</a></td>
                          <td class="text-center"><a href="{{ url('ControlPanel/calendario/'.$item->id.'/edit') }}">{{ date('d/m/Y', strtotime($item->nextProcess)) }}</a></td>
                          <td class="text-center"><a href="{{ url('ControlPanel/calendario/'.$item->id.'/edit') }}">{{ $item->status }}</a></td>
                          
                        </tr>
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        {{-- <th>ID</th> --}}
                        <th>DONANTE</th>
                        {{-- <th>APLICAR A</th> --}}
                        <th>TARJETA</th>
                        <th>MONTO U$</th>
                        <th>DONACION</th>
                        {{-- <th>DESDE</th> --}}
                        <th>ULT. COBRO</th>
                        <th>PROX. COBRO</th>
                        <th>ESTATUS</th>
                        
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                </div> {{-- col-md-12 --}}
            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')
<!-- page script -->
    <script type="text/javascript">
      $(function () {
        // $("#transaction_history").dataTable();
        $('#transaction_history').dataTable({
          "bSortClasses": 'sorting_3',
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "aaSorting": [[0,'desc']],
          "iDisplayLength": 100,
          "aLengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]]
        });
      });
    </script>
@endsection