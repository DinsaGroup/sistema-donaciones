@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ url('ControlPanel/calendario') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

            {!! Form::open(['url' => 'ControlPanel/calendario', 'class' => 'form-horizontal', 'method'=>'POST']) !!}
            {{ csrf_field() }}

            <div class="">

                <div class="form-group">
                    {!! Form::label('creditcard_id', 'Numero de tarjeta : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-4">
                        {!! Form::select('creditcard_id',$creditcards, null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar tarjeta']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('donationType_id', 'TIPO DE DONACIÓN : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-4">
                        {!! Form::select('donationType_id',$donationTypes, null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar tipo donación', ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('donationType_id', 'MONEDA : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-4">
                        {!! Form::select('currency',$currencies, null, [ 'class'=>'form-control', 'required'=>'required', 'placeholder'=>'Seleccionar moneda', 'id'=>'currency']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('amount_id', 'MONTO A DONAR: ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-4">
                        {!! Form::select('amount_id',[] ,null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar monto', 'id'=>'amount_id']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('campaign_id', 'APLICAR A: ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-4">
                        {!! Form::select('campaign_id', $campaigns,null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar campaña', 'id'=>'campaign']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('debit_at', 'DEBITAR DESDE: ', ['class' => ' control-label col-md-3']) !!}
                    <div class="input-group date col-sm-2 no-padding" style="margin-left: 5px; float: left;">
                        <span class="input-group-addon"><i class="fa fa-calendar" style="color: black"></i></span>
                        <input id="debit_at" type="text" class="form-control" name="debit_at" placeholder="Comenzar debito" required="required">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>
                </div>
                
            </div>

            {!! Form::close() !!}


            </div>

        </section>

    </div>


@endsection

@section('javascript')
<script src="{{ url('js/backend/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script>
        $(document).ready(function(){

            $('#debit_at').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                format: 'yyyy/mm/dd',
                autoclose: true
            });

            $('#currency').change(function(){
            var $value = $(this).val();
            var $url = '{{ url('ControlPanel/amounts') }}';
            $url = $url + '/' + $value;

            $.getJSON($url,'',function(resp) {
                $('#amount_id').empty();
                $('#amount_id').append('<option selected="selected" disabled="disabled" value="" hidden="hidden">Seleccionar monto</option>');
                $.each(resp, function(key, value){
                    $('#amount_id').append('<option value="'+key+'">'+value+'</option>');
                })

            });
            
            });
        });
    </script>
@endsection