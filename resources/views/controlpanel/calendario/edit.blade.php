@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
       

            <h1>{{ $title }} <a href="{{ url('ControlPanel/calendario') }}" class="btn btn-danger pull-right btn-sm"><i class="fa fa-times"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

            {!! Form::model($schedule, [
                    'method' => 'PATCH',
                    'url' => ['ControlPanel/calendario', $schedule->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}
                {{ csrf_field() }}
            <div class="">
            
                <div class="form-group">
                    {!! Form::label('name', 'Donante : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-4">
                        {!! Form::text('name', $schedule->creditcard->user->name, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: Ana Carolina Baez', 'readonly'=>'readonly']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('creditcard_id', 'Tarjeta : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-4">
                        {{-- <img src="{{ url('img/credit/'. $schedule->creditcard->ico) }}" class="ccard"> **** **** **** {{ $schedule->creditcard->lastdigit }}  --}}
                        {!! Form::select('creditcard_id', $creditcards ,null, ['class' => 'form-control', 'placeholder'=>'Seleccionar donativo']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('ccard', 'Donativo actual : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-4">
                        U$ {{ number_format($schedule->amount,2,'.',',') }} <a href="#" class="change" style="margin-left: 10px"><i class="fa fa-refresh"> Modificar donativo</i></a>
                    </div>
                </div>

                <div id="amount" class="form-group" style="display: none">
                    {!! Form::label('amount_id', 'Cambiar donativo : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-4">
                        {!! Form::select('amount_id', $donationAmounts ,null, ['class' => 'form-control', 'placeholder'=>'Seleccionar donativo']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('donation_type_id', 'Seleccion frecuencia: ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-4">
                        {!! Form::select('donation_type_id', $donationTypes ,null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar donativo']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('campaign_id', 'Seleccion Campaña: ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-4">
                        {!! Form::select('campaign_id', $campaigns ,null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar donativo']) !!}
                    </div>
                </div>


                <div class="form-group">
                    {!! Form::label('house', 'Activo desde : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-2">
                        {{ $schedule->created_at }}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('house', 'Último intento : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-2">
                        {{ $schedule->lastProcess }}
                    </div>
                </div>

                {{-- <div class="form-group">
                    {!! Form::label('house', 'Próximo intento programado : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-2">
                        {{ $schedule->nextProcess }}
                    </div>
                </div> --}}

                <div class="form-group">
                    {!! Form::label('nextProcess', 'Proximo intento programado : ', ['class' => 'control-label col-md-3']) !!}
                    <div class="col-md-3">
                        {!! Form::text('nextProcess', null, ['class' => 'form-control datepicker', 'required' => 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('house', 'Intentos fallidos : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-2">
                        {{ $schedule->failedAttempts }}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('status', 'Estatus : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-2">
                        {!! Form::select('status', ['Activa'=>'Activa','Tarjeta Vencida'=>'Tarjeta Vencida', 'Desactiva'=>'Desactiva'] ,null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar opción']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('comments', 'Resultado última transacción : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-4">
                        {!! Form::textarea('comments', null, ['class' => 'form-control', 'rows'=>3, 'readonly']) !!}
                    </div>
                </div>
                    
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>
                </div>

                {!! Form::close() !!}

            </div>

        </section>

    </div>


@endsection

@section('javascript')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.change').click(function(){
                // $e.preventdefault();
                $('#amount').fadeIn('slow',function(){
                    $('.change').fadeOut();  
                });
            })

            $('.datepicker').datepicker({
                autoclose:true,
                dateFormat: 'yy-mm-dd',
            })
        });
    </script>
@endsection