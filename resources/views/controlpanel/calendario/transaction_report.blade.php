@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">

        <h1>
          {{ $title }} 
         {{--  @if(Auth::user()->can('crear-donación'))
            <a href="{{ url('ControlPanel/donantes/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-print" style="margin-right: 10px;"></i> Imprimir</a>
          @endif
          @if(Auth::user()->can('crear-donante'))
            <a href="{{ url('ControlPanel/transacciones/create') }}" class="btn btn-primary pull-right btn-sm" style="margin-right: 20px"><i class="fa fa-file-pdf-o" style="margin-right: 10px;"></i> Export</a>
          @endif --}}
          
        </h1>
        <small>Listado de donaciones agendadas, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
          
        </section>

        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-calendar"></i> Historial</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="transaction_history" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>INVOICE</th>
                        <th>TRANS. NO.</th>
                        <th>TARJETA</th>
                        <th>APLICAR A</th>
                        <th>MONTO U$</th>
                        <th>ESTATUS</th>
                        <th>COMENTARIO</th> 
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($transactions as $item)
                        <tr>
                          <td class="text-center">
                            <a target="_blank" href="{{ url('ControlPanel/transacciones/'.$item->id) }}">{{ $item->id }}</a>
                          </td>
                          <td class="text-center">
                            <a target="_blank" href="{{ url('ControlPanel/transacciones/'.$item->id) }}">{{ $item->transactionid }}</a>
                          </td>
                          
                          <td class="text-center">
                            <a target="_blank" href="{{ url('ControlPanel/transacciones/'.$item->id) }}"><img src="{{ url('img/credit/'. $item->creditcard->ico) }}" class="ccard"> **** {{ $item->creditcard->lastdigit }}</a>
                          </td>

                          <td class="text-center">
                            <a target="_blank" href="{{ url('ControlPanel/transacciones/'.$item->id) }}">{{ $item->campaign->name }}</a>
                          </td>

                          <td class="text-center" >
                            <a target="_blank" href="{{ url('ControlPanel/transacciones/'.$item->id) }}">U$ {{ number_format($item->amount,2,'.',',') }}</a>
                          </td>

                          <td>
                            <a target="_blank" href="{{ url('ControlPanel/transacciones/'.$item->id) }}">{{ $item->status }} </a>
                          </td>
                           <td>
                            <a target="_blank" href="{{ url('ControlPanel/transacciones/'.$item->id) }}">{{ $item->comments }}</a>
                          </td>

                        </tr>
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>TRANS. NO.</th>
                        <th>TARJETA</th>
                        <th>APLICAR A</th>
                        <th>MONTO U$</th>
                        <th>RESULTADO</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                </div> {{-- col-md-12 --}}
            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')
<!-- page script -->
    <script type="text/javascript">
      $(function () {
        // $("#transaction_history").dataTable();
        $('#transaction_history').dataTable({
          "bSortClasses": 'sorting_3',
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "aaSorting": [[0,'asc']],
          "iDisplayLength": 100,
          "aLengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]]
        });
      });
    </script>
@endsection