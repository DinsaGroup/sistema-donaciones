@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')



    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }}</h1>
            <small>Listado de reportes, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
          
        </section>

        <section class="content">

                <div class="ibox float-e-margins">
                    <div class="ibox-content"> <!-- comienza el contenido de la seccion -->

            
                        <p>Seleccione el reporte que desea generar.</p>

                        <div class="table-responsive"> 
                            <table class="table table-striped table-bordered table-hover dataTables"> {{-- update --}}
                                <thead>
                                    <tr>
                                        <th class="text-left">REPORTES DISPONIBLES</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {{-- Reporte de un donante específico --}}
                                    <tr class="gradeA"> {{-- update --}}
                                        <td>
                                            <a href="{{ url('ControlPanel/reporte/donante/pdf') }}" class="action" data-toggle="modal" data-target="#donante" >Reporte de un donante específico</a>
                                        </td>    
                                        <td class="text-right">
                                            <a href="{{ url('ControlPanel/reporte/donante/pdf') }}" class="action" data-toggle="modal" data-target="#donante"><i class="fa fa-file-pdf-o" style="margin:0 5px 0 15px;"></i> PDF </a>
                                            {{-- <a href="{{ url('ControlPanel/reporte/donante/download') }}" class="action" data-toggle="modal" data-target="#donante"><i class="fa fa-download" style="margin: 0 5px 0 15px;"></i> Descargar</a> --}}
                                        </td>  
                                    </tr>

                                    {{-- Listado de donantes --}}
                                    <tr class="gradeA"> {{-- update --}}
                                        <td>
                                            <a href="{{ url('ControlPanel/reporte/donantes') }}" target="_blank">Listado de donantes</a>
                                        </td>    
                                        <td class="text-right">
                                            <a href="{{ url('ControlPanel/reporte/donantes') }}" target="_blank"><i class="fa fa-file-pdf-o" style="margin:0 5px 0 15px;"></i> PDF </a>
                                            {{-- <a href="{{ url('ControlPanel/reporte/donantes/descargar') }}" target="_blank"><i class="fa fa-download" style="margin:0 5px 0 15px;"></i> Descargar</a> --}}
                                        </td>  
                                    </tr>

                                    {{-- Listado de donaciones realizadas --}}
                                    <tr class="gradeA"> {{-- update --}}
                                        <td>
                                            <a href="{{ url('ControlPanel/reporte/donaciones') }}" class="action" data-toggle="modal" data-target="#donaciones">
                                                Listado de donaciones realizadas
                                            </a>
                                        </td>    
                                        <td class="text-right">
                                            <a href="{{ url('ControlPanel/reporte/pdf/donantes') }}"><i class="fa fa-file-pdf-o" style="margin:0 5px 0 15px;"></i> PDF </a>
                                            {{-- <a href="{{ url('ControlPanel/reporte/descargar/donantes') }}"><i class="fa fa-download" style="margin:0 5px 0 15px;"></i> Descargar</a> --}}
                                        </td>  
                                    </tr>

                                    {{-- Comparativo de donaciones por genero, campaña, edad y país --}}
                                    {{-- <tr class="gradeA">
                                        <td>
                                            Comparativo de donaciones por genero, campaña, edad y país
                                        </td>    
                                        <td class="text-right">
                                            <a href="{{ url('ControlPanel/reporte/pdf/donantes') }}"><i class="fa fa-file-pdf-o" style="margin:0 5px 0 15px;"></i> PDF </a>
                                        </td>  
                                    </tr> --}}

                                    {{-- Listado de tarjetas en el sistema --}}
                                    <tr class="gradeA"> {{-- update --}}
                                        <td>
                                            <a href="{{ url('ControlPanel/reporte/tarjetas') }}" target="_blank">Listado de tarjetas en el sistema</a>
                                        </td>    
                                        <td class="text-right">
                                            <a href="{{ url('ControlPanel/reporte/pdf/tarjetas') }}"><i class="fa fa-file-pdf-o" style="margin:0 5px 0 15px;"></i> PDF </a>
                                            {{-- <a href="{{ url('ControlPanel/reporte/descargar/tarjetas') }}"><i class="fa fa-download" style="margin:0 5px 0 15px;"></i> Descargar</a> --}}
                                        </td>  
                                    </tr>

                                    {{-- Listado de todas las transacciones realizadas --}}
                                    <tr class="gradeA"> {{-- update --}}
                                        <td>
                                            <a href="{{ url('ControlPanel/reporte/transacciones') }}" class="action" data-toggle="modal" data-target="#transacciones">
                                            Listado de todas las transacciones realizadas
                                            </a>
                                        </td>    
                                        <td class="text-right">
                                            <a href="{{ url('ControlPanel/reporte/pdf/donantes') }}"><i class="fa fa-file-pdf-o" style="margin:0 5px 0 15px;"></i> PDF </a>
                                            {{-- <a href="{{ url('ControlPanel/reporte/descargar/donantes') }}"><i class="fa fa-download" style="margin:0 5px 0 15px;"></i> Descargar</a> --}}
                                        </td>  
                                    </tr>

                                    {{-- Listado de todas las transacciones aprobadas --}}
                                    {{-- <tr class="gradeA"> 
                                        <td>
                                            Listado de todas las transacciones aprobadas
                                        </td>    
                                        <td class="text-right">
                                            <a href="{{ url('ControlPanel/reporte/pdf/donantes') }}"><i class="fa fa-file-pdf-o" style="margin:0 5px 0 15px;"></i> PDF </a>
                                        </td>  
                                    </tr> --}}

                                    {{-- Listado de todas las transacciones rechazadas --}}
                                    {{-- <tr class="gradeA"> 
                                        <td>
                                            Listado de todas las transacciones rechazadas
                                        </td>    
                                        <td class="text-right">
                                            <a href="{{ url('ControlPanel/reporte/pdf/donantes') }}"><i class="fa fa-file-pdf-o" style="margin:0 5px 0 15px;"></i> PDF </a>
                                        </td>  
                                    </tr> --}}

                                    {{-- Listado de las campañas con los montos recaudados --}}
                                    {{-- <tr class="gradeA"> 
                                        <td>
                                            Listado de las campañas con los montos recaudados
                                        </td>    
                                        <td class="text-right">
                                            <a href="{{ url('ControlPanel/reporte/pdf/donantes') }}"><i class="fa fa-file-pdf-o" style="margin:0 5px 0 15px;"></i> PDF </a>
                                        </td>  
                                    </tr> --}}

                                    
                                </tbody>
                            </table>
                            <div class="pagination"></div>
                        </div>

                    </div> <!-- Termina el contenido de la seccion --> 
                </div> <!-- Termina el ibox --> 
            

        </section>

    </div>


    <!-- Modals -->

    <!-- Modal DONANTE -->
    <div id="donante" class="modal fade" role="dialog">

        {!! Form::open(['url' => 'ControlPanel/reportes', 'class' => 'form-horizontal', 'method'=>'POST', 'target'=>'_blank']) !!}
        {{ csrf_field() }}

        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reporte de un donante especifico</h4>
                  </div>
                  <div class="modal-body">  
                        
                    {!! Form::label('donante_id', 'SELECIONAR DONANTE : ', ['class' => ' control-label']) !!}

                    <div class="">
                        {!! Form::select('donante_id',$donantes, null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar donante', 'id'=>'donante']) !!}
                    </div>

                  </div>
                  <div class="modal-footer">
                    {!! Form::submit('Procesar solicitud', ['class' => 'btn btn-primary form-control create']) !!}
                  </div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>

    <!-- Modal DONACIONES -->
    <div id="donaciones" class="modal fade" role="dialog">

        {!! Form::open(['url' => 'ControlPanel/donaciones', 'class' => 'form-horizontal', 'method'=>'POST', 'target'=>'_blank']) !!}
        {{ csrf_field() }}

        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reporte de donaciones</h4>
                  </div>
                  <div class="modal-body"> 
                    <h4>Seleccionar el periodo que desea consultar.</h4> 
                        
                    <div class="form-group">
                    

                        <div class="col-md-6">
                            {!! Form::label('donante_id', 'SELECIONAR DESDE : ', ['class' => ' control-label']) !!}
                            {!! Form::text('desde', null, ['class' => 'form-control datepicker', 'required' => 'required', 'placeholder' => 'MM / DD / YYYY']) !!}
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('donante_id', 'SELECIONAR HASTA : ', ['class' => ' control-label']) !!}
                            {!! Form::text('hasta', null, ['class' => 'form-control datepicker', 'required' => 'required', 'placeholder' => 'MM / DD / YYYY']) !!}
                        </div>

                    </div>

                  </div>
                  <div class="modal-footer">
                    {!! Form::submit('Procesar solicitud', ['class' => 'btn btn-primary form-control create']) !!}
                  </div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>

    <!-- Modal DONACIONES -->
    <div id="transacciones" class="modal fade" role="dialog">

        {!! Form::open(['url' => 'ControlPanel/transacciones', 'class' => 'form-horizontal', 'method'=>'POST', 'target'=>'_blank']) !!}
        {{ csrf_field() }}

        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reporte de transacciones</h4>
                  </div>
                  <div class="modal-body"> 
                    <h4>Seleccionar el periodo que desea consultar.</h4> 
                        
                    <div class="form-group">
                    

                        <div class="col-md-6">
                            {!! Form::label('donante_id', 'SELECIONAR DESDE : ', ['class' => ' control-label']) !!}
                            {!! Form::text('desde', null, ['class' => 'form-control datepicker', 'required' => 'required', 'placeholder' => 'MM / DD / YYYY']) !!}
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('donante_id', 'SELECIONAR HASTA : ', ['class' => ' control-label']) !!}
                            {!! Form::text('hasta', null, ['class' => 'form-control datepicker', 'required' => 'required', 'placeholder' => 'MM / DD / YYYY']) !!}
                        </div>

                    </div>

                  </div>
                  <div class="modal-footer">
                    {!! Form::submit('Procesar solicitud', ['class' => 'btn btn-primary form-control create']) !!}
                  </div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>


@endsection

@section('javascript')
    <script>
        $(document).ready(function(){

            $('.action').click(function(){
                $modal = $(this).data('target');
                $($modal).find('form').attr('action',$(this).attr('href'));
            })

            $('.datepicker').datepicker({
                autoclose:true,
                dateFormat: 'yy-mm-dd',
            })

        });
    </script>
@endsection