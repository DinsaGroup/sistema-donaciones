<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Reporte detallado del donante</title>
  <style>
    html,body{
      font-family: arial;
      color:#1c1c1c;
    }
    .col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-10,.col-md-11,.col-md-12, .header{
      float: left;
    }
    .col-md-2{
      width: 16.66%;
    }
    .col-md-3{
      width: 25%;
    }
    .col-md-4{
      width: 33.33%;
    }
    .col-md-6{
      width: 50%;
    }
    .col-md-8{
      width: 66.66%;
    }
    .col-md-9{
      width: 75%;
    }
    .col-md-12{
      width: 100%;
    }
    .top{
    font-size: 10px !important;
    color:#ccc;
    }
    .txt-right{
    text-align: right;
    }
    .header, .details{
    width: 100%;
    float: left;
    }
    .title{
    text-transform: uppercase;
    width: 100%;
    margin-bottom: 0px;
    /*background-color: #ccc;*/
    }
    .row{
    width: 100%;
    float: left;
    }
    .label{
    font-size: 10px;
    color: #ccc;
    line-height: 60%;
    }
    .description{
    font-size: 12px;
    }

    .left{
    float: left;
    }

    .table{
    width: 400px !important;
    margin-left: 20px;
    }

    img{
    width: 120px;
    border-radius: 50%;
    }
    .logo{
      width: 25px !important;
      margin-top: 2px;
      border-radius: 0;
    }
    table{
      border: 0px;
      font-size: 10px;
    }
    th{
    margin-bottom: 10px;
    }

    .tarjetas tr + tr{
      padding: 5px 0;
      line-height: 25px;
      text-align: left;
      vertical-align: middle;
    }
    hr{
      border: thin solid #ccc;
      clear: both;
    }

    .clear{
      clear: both;
      width: 100%;
    }

  </style>
	  
</head>
<body>

  <header>
    <div class="top">Reporte generado desde la consola administrativa del sistema de donaciones </div>
    <div class="top">
      {{ date('l jS \of F Y h:i:s A') }}
    </div>
  </header>

  <details>
    <h3 class="title">
      Información del donante
    </h3>
    <hr>

    <img src="{{ url('img/user/'.$donante->photo ) }}" width="100%" class="left" />

    <table class="left" style="margin-bottom: 40px;">
      <tbody>
        <tr>
          <td>

            <table class="table left">
              <tbody>
                <tr>
                  <td width="35">Nombre</td>
                  <td>{{ $donante->name }}</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>{{ $donante->email }}</td>
                </tr>
                <tr>
                  <td>País</td>
                  <td>{{ $donante->country->name }}</td>
                </tr>
                <tr>
                  <td>Casa</td>
                  <td>{{ $donante->country->ext }} {{ $donante->house}}</td>
                </tr>
                <tr>
                  <td>Oficina</td>
                  <td>{{ $donante->country->ext }} {{ $donante->office }}</td>
                </tr>
                <tr>
                  <td>Celular</td>
                  <td>{{ $donante->country->ext }} {{ $donante->phone }}</td>
                </tr>
              </tbody>
            </table>
          </td>
          <td>
            <table class="table left tarjetas" style="margin-left: 60px">
              <tbody>
                <tr>
                    <th style="width: 100px">TARJETA</th>
                    <th style="width: 30px">EXP.</th>
                    <th style="width: 60px">DEFECTO</th>
                </tr>
                
                @foreach($donante->creditcards as $item)
                    <tr>
                        <td><img src="{{ url('img/credit/'.$item->ico ) }}" class="logo" /> **** **** **** {{ $item->lastdigit }}</td>
                        <td>{{ $item->month }} / {{ $item->year }}</td>
                        <td>
                            @if($item->default) 
                                <i class="fa fa-star"></i> Predeterminada 
                            @endif
                        </td>
                    </tr>
                @endforeach
                
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>

    <div class="clear">
    
    </div>

    <table class="left" style="margin-top: 20px;">
      <thead>
        <tr>
          <td colspan="9" ><h3 class="title">Detalle de transacciones</h3><br>A continuación se presentan todas las transcciones asociadas al donante.<hr></td>
        </tr>
        <tr>
          <th style="width: 20px;">ID</th>
          <th style="width: 40px;">TIPO</th>
          <th style="width: 110px;">TRANS. ID</th>
          <th style="width: 100px;">APLICADO A</th>

          <th style="width: 60px;">TARJETA</th>
          <th style="width: 60px;">MONTO</th>
          <th style="width: 100px;">FECHA</th>
          <th style="width: 50px;">ESTATUS</th>
          <th style="width: 40px;">OBSERVACION</th>
        </tr>
      </thead>
      <tbody>
        @foreach($transactions as $item)
          <tr>
            <td class="text-center">{{ $item->id }}</td>
            <td class="text-center">{{ $item->donationtype->name }}</td>
            <td class="text-center">{{ $item->transactionid }}</td>
            <td>{{ $item->campaign->name }}</td>
            <td class="text-center">**** {{ $item->creditcard->lastdigit }}</td>
            <td class="text-center">U$ {{ number_format($item->amount,2,'.',',') }}</td>
            <td class="text-center">{{ $item->created_at }}</td>
            <td class="text-center">{{ $item->status }}</td>
            <td>{{ $item->comments }}</td>
          </tr>
        @endforeach
        
      </tbody>
      
    </table> 
  </details>

  

</body>
</html>


