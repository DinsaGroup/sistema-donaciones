<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Listado de donantes</title>
  <style>
    html,body{
      font-family: arial;
      color:#1c1c1c;
    }
    .col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-10,.col-md-11,.col-md-12, .header{
      float: left;
    }
    .col-md-2{
      width: 16.66%;
    }
    .col-md-3{
      width: 25%;
    }
    .col-md-4{
      width: 33.33%;
    }
    .col-md-6{
      width: 50%;
    }
    .col-md-8{
      width: 66.66%;
    }
    .col-md-9{
      width: 75%;
    }
    .col-md-12{
      width: 100%;
    }
    .top{
    font-size: 10px !important;
    color:#ccc;
    }
    .txt-right{
    text-align: right;
    }
    .header, .details{
    width: 100%;
    float: left;
    }
    .title{
    text-transform: uppercase;
    width: 100%;
    margin-bottom: 0px;
    /*background-color: #ccc;*/
    }
    .row{
    width: 100%;
    float: left;
    }
    .label{
    font-size: 10px;
    color: #ccc;
    line-height: 60%;
    }
    .description{
    font-size: 12px;
    }

    .left{
    float: left;
    }

    .table{
    width: 400px !important;
    margin-left: 20px;
    }

    img{
    width: 120px;
    border-radius: 50%;
    }
    .logo{
      width: 25px !important;
      margin-top: 2px;
      border-radius: 0;
    }
    table{
      border: 0px;
      font-size: 10px;
    }
    th{
    margin-bottom: 10px;
    }

    .tarjetas tr + tr{
      padding: 5px 0;
      line-height: 25px;
      text-align: left;
      vertical-align: middle;
    }
    hr{
      border: thin solid #ccc;
      clear: both;
    }

    .clear{
      clear: both;
      width: 100%;
    }

  </style>  
</head>
<body>

  <header>
    <div class="top">Reporte generado desde la consola administrativa del sistema de donaciones </div>
    <div class="top">
      {{ date('l jS \of F Y h:i:s A') }}
    </div>
  </header>

  <details>
    <h3 class="title">
      Listado de donantes
    </h3>
    <hr>

    <table class="left" style="margin-top: 20px;">
      <thead>
        <tr>
          <th style="width: 20px;">ID</th>
          <th style="width: 200px;">NOMBRE</th>
          <th style="width: 150px;">EMAIL</th>
          <th style="width: 110px;">PAIS</th>
          <th style="width: 60px;">CASA</th>
          <th style="width: 60px;">OFICINA</th>
          <th style="width: 60px;">CELULAR</th>
          <th style="width: 60px;">SUSCRITO</th>
        </tr>
      </thead>
      <tbody>
        @foreach($donantes as $item)
          <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->name }} </td>
            <td>{{ $item->email }}</td>
            <td>{{ $item->country->name }}</td>
            <td>{{ $item->house }}</td>
            <td>{{ $item->office }}</td>
            <td>{{ $item->phone }}</td>
            <td>{{ $item->boletin }}</td>
          </tr>
        @endforeach
        
      </tbody>
      
    </table> 
  </details>

  

</body>
</html>


