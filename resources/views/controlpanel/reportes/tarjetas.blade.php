<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Listado de donantes</title>
  <style>
    html,body{
      font-family: arial;
      color:#1c1c1c;
    }
    .col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-10,.col-md-11,.col-md-12, .header{
      float: left;
    }
    .col-md-2{
      width: 16.66%;
    }
    .col-md-3{
      width: 25%;
    }
    .col-md-4{
      width: 33.33%;
    }
    .col-md-6{
      width: 50%;
    }
    .col-md-8{
      width: 66.66%;
    }
    .col-md-9{
      width: 75%;
    }
    .col-md-12{
      width: 100%;
    }
    .top{
    font-size: 10px !important;
    color:#ccc;
    }
    .txt-right{
    text-align: right;
    }
    .header, .details{
    width: 100%;
    float: left;
    }
    .title{
    text-transform: uppercase;
    width: 100%;
    margin-bottom: 0px;
    /*background-color: #ccc;*/
    }
    .row{
    width: 100%;
    float: left;
    }
    .label{
    font-size: 10px;
    color: #ccc;
    line-height: 60%;
    }
    .description{
    font-size: 12px;
    }

    .left{
    float: left;
    }

    .table{
    width: 400px !important;
    margin-left: 20px;
    }

    img{
    width: 120px;
    border-radius: 50%;
    }
    .logo{
      width: 25px !important;
      margin-top: 2px;
      border-radius: 0;
    }
    table{
      border: 0px;
      font-size: 10px;
    }
    th{
    margin-bottom: 10px;
    }

    .tarjetas tr + tr{
      padding: 5px 0;
      line-height: 25px;
      text-align: left;
      vertical-align: middle;
    }
    hr{
      border: thin solid #ccc;
      clear: both;
    }

    .clear{
      clear: both;
      width: 100%;
    }

  </style>  
</head>
<body>

  <header>
    <div class="top">Reporte generado desde la consola administrativa del sistema de donaciones </div>
    <div class="top">
      {{ date('l jS \of F Y h:i:s A') }}
    </div>
  </header>

  <details>
    <h3 class="title">
      Listado de tarjetas del sistema
    </h3>
    <hr>

    <table class="left" style="margin-top: 20px;">
      <thead>
        <tr>
          <th style="width: 20px;">ID</th>
          <th style="width: 180px;">DONANTE</th>
          <th style="width: 70px;">TARJETA</th>
          <th style="width: 180px;">NOMBRE SOBRE LA TARJETA</th>
          <th style="width: 80px;">EXPIRA</th>
          <th style="width: 100px;">POR DEFECTO</th>
          <th style="width: 100px;">CREADA</th>
        </tr>
      </thead>
      <tbody>
        @foreach($tarjetas as $item)
          <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->user->name }} </td>
            <td>**** {{ $item->lastdigit }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ $item->month }} / {{ $item->year }}</td>
            <td>@if($item->default) <i class="fa fa-star"></i>Predeterminada @endif</td>
            <td>{{ $item->created_at }}</td>
          </tr>
        @endforeach
        
      </tbody>
      
    </table> 
  </details>

  

</body>
</html>


