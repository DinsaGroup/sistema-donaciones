<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Reporte de donaciones</title>
  <style>
    html,body{
      font-family: arial;
      color:#1c1c1c;
      font-size: 0.6rem;
    }
    .col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-10,.col-md-11,.col-md-12, .header{
      float: left;
    }
    .col-md-2{
      width: 16.66%;
    }
    .col-md-3{
      width: 25%;
    }
    .col-md-4{
      width: 33.33%;
    }
    .col-md-6{
      width: 50%;
    }
    .col-md-8{
      width: 66.66%;
    }
    .col-md-9{
      width: 75%;
    }
    .col-md-12{
      width: 100%;
    }
    .top{
    font-size: 10px !important;
    color:#ccc;
    }
    .txt-right{
    text-align: right;
    }
    .header, .details{
    width: 100%;
    float: left;
    }
    .title{
    text-transform: uppercase;
    width: 100%;
    margin-bottom: 0px;
    /*background-color: #ccc;*/
    }
    .row{
    width: 100%;
    float: left;
    }
    .label{
    font-size: 10px;
    color: #ccc;
    line-height: 60%;
    }
    .description{
    font-size: 12px;
    }

    .left{
    float: left;
    }

    .table{
    width: 400px !important;
    margin-left: 20px;
    }

    img{
    width: 120px;
    border-radius: 50%;
    }
    .logo{
      width: 25px !important;
      margin-top: 2px;
      border-radius: 0;
    }
    table{
      border: 0px;
      font-size: 10px;
    }
    th{
    margin-bottom: 10px;
    }

    .tarjetas tr + tr{
      padding: 5px 0;
      line-height: 25px;
      text-align: left;
      vertical-align: middle;
    }
    hr{
      border: thin solid #ccc;
      clear: both;
    }

    .clear{
      clear: both;
      width: 100%;
    }

  </style>  
</head>
<body>

  <header>

    <div class="top">Reporte generado desde la consola administrativa del sistema de donaciones </div>
      
    <div class="top">
      {{ date('l jS \of F Y h:i:s A') }}
    </div>
    
    <div class="col-md-6">
      
      <h1 style="font-size: 2.5rem">FONDO CENTROAMERICANO DE MUJERES<br>SISTEMA DE DONACIONES</h1>
      
      <table>
        <tr>
          <td colspan="2">PERIODO</td>
        </tr>
        <tr>
          <td style="width: 150px">Desde</td>
          <td style="width: 150px">Hasta</td>
        </tr>
        <tr>
          <td>{{ $desde }}</td>
          <td>{{ $hasta }}</td>
        </tr>
      </table>
    </div>
    <div class="col-md-6"> 
      <br>
      <br>
      <table style="border:solid 1px #ccc;">
        <tr style="background-color: #ccc">
          <td style="width: 200px"><b>TOTAL DE TRANSACCIONES</b></td>
          <td><b>U$ {{ number_format($total,2,'.',',') }}</b></td>
        </tr>
        <tr>
          <td>MONTO TOTAL PROCESANDO</td>
          <td>U$ {{ number_format($procesando,2,'.',',') }}</td>
        </tr>
        <tr>
          <td>MONTO TOTAL REVERSION</td>
          <td>U$ {{ number_format($reversionAprobada,2,'.',',') }}</td>
        </tr>
        <tr>
          <td>MONTO TOTAL ANULACION</td>
          <td>U$ {{ number_format($anulacion,2,'.',',') }}</td>
        </tr>
        <tr>
          <td>MONTO TOTAL PAGO REVERTIDO</td>
          <td>U$ {{ number_format($pagoRevertido,2,'.',',') }}</td>
        </tr>
        <tr>
          <td>MONTO TOTAL APROBADO</td>
          <td>U$ {{ number_format($aprobada,2,'.',',') }}</td>
        </tr>
      </table>
    </div>
  </header>

<hr>
  <details>
    <h3 class="title">
      Detalle de Transacciones Procesado
    </h3>
    <hr>

    <table class="left" style="margin-top: 20px;">
      <thead>
        <tr>
          <th style="width: 10px;">ID</th>
          <th style="width: 40px;">TIPO</th>
          <th style="width: 40px;">TRANS. ID</th>
          <th style="width: 85px;">APLICADO A</th>
          <th style="width: 85px;">DONANTE</th>
          <th style="width: 50px;">TARJETA</th>
          <th style="width: 50px;">MONTO U$</th>
          <th style="width: 90px;">FECHA</th>
          <th style="width: 100px;">ESTATUS</th>
          <!-- <th style="width: 60px;">OBSERVACION</th> -->
        </tr>
      </thead>
      <tbody>
        @foreach($transactions as $item)
          <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->donationtype->name }} </td>
            <td>{{ $item->transactionid }}</td>
            <td>{{ $item->campaign->name }}</td>
            <td style="text-transform: lowercase;">{{ $item->creditcard->user->name }}</td>
            <td>**** {{ $item->creditcard->lastdigit }}</td>
            <td>U$ {{ number_format($item->amount,2,'.',',') }}</td>
            <td>{{ $item->created_at }}</td>
            <td style="text-transform: lowercase;">{{ $item->status }}</td>
            <!-- <td>{{ $item->comments }}</td> -->
          </tr>
        @endforeach
        
      </tbody>
      
    </table> 
  </details>

  

</body>
</html>