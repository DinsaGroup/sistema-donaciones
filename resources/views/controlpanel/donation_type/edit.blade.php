@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
                

            <h1>{{ $title }} <a href="{{ url('ControlPanel/tipo-donantes') }}" class="btn btn-danger pull-right btn-sm"><i class="fa fa-times"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

            {!! Form::model($donationType, [
                    'method' => 'PATCH',
                    'url' => ['ControlPanel/tipo-donantes', $donationType->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}
                {{ csrf_field() }}
            <div class="col-xs-12">
            
                    <div class="form-group">
                        {!! Form::label('name', 'PERIODO : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: Mensual, Anual, etc.']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'DESCRIPCIÓN : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-6">
                            {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: Descripción del periodo']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('days', 'MESES : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-1">
                            {!! Form::text('days', null, ['class' => 'form-control only_integer', 'required' => 'required', 'placeholder'=>'Ej: 1, 2,  3,']) !!}
                        </div>
                    </div>
                
                    
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>
                </div>

            {!! Form::close() !!}

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection