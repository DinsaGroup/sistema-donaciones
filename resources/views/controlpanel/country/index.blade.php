@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ url('ControlPanel/paises/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a></h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
          
        </section>

        <section class="content">

                <div class="ibox float-e-margins">
                    <div class="ibox-content"> <!-- comienza el contenido de la seccion -->

            
                        <p>Se encontraron {{ count($countries) }} paises registrados en la última consulta.</p>

                        <div class="table-responsive"> {{-- update --}}
                            <table class="table table-striped table-bordered table-hover dataTables"> {{-- update --}}
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">CODE 1</th>
                                        <th class="text-center">CODE 2</th>
                                        <th class="text-center">AREA</th>
                                        <th class="text-center">PAIS</th>
                                        <th class="text-center"><i class="fa fa-sort-desc"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- */$x=0;/* --}}
                                    @foreach($countries as $item)
                                        {{-- */$x++;/* --}}
                                        <tr class="gradeA"> {{-- update --}}
                                            <td class="text-center">{{ $item->id }}</td>
                                            <td class="text-center">{{ $item->code1 }}</td>
                                            <td class="text-center">{{ $item->code2 }}</td>
                                            <td class="text-center">{{ $item->ext }}</td>
                                            <td>{{ $item->name }}</td>
                                            
                                            <td  class="text-center">
                                                <a href="{{ url('ControlPanel/paises/' . $item->id . '/edit') }}" class="details">
                                                    <i class="fa fa-pencil"></i>
                                                </a> 
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination"></div>
                        </div>

                    </div> <!-- Termina el contenido de la seccion --> 
                </div> <!-- Termina el ibox --> 
            

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')

<!-- Data Tables -->
    <script src="{{{asset('js/backend/dataTables/jquery.dataTables.js')}}}"></script>
    <script src="{{{asset('js/backend/dataTables/dataTables.bootstrap.js')}}}"></script>
    <script src="{{{asset('js/backend/dataTables/dataTables.responsive.js')}}}"></script>
    <script src="{{{asset('js/backend/dataTables/dataTables.tableTools.min.js')}}}"></script>

<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {
        $('.dataTables').dataTable({
            responsive: true,
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "{{{asset('js/backend/dataTables/swf/copy_csv_xls_pdf.swf')}}}"
            },
            "pageLength": 100,
            "order" : [[0, 'desc']],
            "language": {
               "lengthMenu": '<select class="form-control input-sm">'+
                 '<option value="100">100</option>'+
                 '<option value="200">200</option>'+
                 '<option value="300">300</option>'+
                 '<option value="400">400</option>'+
                 '<option value="500">500</option>'+
                 '<option value="-1">All</option>'+
                 '</select> por pagina',
                "search": "Filtrar : " 
             }
        });
        
    });  
</script>

@endsection