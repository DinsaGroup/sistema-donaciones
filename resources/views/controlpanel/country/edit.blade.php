@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
                

            <h1>{{ $title }} <a href="{{ url('ControlPanel/paises') }}" class="btn btn-danger pull-right btn-sm"><i class="fa fa-times"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

            {!! Form::model($countries, [
                    'method' => 'PATCH',
                    'url' => ['ControlPanel/paises', $countries->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}
                {{ csrf_field() }}

            <div class="col-xs-12">
            
            

                    <div class="form-group">
                        {!! Form::label('code1', 'CODE 1 : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-1">
                            {!! Form::text('code1', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'NI']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('code2', 'CODE 2 : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-1">
                            {!! Form::text('code2', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'NIC']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('ext', 'CODIGO DE AREA : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-1">
                            {!! Form::text('ext', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'505']) !!}
                        </div>
                    </div>

                     <div class="form-group">
                        {!! Form::label('name', 'PAIS : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'NICARAGUA']) !!}
                        </div>
                    </div>
                
                    
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>
                </div>

            {!! Form::close() !!}

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection