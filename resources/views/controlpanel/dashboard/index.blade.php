@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

	<!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Resumen comportamiento</small>
            <a href="{{ url('ControlPanel/update-data/ledger') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-refresh"></i> Actualizar datos</a>
          </h1>
          
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-social-usd"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Monto recaudado</span>
                  <span class="info-box-number">U$ {{ number_format($ledger->total,2,'.',',') }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Donantes</span>
                  <span class="info-box-number">{{ $ledger->donants }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-exchange"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Transacciones</span>
                  <span class="info-box-number">{{ $ledger->transactions }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-cc"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Tarjetas registradas</span>
                  <span class="info-box-number">{{ $ledger->creditcards }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

          </div><!-- /.row -->

          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Comportamiento mensual</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <div class="btn-group">
                      <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </div>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <p class="text-center">
                        <strong>Resumen de lo recaudado</strong>
                      </p>
                      <div class="chart-responsive">
                        <!-- Sales Chart Canvas -->
                        <canvas id="salesChart" height="180"></canvas>
                      </div><!-- /.chart-responsive -->
                    </div><!-- /.col -->
                    
                  </div><!-- /.row -->
                </div><!-- ./box-body -->
                <div class="box-footer">
                  <div class="row">
                    <div class="col-sm-3 col-xs-6">
                      <div class="description-block border-right">
                        <h5 class="description-header">U$ {{ number_format($ledger->year, 2,'.',',') }} USD</h5>
                        <span class="description-text">TOTAL RECAUDADO EN EL {{ date('Y') }}</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-3 col-xs-6">
                      <div class="description-block border-right">
                        <h5 class="description-header">U$ {{ number_format($ledger->month, 2,'.',',') }} USD</h5>
                        <span class="description-text">TOTAL MES ACTUAL</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-3 col-xs-6">
                      <div class="description-block border-right">
                        <h5 class="description-header">U$ {{ number_format($ledger->lastdonation, 2,'.',',') }} USD</h5>
                        <span class="description-text">ULTIMA DONACION</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-3 col-xs-6">
                      <div class="description-block">
                        <h5 class="description-header">C$ {{ $exchange_rate_bcn }}</h5>
                        <span class="description-text">TIPO DE CAMBIO BCN</span>
                      </div>
                    </div>
                  </div><!-- /.row -->
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->


          <div class="row">
            <div class="col-md-7">
              <!-- TABLE: LATEST ORDERS -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-exchange"></i> Últimas transacciones</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table id="transaction_history" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>TRANS. ID</th>
                        <th>TIPO</th>
                        <th>DONANTE</th>
                        <th>TARJETA</th>
                        <th>MONTO U$</th>
                        <th>FECHA</th>
                        <th>ESTATUS</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($transactions as $item)
                        <tr>
                          <td class="text-center">
                            <a href="{{ url('ControlPanel/transacciones/'.$item->id) }}" class="" >
                              {{ $item->id }}
                            </a>
                          </td>
                          <td class="text-center">{{ $item->transactionid }}</td>
                          <td class="text-center">{{ $item->donationtype->name }}</td>
                          <td class="text-center">
                            <a href="{{ url('ControlPanel/donantes/'.$item->creditcard->user->id) }}" >
                              <img src="{{ url('img/user/'.$item->creditcard->user->photo )}}" class="user-image"> {{ $item->creditcard->user->name }}
                            </a>
                          </td>
                          <td class="text-center"><img src="{{ url('img/credit/'. $item->creditcard->ico) }}" class="ccard"> **** **** **** {{ $item->creditcard->lastdigit }}</td>
                          <td class="text-center">U$ {{ number_format($item->amount,2,'.',',') }}</td>
                          <td class="text-center">{{ $item->created_at }}</td>
                          <td class="text-center">{{ $item->status }}</td>
                        </tr>
                      @endforeach

                    </tbody>
                    
                  </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                  <a href="{{ url('ControlPanel/transacciones/create') }}" class="btn btn-sm btn-info btn-flat pull-left">Hacer una nueva donación</a>
                  <a href="{{ url('ControlPanel/transacciones') }}" class="btn btn-sm btn-default btn-flat pull-right">Ver todas</a>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class='col-md-5'>
              <!-- USERS LIST -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-group"></i> Últimos donantes registrados</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <ul class="users-list clearfix">
                    @foreach($donantes as $item)
                      <li>
                        <a href="{{ url('ControlPanel/donantes/'.$item->id) }}" class="" >
                          <img src="{{ url('img/user/'.$item->photo) }}" alt="User Image" class="photo-donante" />
                          <a class="users-list-name" href="#">{{ $item->name }}</a> 
                        </a>
                      </li>
                    @endforeach
                  </ul><!-- /.users-list -->
                </div><!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="{{ url('ControlPanel/donantes') }}" class="uppercase">Ver todos los donantes</a>
                </div><!-- /.box-footer -->
              </div><!--/.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

@endsection

@section('javascript')

<!-- jvectormap -->
<script src="{{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}}" type="text/javascript"></script>
<script src="{{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}}" type="text/javascript"></script>

<!-- ChartJS 1.0.1 -->
<script src="{{{ asset('plugins/chartjs/Chart.min.js') }}}" type="text/javascript"></script>


<script>
  $(function () {
  
  //Simple implementation of direct chat contact pane toggle (TEMPORARY)
  $('[data-widget="chat-pane-toggle"]').click(function(){
    $("#myDirectChat").toggleClass('direct-chat-contacts-open');
  });

  // Get context with jQuery - using jQuery's .get() method.
  var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
  // This will get the first returned node in the jQuery collection.
  var salesChart = new Chart(salesChartCanvas);

  var salesChartData = {
    
    labels: ["{{ array_get($saleChart,'0.month') }}","{{ array_get($saleChart,'1.month') }}","{{ array_get($saleChart,'2.month') }}","{{ array_get($saleChart,'3.month') }}","{{ array_get($saleChart,'4.month') }}","{{ array_get($saleChart,'5.month') }}", "{{ array_get($saleChart,'6.month') }}", "{{ array_get($saleChart,'7.month') }}", "{{ array_get($saleChart,'8.month') }}", "{{ array_get($saleChart,'9.month') }}", "{{ array_get($saleChart,'10.month') }}", "{{ array_get($saleChart,'11.month') }}"],
    datasets: [
      {
        label: "Electronics",
        fillColor: "rgba(60,141,188,0.9)",
        strokeColor: "rgba(60,141,188,0.8)",
        pointColor: "#3b8bba",
        pointStrokeColor: "rgba(60,141,188,1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(60,141,188,1)",
        
        data: [{{ array_get($saleChart, '0.amount') }}, {{ array_get($saleChart, '1.amount') }}, {{ array_get($saleChart, '2.amount') }}, {{ array_get($saleChart, '3.amount') }}, {{ array_get($saleChart, '4.amount') }}, {{ array_get($saleChart, '5.amount') }}, {{ array_get($saleChart, '6.amount') }}, {{ array_get($saleChart, '7.amount') }}, {{ array_get($saleChart, '8.amount') }}, {{ array_get($saleChart, '9.amount') }}, {{ array_get($saleChart, '10.amount') }}, {{ array_get($saleChart, '11.amount') }}]
      },
      // {
      //   label: "Digital Goods",
      //   fillColor: "rgb(210, 214, 222)",
      //   strokeColor: "rgb(210, 214, 222)",
      //   pointColor: "rgb(210, 214, 222)",
      //   pointStrokeColor: "#c1c7d1",
      //   pointHighlightFill: "#fff",
      //   pointHighlightStroke: "rgb(220,220,220)",
      //   data: [28, 48, 40, 19, 86, 27, 90]
      // }
    ]
  };

  var salesChartOptions = {
    //Boolean - If we should show the scale at all
    showScale: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: false,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - Whether the line is curved between points
    bezierCurve: true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension: 0.3,
    //Boolean - Whether to show a dot for each point
    pointDot: false,
    //Number - Radius of each point dot in pixels
    pointDotRadius: 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 1,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,
    //Boolean - Whether to fill the dataset with a color
    datasetFill: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: false,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true
  };

  //Create the line chart
  salesChart.Line(salesChartData, salesChartOptions);

  
});
</script>

@endsection