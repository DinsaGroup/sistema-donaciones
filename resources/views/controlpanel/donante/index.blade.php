@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
        @if(Auth::user()->can('crear-donante'))
            <h1>
              {{ $title }} 
              <a href="{{ url('ControlPanel/donantes/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a>
              <a href="{{ url('ControlPanel/reporte/donantes/') }}" class="btn btn-primary pull-right btn-sm" style="margin: 0 10px;" target="_blank"><i class="fa fa-print"></i> Imprimir PDF</a>
            </h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
        @else
            <h1>{{ $title }}</h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
        @endif
          
        </section>

         <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-users"></i> Listado de donantes</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="transaction_history" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                          {{-- <th>ID</th> --}}
                          <th>Nombre</th>
                          <th>Email</th>
                          <th>País</th>
                          <th>Casa</th>
                          <th>Oficina</th>
                          <th>Celular</th>
                          <th>Boletín</th>
                          
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($donantes as $item)
                          {{-- */$x++;/* --}}
                          <tr class="gradeA"> {{-- update --}}
                              {{-- <td>{{ $item->id }}</td> --}}

                              <td>
                                <img src="{{ url('img/user/'. $item->photo) }}" class="user-image"/> 
                                <a href="{{ url('ControlPanel/donantes/' . $item->id ) }}" class="details">{{ $item->name }}</a>
                              </td>
                              <td><a href="{{ url('ControlPanel/donantes/' . $item->id ) }}" class="details">{{ $item->email }}</a></td>
                              <td><a href="{{ url('ControlPanel/donantes/' . $item->id ) }}" class="details">{{ $item->country->name }} + ( {{ $item->country->ext }} )</a></td>
                              <td><a href="{{ url('ControlPanel/donantes/' . $item->id ) }}" class="details">{{ $item->phone }}</a></td>
                              <td><a href="{{ url('ControlPanel/donantes/' . $item->id ) }}" class="details">{{ $item->office }}</a></td>
                              <td><a href="{{ url('ControlPanel/donantes/' . $item->id ) }}" class="details">{{ $item->cell }}</a></td>
                              <td><a href="{{ url('ControlPanel/donantes/' . $item->id ) }}" class="details">{{ $item->boletin }}</a></td>
                              
                          </tr>
                      @endforeach
                      
                    </tbody>
                    <tfoot>
                      <tr>
                          {{-- <th>ID</th> --}}

                          <th>Nombre</th>
                          <th>Email</th>
                          <th>País</th>
                          <th>Casa</th>
                          <th>Oficina</th>
                          <th>Celular</th>
                          <th>Boletín</th>
                          {{-- <th><i class="fa fa-sort-desc"></i></th> --}}
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                </div> {{-- col-md-12 --}}
            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')
<!-- page script -->
    <script type="text/javascript">
      $(function () {
        $('#transaction_history').dataTable({
          "bSortClasses": 'sorting_1',
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "aaSorting": [[0,'desc']],
          "iDisplayLength": 100,
          "aLengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]]
        });
      });
    </script>
@endsection