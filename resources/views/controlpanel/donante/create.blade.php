@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ url('ControlPanel/donantes') }}" class="btn btn-danger pull-right btn-sm"><i class="fa fa-times"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

            {!! Form::open(['url' => 'ControlPanel/donantes', 'class' => 'form-horizontal', 'method'=>'POST', 'files'=>true]) !!}
            {{ csrf_field() }}

                <div class="">
                    <div class="form-group">
                        {!! Form::label('name', 'Nombre : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-6">
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: Ana Carolina Baez']) !!}
                        </div>
                    </div>
     
                    <div class="form-group">
                        {!! Form::label('email', 'Correo electrónico : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-6">
                            {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'micorreo@midominio.com']) !!}

                        </div>
                        <div class="col-md-2">
                            <div id='ok' class="hidden">
                                <i  class="fa fa-check fa-2x" style="color:green"></i> Ok
                            </div>

                            <div id='bad' class="hidden">
                                <i class="fa fa-times fa-2x" style="color:red"></i> ¡Ya existe!
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('avatar', 'Seleccionar avatar : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-6">
                            <img src="/img/user/avatar-default.jpg" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                            <input type='file' name="avatar" class="from-control" style="padding-top: 10px"> 
                            {{-- {!! Form::file('avatar', null, ['class' => 'form-control ', 'style'=>'padding-top:10px' ]) !!} --}}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('country_id', 'País : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-6">
                            {!! Form::select('country_id', $countries ,null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar País']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('house', 'Teléfono casa : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-2">
                            {!! Form::text('house', null, ['class' => 'form-control phone_number', 'placeholder'=>'8888 8888']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('office', 'Teléfono oficina : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-2">
                            {!! Form::text('office', null, ['class' => 'form-control phone_number', 'placeholder'=>'8888 8888']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone', 'Celular : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-2">
                            {!! Form::text('phone', null, ['class' => 'form-control phone_number', 'placeholder'=>'8888 8888']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('boletin', 'Suscribir al boletín : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::select('boletin', ['Si'=>'Suscribir','No'=>'No suscribir'] ,null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccionar opción']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('comments', 'Notas internas : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-6">
                            {!! Form::textarea('comments', null, ['class' => 'form-control','rows'=>3]) !!}
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-4">
                            {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                        </div>
                    </div>

                
                
                </div>

                {!! Form::close() !!}
        
        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')
<script>
    $(document).ready(function(){
        $('#email').focusout(function(){
            var $email = $(this).val();
            var $url = '{{ url('ControlPanel/email/') }}';
            $url = $url + '/' + $email;

            $.getJSON($url,'',function(resp) {

                if(resp=='true'){
                    if(!$('#ok').hasClass('hidden')){
                       $('#ok').toggleClass('hidden'); 
                    }

                    $('#bad').toggleClass('hidden');

                }else{
                    if(!$('#bad').hasClass('hidden')){
                       $('#bad').toggleClass('hidden'); 
                    }

                    $('#ok').toggleClass('hidden');    
                }
                
            });
        })
    })
</script>
@endsection