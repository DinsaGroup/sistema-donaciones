@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Perfil {{ $title }} 
                <a href="{{ url('ControlPanel/donantes') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-undo"></i> Regresar</a>
                <a href="{{ url('ControlPanel/reporte/donante/'. $donante->id) }}" class="btn btn-primary pull-right btn-sm" style="margin: 0 10px;" target="_blank"><i class="fa fa-print"></i> Imprimir PDF</a>
            </h1>
            <small>Detalle del perfil</small>
          
        </section>

        <section class="content">

            <div class="row">
                {{-- Perfil --}}
                <div class="col-md-6">
                  <!-- TABLE: LATEST ORDERS -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title"><i class="fa fa-user"></i> Perfil</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="direct-chat-messages">
                            <div class="col-xs-4">
                                <img src="{{ url('img/user/'.$donante->photo ) }}" width="100%" />
                            </div>
                            <div class="col-xs-8">
                                <div class="row"><div class="col-xs-3">Nombre</div><div class="col-xs-8">{{ $donante->name }}</div></div>
                                <div class="row"><div class="col-xs-3">Email</div><div class="col-xs-8">{{ $donante->email }}</div></div>
                                <div class="row"><div class="col-xs-3">País</div><div class="col-xs-8">{{ $donante->country->name }}</div></div>
                                <div class="row"><div class="col-xs-3">Casa</div><div class="col-xs-8">({{ $donante->country->ext }}) {{ $donante->house}}</div></div>
                                <div class="row"><div class="col-xs-3">Oficina</div><div class="col-xs-8">({{ $donante->country->ext }}) {{ $donante->office }}</div></div>
                                <div class="row"><div class="col-xs-3">Celular</div><div class="col-xs-8">({{ $donante->country->ext }}) {{ $donante->phone }}</div></div>
                                <br><br>
                                <div class="row">
                                    <div class="col-xs-12">Notas privadas</div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12"> {{ $donante->comments }}</div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    @if(Auth::user()->can('editar-donante'))
                    <div class="box-footer clearfix">
                      <a href="{{ url('ControlPanel/donantes/'.$donante->id.'/edit') }}" class="btn btn-sm btn-info btn-flat pull-left">Editar perfil</a>
                    </div><!-- /.box-footer -->
                    @else
                    <div class="box-footer clearfix">
                      
                    </div><!-- /.box-footer -->
                    @endif
                    
                  </div><!-- /.box -->
                </div>
                
                {{-- Tarjetas asociadas --}}
                <div class="col-md-6">
                  <!-- TABLE: LATEST ORDERS -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title"><i class="fa fa-credit-card"></i> Tarjetas asociadas</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="direct-chat-messages">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <thead>
                                        <tr>
                                            <th>TARJETA</th>
                                            <th>EXP.</th>
                                            <th>DEFECTO</th>
                                            <th><i class="fa fa-sort-desc"></i></th>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($donante->creditcards as $item)
                                            <tr>
                                                <td><img src="{{ url('img/credit/'.$item->ico ) }}" class="ccard" /> **** **** **** {{ $item->lastdigit }}</td>
                                                <td>{{ $item->month }} / {{ $item->year }}</td>
                                                <td>
                                                    @if($item->default) 
                                                        <i class="fa fa-star"></i> Predeterminada 
                                                    {{-- @else 
                                                        <a href="{{ url('ControlPanel/tarjetas/predeterminada/'.$item->id) }}"><i class="fa fa-star-o"></i> Cambiar a predeterminada</a> --}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(Auth::user()->can('editar-tarjeta'))
                                                        <a href="{{ url('ControlPanel/tarjetas/' . $item->id . '/edit') }}" class="details">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->
                        </div>
                        
                    </div><!-- /.box-body -->

                    <div class="box-footer clearfix">
                        @if(Auth::user()->can('crear-tarjeta'))
                            <a href="#" class="btn btn-sm btn-info btn-flat pull-left addCC" data-toggle="modal" data-target="#creditcard">Agregar nuevo</a>
                        @endif
                    </div><!-- /.box-footer -->
                    
                  </div><!-- /.box -->
                </div>
 
            </div><!-- /.row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-exchange"></i> Historial de transacciones</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="transaction_history" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>TIPO</th>
                        <th>TRANS. ID</th>
                        <th>APLICADO A</th>

                        <th>TARJETA</th>
                        <th>MONTO U$</th>
                        <th>FECHA</th>
                        <th>ESTATUS</th>
                        <th>OBSERVACION</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($transactions as $item)
                        <tr>
                          <td class="text-center">
                            <a href="{{ url('ControlPanel/transacciones/'.$item->id) }}" class="" >
                              {{ $item->id }}
                            </a>
                            
                            </td>
                          <td class="text-center">{{ $item->donationtype->name }}</td>
                          <td class="text-center">{{ $item->transactionid }}</td>
                          <td>{{ $item->campaign->name }}</td>
                          <td class="text-center"><img src="{{ url('img/credit/'. $item->creditcard->ico) }}" class="ccard"> **** **** **** {{ $item->creditcard->lastdigit }}</td>
                          <td class="text-center">U$ {{ number_format($item->amount,2,'.',',') }}</td>
                          <td class="text-center">{{ $item->created_at }}</td>
                          <td class="text-center">{{ $item->status }}</td>
                          <td>{{ $item->comments }}</td>
                        </tr>
                      @endforeach
                      
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>ID</th>
                        <th>TIPO</th>
                        <th>TRANS. ID</th>
                        <th>APLICADO A</th>
                        <th>TARJETA</th>
                        <th>MONTO U$</th>
                        <th>FECHA</th>
                        <th>ESTATUS</th>
                        <th>OBSERVACION</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                </div> {{-- col-md-12 --}}
            </div>

        </section>

    </div>


    <!-- Modals -->

    <!-- Modal DONANTE -->
    <div id="creditcard" class="modal fade" role="dialog">

        {!! Form::open(['url' => 'ControlPanel/tarjetas', 'class' => 'form-horizontal', 'method'=>'POST']) !!}
        {{ csrf_field() }}

        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-credit-card"></i> NUEVA TARJETA</h4>
                </div>

                <div class="modal-body">  
                        
                    <div class="form-group">
                        <div class="col-md-4">
                            {!! Form::hidden('user_id',$donante->id, ['class' => 'form-control', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-4">
                            {!! Form::hidden('url','ControlPanel/donantes/'.$donante->id, ['class' => 'form-control', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('type', 'TIPO TARJETA : ', ['class' => 'control-label col-md-5']) !!}
                        <div class="col-md-6">
                            {!! Form::select('type',['visa.png'=>'VISA','mastercard.png'=>'MASTERCARD','american-express.png'=>'AMERICAN EXPRESS'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'SELECCIONAR']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('number', 'NUMERO DE TARJETA : ', ['class' => ' control-label col-md-5']) !!}
                        <div class="col-md-6">
                            {!! Form::text('number', null, ['class' => 'form-control only_integer', 'required' => 'required', 'placeholder'=>'4111 1111 1111 1111']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('name', 'NOMBRE SOBRE LA TARJETA : ', ['class' => ' control-label col-md-5']) !!}
                        <div class="col-md-6">
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'NOMBRE SOBRE LA TARJETA']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('month', 'VENCE : ', ['class' => 'control-label col-md-5']) !!}

                        <div class="col-md-3">
                            {!! Form::select('month',['01'=>'01 Enero', '02'=>'02 Febrero', '03'=>'03 Marzo', '04'=>'04 Abril', '05'=>'05 Mayo', '06'=>'06 Junio', '07'=>'07 Julio', '08'=>'08 Agosto', '09'=>'09 Septiembre', '10'=>'10 Octubre', '11'=>'11 Noviembre', '12'=>'12 Diciembre'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'MES']) !!}
                        </div>
                        <div class="col-md-3"> 
                            {!! Form::select('year',['2022'=>'2022', '2021'=>'2021', '2020'=>'2020', '2019'=>'2019', '2018'=>'2018', '2017'=>'2017'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'AÑO']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('cvc', 'CVC : ', ['class' => ' control-label col-md-5']) !!}
                        <div class="col-md-2">
                            {!! Form::text('cvc', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'421']) !!}
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    {!! Form::submit('Guardar tarjeta', ['class' => 'btn btn-primary form-control create']) !!}
                </div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>

@endsection

@section('javascript')
<!-- page script -->
    <script type="text/javascript">
      $(function () {
        $('#transaction_history').dataTable({
          "bSortClasses": 'sorting_3',
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "aaSorting": [[6,'desc']],
          "iDisplayLength": 100,
          "aLengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]]
        });
      });

      $(document).ready(function(){
        $('.addCC').click(function(){
            // $e.preventDefault();
            $('#creditcards').fadeOut('slow',function(){
                $('#new_creditcard').toggleClass('hide');
            })
        })
      })
    </script>
@endsection