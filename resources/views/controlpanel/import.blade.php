@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

<div class="content-wrapper">
    <section class="content-header">
            <h1>
            {{ $title }}
             
            </h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>          
        </section>

    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">

                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-building"></i> Importar información</h3>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                          <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-donantes"><i class="fa fa-upload"></i> Importar donantes</a>

                          <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-tarjetas"><i class="fa fa-upload"></i> Importar tarjetas</a>

                          <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-ip"><i class="fa fa-upload"></i> Importar ip</a>                

                          <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-transacciones"><i class="fa fa-upload"></i> Importar transacciones</a>

                          <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-calendario"><i class="fa fa-upload"></i> Importar calendario</a>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div> {{-- col-md-12 --}}
        </div>

    </section>

</div>

<div id="upload-donantes" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importdonantes', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir donantes</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-tarjetas" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importtarjetas', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir tarjetas de creditos</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-ip" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importips', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir ip al sistemas</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-transacciones" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importtransacciones', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir transacciones</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-calendario" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importcalendario', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir calendario</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

@endsection
