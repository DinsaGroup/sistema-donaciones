<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ url('img/user/'. Auth::user()->photo ) }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>{{ Auth::user()->name }}</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        {{-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form> --}}
        <!-- /.search form -->

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENU PRINCIPAL</li>

            <li class="@if (array_get($menu,'level_1') === 'dashboard') active  @endif treeview">
              <a href="{{ url('ControlPanel') }}">
                <i class="fa fa-dashboard"></i> <span>DASHBOARD</span></i>
              </a>
            </li>

            <li class="@if (array_get($menu,'level_1') === 'donantes') active  @endif treeview">
              <a href="{{ url('ControlPanel/donantes') }}">
                <i class="fa fa-users"></i>
                <span>DONANTES</span>
                {{-- <span class="label label-primary pull-right">4</span> --}}
              </a>
            </li>

            <li class="@if (array_get($menu,'level_1') === 'tarjetas') active  @endif treeview">
              <a href="{{ url('ControlPanel/tarjetas') }}">
                <i class="fa fa-credit-card"></i>
                <span>TARJETAS</span>
                {{-- <span class="label label-primary pull-right">4</span> --}}
              </a>
            </li>

            <li class="@if (array_get($menu,'level_1') === 'transacciones') active  @endif treeview">
              <a href="{{ url('ControlPanel/transacciones') }}">
                <i class="fa fa-exchange"></i>
                <span>DONACIONES</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('ControlPanel/donaciones/aprobadas') }}"><i class="fa fa-check"></i> Aprobadas</a></li>
                <li><a href="{{ url('ControlPanel/donaciones/rechazadas') }}"><i class="fa fa-ban"></i> Rechazadas</a></li>
                <li><a href="{{ url('ControlPanel/donaciones/revertidas') }}"><i class="fa fa-undo"></i> Revertidas</a></li>
              </ul>
            </li>

            <li class="@if (array_get($menu,'level_1') === 'calendario') active  @endif treeview">
              <a href="{{ url('ControlPanel/calendario') }}">
                <i class="fa fa-calendar"></i>
                <span>CALENDARIO</span>
                {{-- <i class="fa fa-angle-left pull-right"></i> --}}
              </a>
            </li>

            <li class="@if (array_get($menu,'level_1') === 'campaigns') active  @endif treeview">
              <a href="{{ url('ControlPanel/campaigns') }}">
                <i class="fa fa-bullhorn"></i>
                <span>CAMPAÑAS</span>
                {{-- <span class="label label-primary pull-right">4</span> --}}
              </a>
            </li>

            <li class="@if (array_get($menu,'level_1') === 'reportes') active  @endif treeview">
              <a href="{{ url('ControlPanel/reportes') }}">
                <i class="fa fa-flag"></i>
                <span>REPORTES</span>
              </a>
            </li>

            {{-- <li class="@if (array_get($menu,'level_1') === 'administradores') active  @endif treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>ADMINISTRACIÓN</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('ControlPanel/administradores') }}"><i class="fa fa-group"></i> Staff administradores</a></li>
                <li><a href="{{ url('ControlPanel/roles-administrativos') }}"><i class="fa fa-reorder"></i> Roles administrativos</a></li>
                <li><a href="{{ url('ControlPanel/ipaccess') }}"><i class="fa fa-desktop"></i> Accesos IP</a></li>
              </ul>
            </li> --}}

            <li class="@if (array_get($menu,'level_1') === 'configuraciones') active  @endif treeview">
              <a href="{{ url('ControlPanel/configuraciones') }}">
                <i class="fa fa-edit"></i> <span>CONFIGURACIONES</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('ControlPanel/tipo-donantes') }}"><i class="fa fa-calendar"></i> Tipo donaciones</a></li>
                <li><a href="{{ url('ControlPanel/monto-donaciones') }}"><i class="fa fa-money"></i> Monto de donaciones</a></li>
                <li><a href="{{ url('ControlPanel/paises') }}"><i class="fa fa-globe"></i> Paises del sistema</a></li>
                <li><a href="{{ url('ControlPanel/monedas') }}"><i class="fa fa-usd"></i> Monedas del sistema</a></li>
                <li><a href="{{ url('ControlPanel/administradores') }}"><i class="fa fa-group"></i> Staff administradores</a></li>
                <li><a href="{{ url('ControlPanel/roles-administrativos') }}"><i class="fa fa-reorder"></i> Roles administrativos</a></li>
                {{-- <li><a href="{{ url('ControlPanel/ipaccess') }}"><i class="fa fa-desktop"></i> Accesos IP</a></li> --}}
              </ul>
            </li>

            {{--
            <li class="treeview">
              <a href="#">
                <i class="fa fa-table"></i> <span>Tables</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
                <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
              </ul>
            </li>

            <li>
              <a href="pages/calendar.html">
                <i class="fa fa-calendar"></i> <span>Calendar</span>
                <small class="label pull-right bg-red">3</small>
              </a>
            </li>

            <li>
              <a href="pages/mailbox/mailbox.html">
                <i class="fa fa-envelope"></i> <span>Mailbox</span>
                <small class="label pull-right bg-yellow">12</small>
              </a>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-folder"></i> <span>Examples</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
                <li><a href="pages/examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
                <li><a href="pages/examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
                <li><a href="pages/examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
                <li><a href="pages/examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
                <li><a href="pages/examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
                <li><a href="pages/examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-share"></i> <span>Multilevel</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                <li>
                  <a href="#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                    <li>
                      <a href="#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
              </ul>
            </li> --}}


            
            
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>