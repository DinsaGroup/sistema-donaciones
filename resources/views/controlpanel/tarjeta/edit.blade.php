@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">

            <h1>{{ $title }} <a href="{{ url('ControlPanel/tarjetas') }}" class="btn btn-danger pull-right btn-sm"><i class="fa fa-times"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

            {!! Form::model($creditCard, [
                    'method' => 'PATCH',
                    'url' => ['ControlPanel/tarjetas', $creditCard->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}
                {{ csrf_field() }}
            <div class="">

                <div class="form-group">
                        <div class="col-md-4">
                            {!! Form::hidden('url',$url, ['class' => 'form-control', 'required' => 'required']) !!}
                        </div>
                    </div>
            
                <div class="form-group">
                        {!! Form::label( 'user_id', 'DONANTE : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-4">
                            {!! Form::select( 'user_id', $donantes, null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'SELECCIONAR DONANTE', 'disabled'=>'disable' ]) !!}
                            {!! Form::hidden('user_code',$creditCard->user->id) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('ico', 'TIPO TARJETA : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-4">
                            {!! Form::select('ico',['visa.png'=>'VISA','mastercard.png'=>'MASTERCARD','american-express.png'=>'AMERICAN EXPRESS'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'SELECCIONAR']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('number', 'NUMERO DE TARJETA : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-4">
                            {!! Form::text('number', $numberCard, ['class' => 'form-control only_integer', 'required' => 'required', 'placeholder'=>'4111 1111 1111 1111']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('name', 'NOMBRE SOBRE LA TARJETA : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-4">
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'NOMBRE SOBRE LA TARJETA']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('month', 'VENCE : ', ['class' => 'control-label col-md-3']) !!}

                        <div class="col-md-2">
                            {!! Form::select('month',['01'=>'01 Enero', '02'=>'02 Febrero', '03'=>'03 Marzo', '04'=>'04 Abril', '05'=>'05 Mayo', '06'=>'06 Junio', '07'=>'07 Julio', '08'=>'08 Agosto', '09'=>'09 Septiembre', '10'=>'10 Octubre', '11'=>'11 Noviembre', '12'=>'12 Diciembre'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'MES']) !!}
                        </div>
                        <div class="col-md-2">
                            {!! Form::select('year',['2025'=>'2025','2024'=>'2024','2023'=>'2023','2022'=>'2022', '2021'=>'2021', '2020'=>'2020', '2019'=>'2019', '2018'=>'2018', '2017'=>'2017'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'AÑO']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('cvc', 'CVC : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-1">
                            {!! Form::text('cvc', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'421']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('default', 'PREDETERMINADA : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-4">
                            @if($creditCard->default)
                            {!! Form::select('default',['true'=>'Convertir en predeterminada','false'=>'No es predeterminada'], 'true', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'SELECCIONAR']) !!}
                            @else
                            {!! Form::select('default',['true'=>'Convertir en predeterminada','false'=>'No es predeterminada'], 'false', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'SELECCIONAR']) !!}
                            @endif
                        </div>
                    </div>
                
                    
                <div class="form-group">
                    <div class="col-sm-4 col-sm-offset-3">
                        {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>
                </div>

            {!! Form::close() !!}

            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection