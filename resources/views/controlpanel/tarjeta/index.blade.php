@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} 

                <a href="{{ url('ControlPanel/tarjetas/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-credit-card" style="margin-right: 10px;"></i> Agregar tarjeta</a>
                <a href="{{ url('ControlPanel/reporte/tarjetas/') }}" class="btn btn-primary pull-right btn-sm" style="margin: 0 10px;" target="_blank"><i class="fa fa-print"></i> Imprimir PDF</a>
            </h1>         
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
           
        </section>

        <section class="content">
            <div class="row">
                
                <div class="col-md-12">
                    <div class="box"> <!-- comienza el contenido de la seccion -->

                        <div class="box-header">
                          <h3 class="box-title"><i class="fa fa-ccard"></i> Listado de tarjetas</h3>
                        </div><!-- /.box-header -->

                        <div class="box-body">
                            @if(Auth::user()->can('editar-tarjeta'))
                            
                                <table id="transaction_history" class="table table-striped table-bordered table-hover dataTables"> {{-- update --}}
                                    <thead>
                                        <tr>
                                            <th class="text-center">ID</th>
                                            <th class="text-center">DONANTE</th>
                                            <th class="text-center">NUMERO</th>
                                            <th class="text-center">NOMBRE SOBRE LA TARJETA</th>
                                            <th class="text-center">VALIDA HASTA</th>
                                            <th class="text-center">POR DEFECTO</th>
                                            <th class="text-center">CREADA</th>
                                            <th class="text-center"><i class="fa fa-sort-desc"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- */$x=0;/* --}}
                                        @foreach($creditCards as $item)
                                            {{-- */$x++;/* --}}
                                            <tr class="gradeA"> {{-- update --}}
                                                <td class="text-center">{{ $item->id }}</td>
                                                <td >
                                                    <a href="{{ url('ControlPanel/donantes/'. $item->user->id) }}" > 
                                                    <img src="{{ url('img/user/'.$item->user->photo) }}" class="user-image">{{ $item->user->name }}
                                                    </a>
                                                </td>
                                                <td class="text-center"> <img src="{{ url('img/credit/'.$item->ico ) }}" class="ccard" /> **** **** **** {{ $item->lastdigit }}</td>
                                                <td >{{ $item->name }}</td>
                                                <td class="text-center">{{ $item->month }} / {{ $item->year }}</td>
                                                <td class="text-center">@if($item->default) 
                                                            <i class="fa fa-star"></i> Predeterminada 
                                                        {{-- @else 
                                                            <a href="{{ url('ControlPanel/tarjetas/predeterminada/'.$item->id) }}"><i class="fa fa-star-o"></i> Cambiar a predeterminada</a> --}}
                                                        @endif</td>
                                                <td class="text-center">{{ $item->created_at }}</td>
                                                <td  class="text-center">
                                                    <a href="{{ url('ControlPanel/tarjetas/' . $item->id . '/edit') }}" class="details">
                                                        <i class="fa fa-pencil"></i>
                                                    </a> 
                                                    
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination"></div>
                            @else
                            
                                <table id="transaction_history" class="table table-striped table-bordered table-hover dataTables"> {{-- update --}}
                                    <thead>
                                        <tr>
                                            <th class="text-center">ID</th>
                                            <th class="text-center">DONANTE</th>
                                            <th class="text-center">NUMERO</th>
                                            <th class="text-center">NOMBRE SOBRE LA TARJETA</th>
                                            <th class="text-center">VALIDA HASTA</th>
                                            <th class="text-center">POR DEFECTO</th>
                                            <th class="text-center">CREADA</th>
                                            {{-- <th class="text-center"><i class="fa fa-sort-desc"></i></th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- */$x=0;/* --}}
                                        @foreach($creditCards as $item)
                                            {{-- */$x++;/* --}}
                                            <tr class="gradeA"> {{-- update --}}
                                                <td class="text-center">{{ $item->id }}</td>
                                                <td class="text-center">
                                                    <a href="{{ url('ControlPanel/donantes/'. $item->user->id) }}" > 
                                                    <img src="{{ url('img/user/'.$item->user->photo) }}" class="user-image">{{ $item->user->name }}
                                                    </a>
                                                </td>
                                                <td class="text-center"> <img src="{{ url('img/credit/'.$item->ico ) }}" class="ccard" /> **** **** **** {{ $item->lastdigit }}</td>
                                                <td class="text-center">{{ $item->name }}</td>
                                                <td class="text-center">{{ $item->month }} / {{ $item->year }}</td>
                                                <td class="text-center">@if($item->default) 
                                                            <i class="fa fa-star"></i> Predeterminada 
                                                        {{-- @else 
                                                            <a href="{{ url('ControlPanel/tarjetas/predeterminada/'.$item->id) }}"><i class="fa fa-star-o"></i> Cambiar a predeterminada</a> --}}
                                                        @endif</td>
                                                <td class="text-center">{{ $item->created_at }}</td>
                                                {{-- <td  class="text-center">
                                                    <a href="{{ url('ControlPanel/tarjetas/' . $item->id . '/edit') }}" class="details">
                                                        <i class="fa fa-pencil"></i>
                                                    </a> 
                                                    
                                                </td> --}}
                                            </tr>
                                        @endforeach
                                    </tbody>

                                    <tfoot>
                                        <tr>
                                            <th class="text-center">ID</th>
                                            <th class="text-center">DONANTE</th>
                                            <th class="text-center">NUMERO</th>
                                            <th class="text-center">NOMBRE SOBRE LA TARJETA</th>
                                            <th class="text-center">VALIDA HASTA</th>
                                            <th class="text-center">POR DEFECTO</th>
                                            <th class="text-center">CREADA</th>
                                            {{-- <th class="text-center"><i class="fa fa-sort-desc"></i></th> --}}
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="pagination"></div>
                            @endif
                        </div>

                    </div> <!-- Termina el contenido de la seccion --> 
                </div> <!-- Termina el ibox --> 
            
            </div>
        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')
<!-- page script -->
    <script type="text/javascript">
      $(function () {
        // $("#transaction_history").dataTable();
        $('#transaction_history').dataTable({
          "bSortClasses": 'sorting_3',
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "aaSorting": [[0,'desc']],
          "iDisplayLength": 100,
          "aLengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]]
        });
      });
    </script>
@endsection