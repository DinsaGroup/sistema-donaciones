
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Notificacion al usuario</title>
    <style type="text/css">
        #outlook a {
            padding: 0;
        } /* Force Outlook to provide a "view in browser" message */
        .ReadMsgBody {
            width: 100%;
        }
        .ExternalClass {
            width: 100%;
        } /* Force Hotmail to display emails at full width */
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        } /* Force Hotmail to display normal line spacing */
        body, table, td, p, a, li, blockquote {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            text-align: center;
            font-family: Tahoma,Geneva,sans-serif;
            color: #505050;
        } /* Prevent WebKit and Windows mobile changing default text sizes */
        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        } /* Remove spacing between tables in Outlook 2007 and up */
        img {
            -ms-interpolation-mode: bicubic;
        } /* Allow smoother rendering of resized image in Internet Explorer */
        body {
            margin: 0;
            padding: 0;
        }
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }
        table {
            border-collapse: collapse !important;
        }
        td{
            padding: 5px 10px;
        }
        body, #bodyTable, #bodyCell {
            height: 100% !important;
            margin: 0;
            padding: 0;
            width: 100% !important;
        }

        #bodyCell {
            padding: 0px;
        }
        #templateContainer {
            width: 600px;
        }

        body, #bodyTable {
            background-color: #FFFFFF;
        }

        #bodyCell {
        }
        #templateContainer {
            border: 1px solid #BBBBBB;
        }
        h1 {
            color: #202020 !important;
            display: block;
            font-family: Tahoma, Geneva, sans-serif;
            font-size: 26px;
            font-style: normal;
            font-weight: bold;
            line-height: 100%;
            letter-spacing: normal;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            text-align: left;
        }

        h2 {
            color: #404040 !important;
            display: block;
            font-family: Tahoma, Geneva, sans-serif;
            font-size: 20px;
            font-style: normal;
            font-weight: bold;
            line-height: 100%;
            letter-spacing: normal;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            text-align: left;
        }

        h3 {
            color: #f47321 !important;
            display: block;
            font-family: Tahoma, Geneva, sans-serif;
            font-size: 14px;
            font-weight: normal;
            line-height: 100%;
            letter-spacing: normal;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            text-align: center;
        }

        h4 {
            color: #043783;
            display: block;
            font-family: Tahoma, Geneva, sans-serif;
            font-size: 22px;
            font-weight: bold;
            line-height: 100%;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            text-align: center;
        }
        h5, .h5 {
            color: #043783;
            display: block;
            font-family: Tahoma, Geneva, sans-serif;
            font-size: 18px;
            font-weight: bold;
            line-height: 100%;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            text-align: center;
        }
        h6, .h6 {
            font-family: Tahoma, Geneva, sans-serif;
            font-size: 18px;
            font-weight: bold;
            line-height: 100%;
            color: #f47321 !important;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            display: block;
            text-align: center;
        }
        .textoblock_orange {
            font-family: Tahoma, Geneva, sans-serif;
            font-size: 18px;
            font-weight: bold;
            line-height: 100%;
            color: #f47321 !important;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            display: block;
            text-align: center;
        }

        #templatePreheader {
            background-color: #f47321 !important;
        }

        .preheaderContent {
            color: #000000;
            font-family: Tahoma, Geneva, sans-serif;
            font-size: 10px;
            line-height: 100%;
            text-align: center;
            background-color: #f47321;
        }

        .preheaderContent a:link, .preheaderContent a:visited, /* Yahoo! Mail Override */ .preheaderContent a .yshortcuts /* Yahoo! Mail Override */ {
            color: #606060;
            font-weight: normal;
            text-decoration: underline;
        }

        #templateHeader {
        }

        .headerContent {
            font-family: Tahoma, Geneva, sans-serif;
            text-align: center;
            height:75px;
            padding: 10px 0;
            background-color: #6c3483;
            margin-bottom: 25px;
            float: left;
            width: 100%;
        }

        .headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */ {
            color: #6c3483;
            font-weight: normal;
            text-decoration: none;
        }
        #headerImage {
            height: 75px;
        }

        #templateBody {
            background-color: #FFFFFF;
        }

        .bodyContent {
            color: #505050;
            font-family: Tahoma, Geneva, sans-serif;
            font-size: 14px;
            line-height: 150%;
            padding-top: 0px;
            padding-right: 20px;
            padding-bottom: 20px;
            padding-left: 20px;
        }

        .bodyContent a:link, .bodyContent a:visited, /* Yahoo! Mail Override */ .bodyContent a .yshortcuts /* Yahoo! Mail Override */ {
            color: #6c3483;
            font-weight: normal;
            text-decoration: none;
        }
        .bodyContent img {
            display: inline;
            height: auto;
            max-width: 560px;
        }

        #templateFooter {
            background-color: #FFFFFF;
            border-top: 1px solid #FFFFFF;
        }

        .footerContent {
            color: #808080;
            font-family: Tahoma, Geneva, sans-serif;
            font-size: 10px;
            line-height: 150%;
            padding-top: 20px;
            padding-right: 20px;
            padding-bottom: 20px;
            padding-left: 20px;
            text-align: center;
        }

        .user-image{
            border-radius: 50%;
            width: 70px;
        }

        .footerContent a:link, .footerContent a:visited, /* Yahoo! Mail Override */ .footerContent a .yshortcuts, .footerContent a span /* Yahoo! Mail Override */ {
            color: #606060;
            font-weight: normal;
            text-decoration: underline;
        }
        #monkeyRewards img {
            max-width: 190px;
        }
        .textnaranja {
            color: #f47321 !important;
        }
        .sloganblocks {
            color: #f47321;
            font-size: 20px;
        }
        .linkwebsite_f {
            text-align: right !important;
        }
        .linkwebsite_f a {
            text-align: right !important;
            color: #f47321 !important;
            font-size: 20px !important;
            text-decoration: none !important;
        }
        .footerContent a {
            font-size: 10px;
            text-decoration: none !important;
            color: #727272;
        }


        @media only screen and (max-width: 480px) {
        body, table, td, p, a, li, blockquote {
            -webkit-text-size-adjust: none !important;
        } /* Prevent Webkit platforms from changing default text sizes */
        body {
            width: 100% !important;
            min-width: 100% !important;
        } /* Prevent iOS Mail from adding padding to the body */
        #bodyCell {
            padding: 10px !important;
        }

        #templateContainer {
            max-width: 600px !important;
            width: 100% !important;
        }

        h1 {
            font-size: 24px !important;
            line-height: 100% !important;
        }

        h2 {
            font-size: 20px !important;
            line-height: 100% !important;
        }

        h3 {
            font-size: 18px !important;
            line-height: 100% !important;
        }

        h4 {
            font-size: 16px !important;
            line-height: 100% !important;
        }
        h6 {
            font-size: 12px !important;
            line-height: 100% !important;
            color: #f47321 !important;
        }
        /* ======== Header Styles ======== */

        #templatePreheader {
            display: none !important;
        } /* Hide the template preheader to save space */

        #headerImage {
            height: auto !important;
            max-width: 600px !important;
            width: 100% !important;
        }

        .headerContent {
            font-size: 20px !important;
            line-height: 125% !important;
        }

        .bodyContent {
            font-size: 18px !important;
            line-height: 125% !important;
        }

        .footerContent {
            font-size: 14px !important;
            line-height: 115% !important;
        }
        .footerContent a {
            display: block !important;
        } /* Place footer social and utility links on their own lines, for easier access */
        .imgbody {
            width: 50%;
            height: auto;
        }
        }
    </style>
</head>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
  <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
    <tr>
      <td align="center" valign="top" id="bodyCell"><!-- BEGIN TEMPLATE // -->
        
        <table border="0" align="center" cellpadding="0" cellspacing="0" id="templateContainer">

          <tr>
            <td align="center" valign="top"><!-- BEGIN HEADER // -->
              
              <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                <tr>
                    <td valign="top" class="headerContent">
                        <img src="https://www.fcmujeres.org/wp-content/themes/fcmujeres2016/images/logo.png" id="headerImage" />
                    </td>
                </tr>
              </table>
              
              <!-- // END HEADER --></td>
          </tr>
          <tr>
            <td align="center" valign="top"><!-- BEGIN BODY // -->
              
              <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                <tr>
                    <td valign="top" class="bodyContent" mc:edit="body_content">
                  
                        <h5 class="h5" style="margin-bottom: 25px; width: 100%;">GRACIAS POR SU DONATIVO</h5>

                        @if($transaction->responseCodeDescription== 'APROBADA' )
                            <p>Agradecemos el apoyo<br>A continuación le presentamos el detalle de la donación realizada</p>
                        @else
                            <p style="color:red">Hubo un problema con su donativo, favor comunicarse con nosotros <br>Telf.: (+505) 2254 4981 / (+505) 2254 4983 Email: info@fcmujeres.org </p>
                        @endif


                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                            <tr>
                                <td style="text-align: right;" width="50%">Transacción no.</td>
                                <td style="text-align: left" width="50%">{{ $transaction->id }}</td>
                            </tr>

                            <tr>
                                <td style="text-align: right;" width="50%">
                                    <img src="http://portal.fcmujeres.org/img/user/{{ $user->photo }}" class="user-image" />
                                </td>
                                <td style="text-align: left" width="50%">
                                    
                                    <span style="text-transform: uppercase; float: left; margin: 5px 0 0 5px; width: 100%">
                                        {{ $user->name }}
                                    </span>
                                    <span style="float: left; margin-left: 5px; width: 100%">
                                        <a href="mailto:{{ $user->email }}" class="link">{{ $user->email }}</a>
                                    </span>
                                    <span style="float: left; margin-left: 5px; width: 100%">
                                        @if($user->house != "") {{ $user->house }} Casa @endif 
                                        @if($user->office != "") | {{ $user->office }} Oficina @endif 
                                        @if($user->phone != "") | {{ $user->phone }} Movil @endif 
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="50%">Tarjeta</td>
                                <td style="text-align: left" width="50%">
                                    <img src="http://portal.fcmujeres.org/img/credit/{{ $transaction->creditcard->ico}}" width="35" style="float: left; margin: 0 5px 0 0" />
                                    <span style="text-transform: uppercase; float: left; margin: 5px 0 0 5px;">
                                        **** {{ $transaction->creditcard->lastdigit }}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="50%">País donante</td>
                                <td style="text-align: left" width="50%">{{ $user->country->name }}</td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="50%">Tipo de donativo</td>
                                <td style="text-align: left" width="50%">{{ $transaction->donationtype->name }}</td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="50%">Campaña</td>
                                <td style="text-align: left" width="50%">{{ $transaction->campaign->name }}</td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="50%">Monto donado</td>
                                <td style="text-align: left" width="50%">U$ {{ number_format($transaction->amount,2,'.',',') }}</td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="50%">Código de autorización</td>
                                <td style="text-align: left" width="50%">{{ $transaction->auth_code }}</td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="50%">Número de referencia</td>
                                <td style="text-align: left" width="50%">{{ $transaction->referenceNumber }}</td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="50%">Código de confirmación</td>
                                <td style="text-align: left" width="50%">{{ $transaction->transactionid }}</td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="50%">Fecha</td>
                                <td style="text-align: left" width="50%">{{ $transaction->created_at }} </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="50%">Próximo intento</td>
                                <td style="text-align: left" width="50%">{{ $nextProcess }} </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="50%">Estatus</td>
                                <td style="text-align: left" width="50%">{{ $status }} </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="50%">Observación</td>
                                <td style="text-align: left" width="50%">La transacción ha sido aprobada <b>{{ $notes }} </b></td>
                            </tr>
                        </table>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; margin: 0 auto; width: 140px">
                            <span>Encuentranos en</span>
                        
                            <span style="float: left; margin: 5px">           
                            <a target="_blank" href="https://www.facebook.com/fcmujeres?fref=ts">
                                <img src="https://www.fcmujeres.org/wp-content/uploads/2018/05/facebook.png" alt="Facebook" border="0" width="25" height="25" style="display:block; border:none; outline:none; text-decoration:none;">
                            </a>
                            </span>
                            <span style="float: left; margin: 5px">
                            <a target="_blank" href="http://twitter.com/fcmujeres">
                                <img src="https://www.fcmujeres.org/wp-content/uploads/2018/05/twitter.png" alt="Twitter" border="0" width="25" height="25" style="display:block; border:none; outline:none; text-decoration:none;">
                            </a>
                            </span>
                            <span style="float: left; margin: 5px">
                            <a target="_blank" href="https://www.youtube.com/channel/UC8YbamMkKjjqg5WfFyvYBQA">
                                <img src="https://www.fcmujeres.org/wp-content/uploads/2018/05/youtube.png" alt="Youtube" border="0" width="25" height="25" style="display:block; border:none; outline:none; text-decoration:none;">
                            </a>
                            </span>
                            <span style="float: left; margin: 5px">
                            <a target="_blank" href="http://issuu.com/fcmujeres">
                                <img src="https://www.fcmujeres.org/wp-content/uploads/2018/05/issuu.png" alt="RSS" border="0" width="25" height="25" style="display:block; border:none; outline:none; text-decoration:none;">
                            </a>
                            </span>
                        </div>
                    </td>
                </tr>
              </table>
              
              <!-- // END BODY --></td>
          </tr>
          <tr>
            <td align="center" valign="top"><!-- BEGIN FOOTER // -->
              
              <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter">
                <tr>
                    <td valign="top" class="footerContent" mc:edit="footer_content00">
                        
                    </td>
                </tr>
                <tr>

                    <td align="center" class="footerContent">
                        <a href="https://fcmujeres.org/" target="_blank"><h5>Fondo Centroamericano de Mujeres</h5></a>
                        Este mensaje fue enviado de forma automatica por nuestro sistema para notificarle que se ha ralizado una donación.<br> Para atención al cliente, contactanos a info@fcmujeres.org
                    </td>
                </tr>
              </table>
              
              <!-- // END FOOTER --></td>
          </tr>
        </table>
        
        <!-- // END TEMPLATE --></td>
    </tr>
  </table>
</center> 
</body>
</html>