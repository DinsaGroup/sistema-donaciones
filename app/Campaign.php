<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class campaign extends Model
{
    use SoftDeletes;

    protected $table = 'campaigns';

    protected $fillable = [ 'name', 'description', 'active' ];

    public function transactions(){
    	return $this->hasMany('App\Transaction');
    }

    public function schedules(){
    	return $this->hasMany('App\Schedule');
    }
    
}
