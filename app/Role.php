<?php

namespace App;

use Zizaco\Entrust\EntrustRole;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends EntrustRole
{
	use SoftDeletes;
  	
  	protected $fillable = ['name', 'display_name', 'description'];

  	protected $dates = ['deleted_at'];

	public function users(){
	        return $this->belongsToMany('App\User');
	}

	public function permissions() {
		return $this->belongsToMany('App\Permission');
	}

}
