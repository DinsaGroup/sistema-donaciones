<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;

    protected $table = 'countries';

    protected $fillable = [ 'code1', 'code2', 'ext','name' ];

    public function users(){
        return $this->hasMany('App\User');
    }

}
