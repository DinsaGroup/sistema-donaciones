<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
 	use SoftDeletes;

    protected $table = 'transactions';

    protected $fillable = [ 'process_type', 'donation_type_id','creditcard_id', 'campaign_id','amount', 'transactionid','ip_id','response_code','auth_code','response','avsresponse','cvvresponse','orderid_response','type','time_response','amount_response','purshamount','hash','referenceNumber', 'responseCode', 'responseCodeDescription', 'systemTraceNumber','status', 'comments' ];

    public function creditcard(){
        return $this->belongsTo('App\CreditCard');
    }

    public function campaign(){
        return $this->belongsTo('App\Campaign');
    }

    public function donationtype(){
        return $this->belongsTo('App\DonationType','donation_type_id','id');
    }

    public function ip(){
    	return $this->belongsTo('App\Ip');
    }
    
    public function reverse(){
        return $this->hasOne('App\Reverse');
    }
}
