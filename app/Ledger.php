<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ledger extends Model
{
    protected $table = 'ledgers';

    protected $fillable = [ 'donants', 'users', 'donationtypes','donationamounts','countries','currencies','creditcards','creditcard_actives','creditcard_inactives','transactions','transaction_oks', 'transaction_errors', 'campaigns', 'schedules', 'schedule_active', 'schedule_pending', 'lastdonation', 'month','year', 'total','exchange_rate'];
}
