<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DonationType extends Model
{
    use SoftDeletes;

    protected $table = 'donation_types';

    protected $fillable = [ 'name', 'description', 'days'];

    public function transactions(){
        return $this->hasMany('App\Transaction');
    }

    public function schedules(){
    	return $this->hasMany('App\Schedule');
    }
}
