<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait { restore as private restoreA; }
    use SoftDeletes { restore as private restoreB; }

    protected $fillable = ['name', 'email', 'password','photo','house','office','phone','country_id', 'boletin', 'comments'];

    protected $hidden = ['password', 'remember_token',];

    public function role(){
        return $this->belongsToMany('App\Role');
    }

    public function creditcards(){
        return $this->hasMany('App\CreditCard');
    }

    public function ips(){
        return $this->hasMany('App\Ip');
    }

    public function logfiles(){
        return $this->hasMany('App\Logfile');
    }

    public function restore(){
        $this->restoreA();
        $this->restoreB();
    }

    public function country(){
        return $this->belongsTo('App\Country');
    }

    public function emails(){
        return $this->hasMany('App\Email');
    }
}
