<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditCard extends Model
{
    use SoftDeletes;

    protected $table = 'creditcards';
  	
  	protected $fillable = ['name', 'number', 'month', 'year', 'cvc','lastdigit', 'ico', 'user_id', 'default', 'status'];

  	protected $dates = ['deleted_at'];

	public function user(){
	        return $this->belongsTo('App\User');
	}

	public function transactions(){
		return $this->hasMany('App\Transaction');
	}

	public function schedule(){
		return $this->hasMany('App\Schedule');
	}
}
