<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Currency;

class MonedaController extends Controller
{
    public function index()
    {
        try{

            $currencies = Currency::all();

            return view('controlpanel.currency.index',[
                'title' => 'Monedas',
                'currencies' => $currencies,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function create()
    {
        try{

            $currencies = Currency::all();

            return view('controlpanel.currency.create',[
                'title' => 'Crear nuevo',
                'currencies' => $currencies,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function store(Request $request)
    {
        try{

            $new = new Currency;
            $new->symbol = $request->symbol;
            $new->symbol2 = $request->symbol2;
            $new->name = $request->name;
            $new->description = $request->description;
            $new->save();

            \Session::flash('success_message','¡El último registo se guardo correctamente!');
            return redirect('ControlPanel/configuraciones');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        try{

            $currencies = Currency::find($id);

            return view('controlpanel.currency.edit',[
                'title' => 'Editar registro',
                'currencies' => $currencies,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function update(Request $request, $id)
    {
        try{

            $update = Currency::find($id);
            $update->symbol = $request->symbol;
            $update->symbol2 = $request->symbol2;
            $update->name = $request->name;
            $update->description = $request->description;
            $update->update();

            \Session::flash('success_message','¡El registro fue actualizado!');
            return redirect('ControlPanel/configuraciones');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function destroy($id)
    {
        try{

            $currencies = Currency::find($id);
            $currencies->delete();

            \Session::flash('success_message','¡El registro fue borrado!');
            return redirect('ControlPanel/configuraciones');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function menu(){
        $menu = [
                'level_1' => 'configuraciones',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
