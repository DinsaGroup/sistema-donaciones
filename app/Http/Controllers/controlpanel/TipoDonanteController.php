<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\DonationType;

class TipoDonanteController extends Controller
{
    public function index()
    {
        try{

            $donationTypes = DonationType::all();

            return view('controlpanel.donation_type.index',[
                'title' => 'Tipos de donaciones',
                'donationTypes' => $donationTypes,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function create()
    {
        try{

            $donationTypes = DonationType::all();

            return view('controlpanel.donation_type.create',[
                'title' => 'Crear nuevo',
                'donationTypes' => $donationTypes,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function store(Request $request)
    {
        try{

            $value = strtolower($request->name);
            $value = ucfirst($value);

            $donationTypes = DonationType::where('name',$value)->get();

            if(count($donationTypes)){
                \Session::flash('error_message','¡El registro ya existe, favor verifique!');
                return redirect('ControlPanel/donationTypes-administrativos');
            }

            $new = new DonationType;
            $new->name = ucfirst(strtolower($request->name));
            $new->description = ucfirst(strtolower($request->description));
            $new->days = $request->days;
            $new->save();

            \Session::flash('success_message','¡El último registo se guardo correctamente!');
            return redirect('ControlPanel/tipo-donantes');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        try{

            $donationType = DonationType::find($id);

            return view('controlpanel.donation_type.edit',[
                'title' => 'Editar registro',
                'donationType' => $donationType,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function update(Request $request, $id)
    {
        try{

            $update = DonationType::find($id);
            $update->name = ucfirst(strtolower($request->name));
            $update->description = ucfirst(strtolower($request->description));
            $update->days = $request->days;
            $update->update();

            \Session::flash('success_message','¡El registro fue actualizado!');
            return redirect('ControlPanel/tipo-donantes');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function destroy($id)
    {
        try{

            $donationTypes = DonationType::find($id);
            $donationTypes->delete();

            \Session::flash('success_message','¡El registro fue borrado!');
            return redirect('ControlPanel/tipo-donantes');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function menu(){
        $menu = [
                'level_1' => 'configuraciones',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
