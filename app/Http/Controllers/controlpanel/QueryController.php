<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Currency;
use App\DonationAmount;

class QueryController extends Controller
{
    public function user($id){
    	$user = User::find($id);
    	return response()->json($user->creditcards);
    }

    public function email($email){
    	$user = User::where('email',$email)->get();
    	if(count($user)){
	    	return response()->json('true');
    	}else{
	    	return response()->json('false');
    	}
    }

    public function amounts($currency_id){
        $donationAmounts = DonationAmount::where('currency_id',$currency_id)->get();

        $Array=array();

        foreach ($donationAmounts as $item) {
            $Array[$item->id] = $item->currency->symbol .' '. number_format($item->amount,2,'.',',');
        }

        return response()->json($Array);

    }
}
