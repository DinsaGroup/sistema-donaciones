<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\DonationAmount;
use App\DonationType;
use App\User;
use App\Schedule;
use App\Campaign;
use App\CreditCard;
use App\Currency;

use SoapClient;
use Carbon\Carbon;

class CalendarioController extends Controller
{
    
    public function index(){
        try{

            $schedules = Schedule::all();

            return view('controlpanel.calendario.index',[
                'title' => 'Calendario de donaciones',
                'schedules' => $schedules,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create(){

        try{

            $donantes = User::whereHas(
                            'roles', function($q){
                                $q->where('name', 'donante');
                            }
                        )->orderBy('name','asc')->get()->pluck('name','id');
            $donationAmounts = $this->donationamounts();
            $currencies = $this->currencies();
            $donationTypes = $this->donationtypes();
            $campaigns = Campaign::all()->pluck('name','id');

            $creditcards = Creditcard::all();
            $tarjetas = array();
            foreach ($creditcards as $item) {
                $tarjetas[$item->id] = ' ****-'.$item->lastdigit.' '.$item->name;
            }

            return view('controlpanel.calendario.create',[
                'title' => 'Programar donación',
                'creditcards' => $tarjetas,
                'donationTypes' => $donationTypes,
                'donationAmounts' => $donationAmounts,
                'currencies' => $currencies,
                'campaigns' => $campaigns,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request){
        
        $amount = DonationAmount::find($request->amount_id);
        $new = new Schedule;
        $new->donation_type_id = $request->donationType_id;
        $new->creditcard_id = $request->creditcard_id;
        $new->campaign_id = $request->campaign_id;
        $new->amount = $amount->amount;
        $new->lastProcess = $request->debit_at;
        $new->nextProcess = $request->debit_at;
        $new->failedAttempts = 0;
        $new->status = 'Activa';
        $new->comments = 'Agregada desde calendario';
        $new->save();
        
        return redirect('ControlPanel/calendario');
    }

    public function edit($id){
        try{

            $schedules = Schedule::find($id);

            $donationAmounts = $this->donationamounts();

            $donationTypes = $this->donationtypes();

            $campaigns = Campaign::OrderBy('name','asc')->get()->pluck('name','id');

            foreach($schedules->creditcard->user->creditcards as $item){
                $tarjetas[$item->id] = '**** **** ****-'.$item->lastdigit.' '.$item->name;
            }

            // $nextProcess = explode(' ',$schedules->nextProcess);
            // $elements = explode('-',$nextProcess[0]);
            // $nextProcess = $elements[0].'/'.$elements[2].'/'.$elements[1];
            // dd($nextProcess);
            return view('controlpanel.calendario.edit',[
                'title' => 'Editar programación',
                'schedule' => $schedules,
                'donationAmounts' => $donationAmounts,
                'donationTypes' => $donationTypes,
                'campaigns' => $campaigns,
                'creditcards'=> $tarjetas,
                // 'nextProcess' => $nextProcess,
                'menu' => $this->menu(),
            ]);

        }catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id){
        try{
            // dd($request);
            $update = Schedule::find($id);

            if($request->amount_id){
                $donationAmount = DonationAmount::find($request->amount_id);
                if($donationAmount->currency_id==1){ // Si la denominacion es cordobas
                    $donation_amount = number_format($donationAmount->amount / $this->exchange_rate_bcn(), 2);
                }else{
                    $donation_amount = number_format($donationAmount->amount,2);
                }   

                $update->amount = $donation_amount;
                
            }


            $update->creditcard_id = $request->creditcard_id;
            $update->donation_type_id = $request->donation_type_id;
            $update->campaign_id = $request->campaign_id;
            $update->status = $request->status;
            $update->nextProcess = $request->nextProcess;
            $update->update();

            \Session::flash('success_message','¡El registro fue actualizado!');
            return redirect('ControlPanel/calendario');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function menu(){
        $menu = [
                'level_1' => 'calendario',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }

    private function donationamounts(){
        $donationAmounts = DonationAmount::all();
        $Array=array();

        foreach ($donationAmounts as $item) {
            $Array[$item->id] = $item->currency->symbol .' '. number_format($item->amount,2,'.',',');
        }

        return $Array;
    }

    private function donationtypes(){
        $donationAmounts = DonationType::all();
        $Array=array();

        foreach ($donationAmounts as $item) {
            $Array[$item->id] = $item->name;
        }

        return $Array;
    }

    private function exchange_rate_bcn(){
        /*
         * CODIGO CLIENTE WSDL DE PHP
         */
        $servicio = "https://servicios.bcn.gob.ni/Tc_Servicio/ServicioTC.asmx?WSDL"; //url del servicio
        $parametros = array(); //parametros de la llamada

        $today = Carbon::now();

        $parametros['Dia'] = $today->day;
        $parametros['Mes'] = $today->month;
        $parametros['Ano'] = $today->year;

        $client = new SoapClient($servicio, $parametros);
        $result = $client->RecuperaTC_Dia($parametros); //llamamos al métdo que nos interesa con los parámetros
        $TasaDiaria = ($result->RecuperaTC_DiaResult);

        if($TasaDiaria>0){
            return $TasaDiaria;
        }else{
            $ledger = Ledger::find(1);
            return $ledger->exchange_rate; 
        }
    }

    private function currencies(){
        $currencies = Currency::all();
        $Array=array();

        foreach ($currencies as $item) {
            $Array[$item->id] = $item->symbol .' '. $item->name;
        }

        return $Array;
    }

}
