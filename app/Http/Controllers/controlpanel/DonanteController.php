<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection as Collection;

use App\User;
use App\Role;
use App\Country;
use App\DonationType;
use App\DonationAmount;
use App\Transaction;
use App\Ledger;


use Image;

class DonanteController extends Controller
{

    public function index()
    {
        try{

            $donantes = User::whereHas(
                            'roles', function($q){
                                $q->where('name', 'donante');
                            }
                        )->get();

            return view('controlpanel.donante.index',[
                'title' => 'Donantes',
                'description' => 'Listado de donantes',
                'donantes' => $donantes,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {
        try{

            return view('controlpanel.donante.create',[
                'title' => 'Crear nuevo',
                'countries' => $this->countries(),
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        try{

            $user = User::where('email',$request->email)->get();

            if(count($user)){
                \Session::flash('error_message','¡El registro ya existe, favor verifique!');
                return redirect()->back();
            }

            $password = $this->generate_password();

            if($request->hasFile('avatar')){
                $avatar = $request->file('avatar');
                $filename = time().'.'.$avatar->getClientOriginalExtension();
                Image::make($avatar)->resize(300,300)->save(public_path('/img/user/'.$filename));

            }else{
                $filename = 'avatar-default.jpg';
            }

            $new = new User;
            $new->name = $request->name;
            $new->email = $request->email;
            $new->password = bcrypt($password);
            $new->country_id = $request->country_id;
            $new->house = $request->house;
            $new->office = $request->office;
            $new->phone = $request->phone;
            $new->boletin = $request->boletin;
            $new->comments = $request->comments;
            $new->photo = $filename;
            $new->save();

            $role = Role::where('name','donante')->first();

            $new->attachRole($role);

            $ledger = Ledger::first();
            $ledger->donants = $ledger->donants + 1;
            $ledger->update();

            \Session::flash('success_message','¡El último registo se guardo correctamente!');
            return redirect('ControlPanel/donantes/'.$new->id);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function show($id)
    {
        try{

            $donante = User::find($id);
            $transactions = array();

            foreach($donante->creditcards as $ccard){
                $transac = Transaction::where('creditcard_id',$ccard->id)->get();
                foreach($transac as $item){
                    $transactions[] = $item;  
                }
            }

            return view('controlpanel.donante.show',[
                'title' => $donante->name,
                'donante' => $donante,
                'transactions' => $transactions,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        try{

            $user = User::find($id);

            return view('controlpanel.donante.edit',[
                'title' => 'Editar registro',
                'user' => $user,
                'countries' => $this->countries(),
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{

            $update = User::find($id);

            if($request->hasFile('avatar')){
                $avatar = $request->file('avatar');
                $filename = time().'.'.$avatar->getClientOriginalExtension();
                Image::make($avatar)->resize(300,300)->save(public_path('/img/user/'.$filename));
                $update->photo = $filename;
            }

            
            $update->name = $request->name;
            $update->email = $request->email;
            empty($request->password) ?  : $update->password = bcrypt($request->password);
            
            $update->country_id = $request->country_id;
            $update->house = $request->house;
            $update->office = $request->office;
            $update->phone = $request->phone;
            $update->comments = $request->comments;
            $update->update();

            \Session::flash('success_message','¡El registro fue actualizado!');
            return redirect('ControlPanel/donantes/'.$id);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        try{

            $user = User::find($id);
            $user->delete();

            $ledger = Ledger::first();
            $ledger->donants = $ledger->donants - 1;
            $ledger->update();

            \Session::flash('success_message','¡El registro fue borrado!');
            return redirect('ControlPanel/donantes');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function menu(){
        $menu = [
                'level_1' => 'donantes',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }

    private function generate_password($length = 25) {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function countries(){
        $countries = Country::all();
        $Array=array();

        foreach ($countries as $item) {
            $Array[$item->id] = $item->code1 .' &nbsp;&nbsp;&nbsp; '. $item->name;
        }

        return $Array;
    }

}
