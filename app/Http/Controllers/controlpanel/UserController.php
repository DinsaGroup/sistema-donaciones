<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Country;
use App\Role;

use Image;

class UserController extends Controller
{

    public function index()
    {
        try{

            $users = User::whereHas(
                            'roles', function($q){
                                $q->where('name', 'superadmin');
                            }
                        )->get();

            return view('controlpanel.user.index',[
                'title' => 'Administradores',
                'users' => $users,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function create()
    {
        try{

            $countries = Country::all()->pluck('name','id');
            $roles = Role::whereNotIn('name',['donante'])->pluck('display_name','id');
            $rol = Role::whereNotIn('name',['donante'])->pluck('display_name', 'id');

            $newRol = array_except($rol, ['1']);

            return view('controlpanel.user.create',[
                'title' => 'Crear nuevo',
                'countries' => $countries,
                'roles' => $roles,
                'newRol' => $newRol,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function store(Request $request)
    {
        try{

            $user = User::where('name',$request->name)->get();

            if(count($user)){
                \Session::flash('error_message','¡El registro ya existe, favor verifique!');
                return redirect('ControlPanel/administradores');
            }

            if($request->hasFile('avatar')){
                $avatar = $request->file('avatar');
                $filename = time().'.'.$avatar->getClientOriginalExtension();
                Image::make($avatar)->resize(300,300)->save(public_path('/img/user/'.$filename));

            }else{
                $filename = 'avatar-default.jpg';
            }

            $new = new User;
            $new->name = $request->name;
            $new->email = $request->email;
            $new->password = bcrypt($request->password);
            $new->country_id = $request->country_id;
            $new->house = $request->house;
            $new->office = $request->office;
            $new->phone = $request->phone;
            $new->photo = $filename;
            $new->save();

            $role = Role::find($request->role_id);

            $new->attachRole($role);

            \Session::flash('success_message','¡El último registo se guardo correctamente!');
            return redirect('ControlPanel/administradores');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        try{

            $user = User::find($id);
            $countries = Country::all()->pluck('name','id');
            $roles = Role::whereNotIn('name',['donante'])->pluck('display_name','id');
            $rol = Role::whereNotIn('name',['donante'])->pluck('display_name', 'id');

            $newRol = array_except($rol, ['1']);

            return view('controlpanel.user.edit',[
                'title' => 'Editar registro',
                'user' => $user,
                'countries' => $countries,
                'roles' => $roles,
                'newRol' => $newRol,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function update(Request $request, $id)
    {
        try{

            $update = User::find($id);

            if($request->hasFile('avatar')){
                $avatar = $request->file('avatar');
                $filename = time().'.'.$avatar->getClientOriginalExtension();
                Image::make($avatar)->resize(300,300)->save(public_path('/img/user/'.$filename));
                $update->photo = $filename;
            }
            
            $update->name = $request->name;
            $update->email = $request->email;
            empty($request->password) ?  : $update->password = bcrypt($request->password);
            
            $update->country_id = $request->country_id;
            $update->house = $request->house;
            $update->office = $request->office;
            $update->phone = $request->phone;
            $update->update();

            \Session::flash('success_message','¡El registro fue actualizado!');
            // return redirect('ControlPanel/administradores');
            return redirect()->back();

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function destroy($id)
    {
        try{

            $user = User::find($id);
            $user->delete();

            \Session::flash('success_message','¡El registro fue borrado!');
            // return redirect('ControlPanel/administradores');
            return redirect()->back();

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function menu(){
        $menu = [
                'level_1' => 'configuraciones',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
