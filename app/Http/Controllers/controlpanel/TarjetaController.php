<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CreditCard;
use App\User;
use App\Ledger;

class TarjetaController extends Controller
{

    public function index()
    {
        try{

            $creditCards = CreditCard::all();

            return view('controlpanel.tarjeta.index',[
                'title' => 'Tarjetas del sistema',
                'creditCards' => $creditCards,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function create()
    {
        try{

            $donantes = User::whereHas(
                            'roles', function($q){
                                $q->where('name', 'donante');
                            }
                        )->get()->pluck('name','id');

            return view('controlpanel.tarjeta.create',[
                'title' => 'Crear nuevo',
                'donantes' => $donantes,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function store(Request $request)
    {
        try{

            $ccard = trim($request->number);

            $creditcards = CreditCard::all();

            foreach($creditcards as $item){
                if(decrypt($item->number) === $ccard){
                    \Session::flash('error_message','¡El registro ya existe, favor verifique!');
                    return redirect('ControlPanel/tarjetas');
                }
            }

            $user = User::find($request->user_id);

            // Desactivar la tarjeta por defecto al usuario
            foreach($user->creditcards as $item){
                // Cambiar el estatus de la tarjeta
                if($item->default){
                    $item->default = false;
                    $item->update();
                }
            }

            $lenght = strlen($request->number);

            $new = new CreditCard;
            $new->user_id = $request->user_id;
            $new->number = encrypt(trim($request->number));
            $new->name = strtoupper($request->name);
            $new->month = $request->month;
            $new->year = $request->year;
            $new->cvc = $request->cvc;
            $new->ico = $request->type;
            $new->lastdigit = substr($request->number,$lenght-4,$lenght);
            $new->default = true;
            $new->save();

            $ledger = Ledger::first();
            $ledger->creditcards = $ledger->creditcards + 1;
            $ledger->update();

            \Session::flash('success_message','¡El último registo se guardo correctamente!');
            
            return redirect($request->url);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        try{

            $creditCard = CreditCard::find($id);
            $donantes = User::whereHas(
                            'roles', function($q){
                                $q->where('name', 'donante');
                            }
                        )->get()->pluck('name','id');

            return view('controlpanel.tarjeta.edit',[
                'title' => 'Editar registro',
                'creditCard' => $creditCard,
                'numberCard' => decrypt($creditCard->number),
                'donantes' => $donantes,
                'menu' => $this->menu(),
                'url' => \URL::previous(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function update(Request $request, $id)
    {
        try{

            $lenght = strlen($request->number);
            $update = CreditCard::find($id);

            foreach($update->user->creditcards as $item){
                $item->default = false;
                $item->update();
            }



            $update->user_id = $request->user_code;
            $update->number = encrypt(trim($request->number));
            $update->name = strtoupper($request->name);
            $update->month = $request->month;
            $update->year = $request->year;
            $update->cvc = $request->cvc;
            $update->ico = $request->ico;
            $update->lastdigit = substr($request->number,$lenght-4,$lenght);
            $update->default = $request->default;
            $update->update();

            \Session::flash('success_message','¡El registro fue actualizado!');
            return redirect($request->url);
            // return back();

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function destroy($id)
    {
        try{

            $creditCard = CreditCard::find($id);
            if($creditCard->default){
                $new_default= CreditCard::where('user_id',$creditCard->user_id)->where('default',false)->first();
                
                if(count($new_default)){

                    if($new_default->id !== $id){
                        $new_default->default = true;
                        $new_default->update();
                    }

                }

            }
            $creditCard->delete();

            $ledger = Ledger::first();
            $ledger->creditcards = $ledger->creditcards - 1;
            $ledger->update();

            \Session::flash('success_message','¡El registro fue borrado!');
            return redirect('ControlPanel/tarjetas');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function menu(){
        $menu = [
                'level_1' => 'tarjetas',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
