<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\DonationAmount;
use App\DonationType;
use App\User;
use App\Schedule;
use App\Campaign;
use App\Role;
use App\Ip;
use App\Transaction;
use App\CreditCard;

use SoapClient;
use Carbon\Carbon;

class ImportController extends Controller
{
    
    public function index()
    {
    	return view('controlpanel.import',[
    		'title' => 'Importar',
    		'menu' => $this->menu(),
    	]);
    }

    public function donantes(Request $request){

    	$role = Role::where('name','donante')->first();

    	if($request->file('imported-file')){
                    $path = $request->file('imported-file')->getRealPath();
                    $data = \Excel::load($path, function($reader) {
                })->get(); 

                if(!empty($data) && $data->count()){
                    $data = $data->toArray();
                    $dataImported = array();

                    for($i=0;$i<count($data);$i++){
                        
                            $new                = new User;
                            $new->name 		    = mb_strtolower($data[$i]['nombre1'], 'utf-8');
                            $new->referenced_id = $data[$i]['idcontacto'];
                            $new->email 	    = mb_strtolower($data[$i]['email'], 'utf-8');
                            $new->photo 	    = 'default.jpg';
                            $new->house 	    = mb_strtolower($data[$i]['teldirecto'], 'utf-8');
                            $new->office 	    = mb_strtolower($data[$i]['teloficina'].' '.$data[$i]['extoficina'], 'utf-8');
                            $new->phone 	    = mb_strtolower($data[$i]['celular'], 'utf-8');
                            $new->comments 	    = mb_strtolower($data[$i]['notas'],'utf-8'). ' '.mb_strtolower($data[$i]['notas2'], 'utf-8'). ' ' .mb_strtolower($data[$i]['notas3'], 'utf-8'). ' ' .mb_strtolower($data[$i]['notas4'], 'utf-8'). ' '.mb_strtolower($data[$i]['notas5'], 'utf-8');
                            $new->save();

                            $new->attachRole($role);
                        
                    }

                    

                }
        }

    	\Session::flash('success_message','¡El último registo se guardo correctamente!');
    	return view('controlpanel.import',[
    		'title' => 'Importar',
    		'menu' => $this->menu(),
    	]);
    }

    public function tarjetas(Request $request){

    	$role = Role::where('name','donante')->first();

    	// if($request->file('imported-file')){
     //                $path = $request->file('imported-file')->getRealPath();
     //                $data = \Excel::load($path, function($reader) {
     //            })->get(); 

     //            if(!empty($data) && $data->count()){
     //                $data = $data->toArray();
     //                $dataImported = array();

     //                for($i=0;$i<count($data);$i++){
                        
     //                        $new                = new User;
     //                        $new->name 		    = mb_strtolower($data[$i]['nombre1'], 'utf-8');
     //                        $new->referenced_id = $data[$i]['idcontacto'];
     //                        $new->email 	    = mb_strtolower($data[$i]['email'], 'utf-8');
     //                        $new->photo 	    = 'default.jpg';
     //                        $new->house 	    = mb_strtolower($data[$i]['teldirecto'], 'utf-8');
     //                        $new->office 	    = mb_strtolower($data[$i]['teloficina'].' '.$data[$i]['extoficina'], 'utf-8');
     //                        $new->phone 	    = mb_strtolower($data[$i]['celular'], 'utf-8');
     //                        $new->comments 	    = mb_strtolower($data[$i]['notas'],'utf-8'). ' '.mb_strtolower($data[$i]['notas2'], 'utf-8'). ' ' .mb_strtolower($data[$i]['notas3'], 'utf-8'). ' ' .mb_strtolower($data[$i]['notas4'], 'utf-8'). ' '.mb_strtolower($data[$i]['notas5'], 'utf-8');
     //                        $new->save();

     //                        $new->attachRole($role);
                        
     //                }

                    

     //            }
     //    }

    	\Session::flash('success_message','¡El último registo se guardo correctamente!');
    	return view('controlpanel.import',[
    		'title' => 'Importar',
    		'menu' => $this->menu(),
    	]);
    }

    public function ips(Request $request){

    	if($request->file('imported-file')){
                    $path = $request->file('imported-file')->getRealPath();
                    $data = \Excel::load($path, function($reader) {
                })->get(); 

                if(!empty($data) && $data->count()){
                    $data = $data->toArray();
                    $dataImported = array();

                    for($i=0;$i<count($data);$i++){
                        
                            $new                = new Ip;
                            $new->ip 		    = $data[$i]['ip'];
                            $new->user_id 		= $data[$i]['user_id'];
                            $new->save();
                        
                    }

                }
                
        }

    	\Session::flash('success_message','¡El último registo se guardo correctamente!');
    	return view('controlpanel.import',[
    		'title' => 'Importar',
    		'menu' => $this->menu(),
    	]);
    }

    public function transacciones(Request $request){

        if($request->file('imported-file')){
                    $path = $request->file('imported-file')->getRealPath();
                    $data = \Excel::load($path, function($reader) {
                })->get(); 

                if(!empty($data) && $data->count()){
                    $data = $data->toArray();
                    $dataImported = array();

                    $result = array();

                    for($i=0;$i<count($data);$i++){

                            $ccard = CreditCard::where('lastdigit',trim( $data[$i]['lastdigit'] ))->first();

                            if($ccard){
                                $new                    = new Transaction;
                                $new->process_type      = 'Débito automático';
                                $new->donation_type_id  = $data[$i]['donationtype'];
                                $new->creditcard_id     = $ccard->id;
                                $new->campaign_id       = $data[$i]['campaignid'];
                                $new->ip_id             = 2;
                                $new->amount            = $data[$i]['amount'];
                                $new->auth_code          = $data[$i]['authcode'];
                                $new->transactionid     = $data[$i]['authcode'];
                                $new->response_code      = '100';
                                $new->status            = 'APROBADA';
                                $new->responseCodeDescription = 'APROBADA';
                                $new->type              = 'MNL';
                                $new->comments          = 'Importado desde excell';
                                $new->created_at        = $data[$i]['createdat'];
                                $new->updated_at        = $data[$i]['createdat'];
                                $new->save();

                            }else{
                                $result[] = $data[$i]['consecutivo'];
                            }      
                        
                    }

                    if($result>0){
                        return $result;
                    }
                    return back();

                }
        }
    }

    private function menu(){
        $menu = [
                'level_1' => 'transacciones',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }

}