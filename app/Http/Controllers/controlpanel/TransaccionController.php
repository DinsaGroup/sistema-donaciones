<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\DonationAmount;
use App\DonationType;
use App\User;
use App\CreditCard;
use App\Transaction;
use App\Ip;
use App\Currency;
use Auth;
use Mail;
use App\Campaign;
use App\Schedule;
use App\Email;
use App\Ledger;
use App\Report;

use Carbon\Carbon;
use SoapClient;

class TransaccionController extends Controller
{
    
    public function index($detail){

        if($detail=='aprobadas'){ $value = array('APROBADA'); };
        if($detail=='revertidas'){ $value = array('REVERSION','REVERSION APROBADA','TRANSACCION REVERSADA'); };
        if($detail=='rechazadas'){ $value= array('ERROR INESPERADO','ERROR','PROCESANDO','PENDIENTE'); };

        $transactions = Transaction::whereIn('status',$value)->get();

        return view('controlpanel.transaccion.index',[
            'title' => 'Transacciones',
            'transactions' => $transactions,
            'menu' => $this->menu(),
            ]);   
    }

    public function create(){
        try{

            $donantes = User::whereHas(
                            'roles', function($q){
                                $q->where('name', 'donante');
                            }
                        )->orderBy('name','asc')->get()->pluck('name','id');
            $donationAmounts = $this->donationamounts();

            $donationTypes = $this->donationtypes();

            $currencies = $this->currencies();

            $campaigns = Campaign::all()->pluck('name','id');

            return view('controlpanel.transaccion.create',[
                'title' => 'Realizar donación',
                'donantes' => $donantes,
                'donationAmounts' => $donationAmounts,
                'donationTypes' => $donationTypes,
                'campaigns' => $campaigns,
                'menu' => $this->menu(),
                'currencies' => $currencies,
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request){
        try{

            $donationAmount = DonationAmount::find($request->amount_id);

            if($donationAmount->currency_id==1){ // Si la denominacion es cordobas
                $donation_amount = number_format($donationAmount->amount / $this->exchange_rate_bcn(), 2,'.','');
            }else{
                $donation_amount = number_format($donationAmount->amount,2,'.','');
            }

            if($request->option == 'savedcard'){

                // Buscar tarjeta seleccionada
                $creditcard = CreditCard::find($request->ccard_id);

            }else{

                $user = User::find($request->donante_id);

                // Desactivar la tarjeta por defecto al usuario
                foreach($user->creditcards as $item){
                    // Cambiar el estatus de la tarjeta
                    if($item->default){
                        $item->default = false;
                        $item->update();
                    }
                }


                $lenght = strlen($request->number);
                //Salvar una nueva tarjeta en el sistema
                $creditcard = new CreditCard;
                $creditcard->user_id = $request->donante_id;
                $creditcard->number = encrypt(trim($request->number));
                $creditcard->name = strtoupper($request->name);
                $creditcard->month = $request->month;
                $creditcard->year = $request->year;
                $creditcard->cvc = $request->cvc;
                $creditcard->ico = $request->type;
                $creditcard->lastdigit = substr($request->number,$lenght-4,$lenght);
                $creditcard->default = true;
                $creditcard->save();
            }

            // Creamos una nueva transaccion
            $new_transaction = new Transaction;
            $new_transaction->process_type = 'E-commerce';
            $new_transaction->donation_type_id = $request->donationType_id;
            $new_transaction->creditcard_id = $creditcard->id;
            $new_transaction->campaign_id = $request->campaign_id;
            $new_transaction->amount = $donation_amount;
            $new_transaction->ip_id = $this->host();
            $new_transaction->status = 'PROCESANDO';
            $new_transaction->comments = 'Enviando solicitud de transacción.';
            $new_transaction->save();

            $ledger = Ledger::first();
            $ledger->transactions = $ledger->transactions + 1;
            

            $time = time(); 

            // Comenzar transaccion con el banco
            $key_private = 'FXWYHuG8D6CYWCARHyNsmxP21ysSvEdK';
            $key_public = '05154294';
            $hashBAC = MD5($new_transaction->id . "|" . $donation_amount . "|" . $time . "|" . $key_private);

            if($creditcard->month<10){
                $month = '0'. $creditcard->month;
            }else{
                $month = $creditcard->month;    
            }

            $ccexpiry = $month . substr($creditcard->year,2,2);

            // dd($ccexpiry);

            // Tratando informacion del BAC
            $requestData = [
                        'username' => 'pruiz021',
                        'type' => 'auth',
                        'key_id' => $key_public,
                        'hash' => $hashBAC,
                        'time' => $time,
                        'amount' => $donation_amount,
                        'orderid' => $new_transaction->id,
                        'ccnumber' => decrypt($creditcard->number),
                        'ccexp' => $ccexpiry,
                        'cvv' => $creditcard->cvc,
                    ];
                        
                        
            //Enviando información al BAC  
            $url = 'https://paycom.credomatic.com/PayComBackEndWeb/common/requestPaycomService.go';
            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($requestData));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 45000);
            $result = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);

            if($info['http_code']==200){ // Si todo esta correcto

                $resp = $this->get_object_from_url($result);

                $days = DonationType::find($request->donationType_id);
                $today = Carbon::now();

                if($days->days > 0){

                    $next = $today->addMonths($days->days);

                    // Salvamos si el tipo de donacion es recurrente
                    $new_schedule = new Schedule;
                    $new_schedule->donation_type_id = $request->donationType_id;
                    $new_schedule->creditcard_id = $creditcard->id;
                    $new_schedule->campaign_id = $request->campaign_id;
                    $new_schedule->amount = $donation_amount;
                    $new_schedule->lastProcess = Carbon::now();
                    $new_schedule->nextProcess = $next;
                    $new_schedule->save();

                    $ledger->schedules = $ledger->schedule + 1;
                }
                
                if($resp['response'] != 1){
                    $new_transaction->status = 'Error';

                    if($days->days > 0){
                        $new_schedule->failedAttempts = 1;
                        $new_schedule->status = 'Activa';
                        $new_schedule->nextProcess = Carbon::tomorrow(); // Salvamos para procesar el dia de mañana
                    }

                    $ledger->transaction_errors = $ledger->transaction_erros + 1;
                    

                    \Session::flash('error_message','¡Hubo un error en la última solicitud!');
                }else{

                    $new_transaction->status = 'APROBADA';
                    if($days->days > 0){
                        $new_schedule->status = 'Activa';
                        $new_schedule->failedAttempts = 0;
                    }

                    $ledger->transaction_oks = $ledger->transaction_oks + 1;
                    $ledger->lastdonation = $donation_amount;
                    $ledger->total = number_format($ledger->total + $donation_amount,2,'.','');
                    $ledger->month = number_format($ledger->month + $donation_amount,2,'.','');
                    $ledger->year = number_format($ledger->year + $donation_amount,2,'.','');

                    // $report = Report::where('year',Carbon::now()->year)->where('month',Carbon::now()->month)->get();
                    
                    // if(count($report)>0){
                    //     $report->amount = number_format($report->get(0)->amount + $donation_amount,2,'.','');
                    //     $report->update();
                    // }else{
                    //     $report = new Report;
                    //     $report->year = Carbon::now()->year;
                    //     $report->month = Carbon::now()->month;
                    //     $report->amount = number_format($donation_amount,2,'.','');
                    //     $report->save();
                    // }


                    \Session::flash('success_message','¡La donación se realizo con éxito!');

                }

                $ledger->update();

                if($resp['purshamount']!=''){
                    $new_transaction->purshamount = $resp['purshamount'];
                }

                $new_transaction->comments = $resp['response'] .' - '. $resp['responsetext'];
                $new_transaction->auth_code = $resp['authcode'];
                $new_transaction->transactionid = $resp['transactionid'];
                $new_transaction->avsresponse = $resp['avsresponse'];
                $new_transaction->cvvresponse = $resp['cvvresponse'];
                $new_transaction->orderid_response = $resp['orderid'];
                $new_transaction->type = $resp['type'];
                $new_transaction->response_code = $resp['response_code'];
                $new_transaction->time_response = date('Y-m-d H:i:s', $resp['time']);
                $new_transaction->hash = $resp['hash'];
                $new_transaction->update();

                if($days->days > 0){
                    $new_schedule->comments = $resp['response'] .' - '. $resp['responsetext'];
                    $new_schedule->update();
                }

                $user = User::find($request->donante_id);

                // Enviar las notificacion al administrador
                Mail::send('email.notification', 
                [
                    'transaction' => $new_transaction,
                    'user' => $user, 
                    
                ], 
                function ($m) {
                $m->from('catoruno@hssoluciones.com', 'FC Mujeres');
                $m->to('catoruno@gmail.com','Carlos Toruno')->subject('Notificación de donaciones.');
                $m->bcc('wendy@fcmujeres.org')->subject('Notificación de donaciones.');
                });
                
                // Enviar notificacion al donante
                Mail::send('email.user_notification', 
                [
                    'transaction' => $new_transaction,
                    'user' => $user, 
                    
                ], 
                function ($m) use($user) {
                $m->from('catoruno@hssoluciones.com', 'FC Mujeres');
                $m->to('catoruno@gmail.com','Carlos Toruno')->subject('Gracias por su donativo.');
                $m->bcc('wendy@fcmujeres.org')->subject('Gracias por su donativo.');
                });

            }else{ // Hubo un problema en la trasacción
                \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            }

            // Reenviar al index            
            $transactions = Transaction::all();

            return view('controlpanel.transaccion.index',[
                'title' => 'Transacciones',
                'transactions' => $transactions,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function show($id){
        $transaction = Transaction::find($id);
        return view('controlpanel.transaccion.show',[
                'title' => 'Transacción # ',
                'transaction' => $transaction,
                'menu' => $this->menu(),
                ]);
    }

    private function menu(){
        $menu = [
                'level_1' => 'transacciones',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }

    private function donationamounts(){
        $donationAmounts = DonationAmount::all();
        $Array=array();

        foreach ($donationAmounts as $item) {
            $Array[$item->id] = $item->currency->symbol .' '. number_format($item->amount,2,'.',',');
        }

        return $Array;
    }

    private function donationtypes(){
        $donationAmounts = DonationType::all();
        $Array=array();

        foreach ($donationAmounts as $item) {
            $Array[$item->id] = $item->name;
        }

        return $Array;
    }

    private function currencies(){
        $currencies = Currency::all();
        $Array=array();

        foreach ($currencies as $item) {
            $Array[$item->id] = $item->symbol .' '. $item->name;
        }

        return $Array;
    }

    private function host(){

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
                $ip = $_SERVER["HTTP_CLIENT_IP"];
            }
            elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
                $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            }
            elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
                $ip = $_SERVER["HTTP_X_FORWARDED"];
            }
            elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
                $ip = $_SERVER["HTTP_FORWARDED_FOR"];
            }
            elseif (isset($_SERVER["HTTP_FORWARDED"])){
                $ip = $_SERVER["HTTP_FORWARDED"];
            }
            else{
                $ip = $_SERVER["REMOTE_ADDR"];
            }

        
            
        $new_ip = New Ip;
        $new_ip->ip = $ip;
        $new_ip->user_id = Auth::id(); 
        $new_ip->save();

        return $new_ip->id;
    }

    private function get_object_from_url($result){

        $result = substr($result, 1, strlen($result));

        $string = explode('&', $result);

        $return_result = array();

        foreach($string as $item){
            $value = explode('=', $item);
            $return_result[$value[0]] = $value[1];
        }

        return $return_result;
    }

    private function exchange_rate_bcn(){
        /*
         * CODIGO CLIENTE WSDL DE PHP
         */
        $servicio = "https://servicios.bcn.gob.ni/Tc_Servicio/ServicioTC.asmx?WSDL"; //url del servicio
        $parametros = array(); //parametros de la llamada

        $today = Carbon::now();

        $parametros['Dia'] = $today->day;
        $parametros['Mes'] = $today->month;
        $parametros['Ano'] = $today->year;

        $client = new SoapClient($servicio, $parametros);
        $result = $client->RecuperaTC_Dia($parametros); //llamamos al métdo que nos interesa con los parámetros
        $TasaDiaria = ($result->RecuperaTC_DiaResult);

        if($TasaDiaria>0){
            return $TasaDiaria;
        }else{
            $ledger = Ledger::find(1);
            return $ledger->exchange_rate; 
        }
    }

    public function revertir($id){

        // $number_card, $exp_date, $security_code, $amount, $transaction_id
        try{

            ini_set('soap.wsdl_cache_enabled',0);
            ini_set('soap.wsdl_cache_ttl',0);

            // Buscamos la transacción para cargar sus datos
            $transaction = Transaction::find($id);

            // Creamos un nuevo registro de la trasancción
            $new_transaction = new Transaction;
            $new_transaction->process_type = 'ANULACION';
            $new_transaction->donation_type_id = $transaction->donation_type_id;
            $new_transaction->creditcard_id = $transaction->creditcard_id;
            $new_transaction->campaign_id = $transaction->campaign_id;
            $new_transaction->amount = $transaction->amount;
            $new_transaction->ip_id = $this->host();
            $new_transaction->status = 'ANULACION';
            $new_transaction->comments = 'Enviando solicitud de anulación';
            $new_transaction->type = 'MNL';
            $new_transaction->save();

            //-------------------------------------- DEMO SANDBOX -------------------------------------------------
            // Preformamos los paremetros para la reversion
            //$wsdl = 'https://csp.credomatic.com/Services/CSP/AuthorizationService.svc?singleWsdl';
           
            // construir la peticion
            // $identification = array(
            //     'Password' => 'juGreifB3_1', // contraseña de conexion
            //     'UserName' => 'wcfnic2016', // usuario de conexion
            // );
            // $terminalId = 'EMVNIC12';

            //-----------------------------------------------------------------------------------------------------
            //--------------------------------------- PRODUCCION --------------------------------------------------
            // Preformamos los paremetros para la reversion
            // $wsdl = 'https://csp.credomatic.com.ni/Services/CSP/AuthorizationService.svc?singleWsdl';
           
            // construir la peticion
            // $identification = array(
            //    'Password' => '05STglcII2', // contraseña de conexion
            //    'UserName' => 'fcam_nic', // usuario de conexion
            // );

            // $terminalId = '80006343';
            $terminalId = 'NISP0565'; // 18/01/2019

            //------------------------------------------------------------------------------------------------------

            // Parametros de reversion
            $request = array(
                'transactionType'       => 'VOID',
                'terminalId'            => $terminalId,
                'invoice'               => $transaction->id,
                'authorizationNumber'   => $transaction->auth_code,
                'referenceNumber'       => $transaction->referenceNumber,
                'systemTraceNumber'     => $transaction->systemTraceNumber,
                'stream_context'        =>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false))),
            );

            // Construimos los parametros para el request
            $parametros = array(
                'identification'=>$identification,
                'request'=>$request,
            );

            // Opciones de conexion soap
            $options = array(
                    'soap_version' => SOAP_1_2,
                    'cache_wsdl' => WSDL_CACHE_NONE,
                    'connection_timeout' => 15,
                    'trace' => true,
                    'encoding' => 'UTF-8',
                    'exceptions' => false,
                );

            // Ejecutamos la solicitud de reversion al servidor 
            $client = new SoapClient($wsdl, $options);
            
            $actionHeader = new \SoapHeader('http://www.w3.org/2005/08/addressing',
                                   'Action',
                                   'https://csp.credomatic.com/Services/CSP/AuthorizationService/IAuthorizationService/executeTransaction');
            $client->__setSoapHeaders($actionHeader);
            $client->__setLocation('https://csp.credomatic.com.ni:443/Services/CSP/AuthorizationService.svc');
            $response = $client->executeTransaction($parametros);

            // Actualizamos la información de la transacción según las respuestas de la petición      
            $new_transaction->referenceNumber = $response->executeTransactionResult->referenceNumber;
            $new_transaction->responseCode = $response->executeTransactionResult->responseCode;
            $new_transaction->responseCodeDescription = $response->executeTransactionResult->responseCodeDescription;
            $new_transaction->systemTraceNumber = $response->executeTransactionResult->systemTraceNumber;
            $new_transaction->transactionid = $response->executeTransactionResult->transactionId;
            
            // Cargamos las estadisticas para actualizarlas
            $ledger = Ledger::first();
            $ledger->transactions = $ledger->transactions + 1;

            // Si la transaccion fue aprobada actualizamos guardamos todos los datos
            if($response->executeTransactionResult->responseCodeDescription == 'APROBADA'){

                // Actualizamos las estadisticas de donaciones
                $ledger->transaction_oks = $ledger->transaction_oks + 1;
                $ledger->month = number_format($ledger->month - $transaction->amount,2,'.','');
                $ledger->year = number_format($ledger->year - $transaction->amount,2,'.','');
                $ledger->total = number_format($ledger->total - $transaction->amount,2,'.','');

                // Actualizamos los datos de la transaccion
                $new_transaction->status = $response->executeTransactionResult->responseCodeDescription;
                $new_transaction->comments = 'Solicitud de reversion de la transaccion : '.$transaction->id.', '.$response->executeTransactionResult->cardBrand;
                $new_transaction->auth_code = $response->executeTransactionResult->authorizationNumber;
                $new_transaction->referenceNumber = $response->executeTransactionResult->referenceNumber;
                $new_transaction->systemTraceNumber = $response->executeTransactionResult->systemTraceNumber;
                $new_transaction->transactionid = $response->executeTransactionResult->transactionId;

                $transaction->status = 'PAGO REVERTIDO';
                $transaction->comments = 'Transación ID : '. $new_transaction->id.', reference # : '. $response->executeTransactionResult->referenceNumber . ', auth code : '.$response->executeTransactionResult->authorizationNumber;

                $transaction->update();

            }else{

                $new_transaction->status = 'Error';
                $new_transaction->comments = 'Solicitud de reversion de la transaccion : '.$transaction->id.', '.$response->executeTransactionResult->responseCode.' - '.$response->executeTransactionResult->responseCodeDescription;
            }

            // Salvamos las modificaciones
            $new_transaction->update();
            $ledger->update();

            $report[] = $new_transaction->id;

            $user = User::find($transaction->creditcard->user->id);

            // Enviar notificacion al donante
            Mail::send('email.reverse_notification', 
            [
                'transaction' => $new_transaction,
                'user' => $user,
                'status' => $new_transaction->status,
                'notes' => $new_transaction->comments,  
            ], 
            function ($m) use($user) {
            $m->from('catoruno@hssoluciones.com', 'FC Mujeres');
            $m->to($user->email, $user->name)->subject('Reversión del donativo aplicado');
            $m->bcc('wendy@fcmujeres.org')->subject('Reversión del donativo aplicado');
            });

            $transactions = Transaction::whereIn('id',$report)->get();

            return view('controlpanel.calendario.transaction_report',[
                    'title' => 'Resultado última transacción',
                    'transactions' => $transactions,
                    'menu' => ['level_1' => 'calendario'],
                ]);

        }catch(Exception $e){

            // Enviar notificacion al donante
            Mail::send('email.user_notification_error', 
            [
                'URL' => 'http://portal.fcmujeres.org/ControlPanel/calendario',
                'number_card' => $number_card,
                'exp_date' => $exp_date,
                'amount' => $amount,
                'transaction_id' => $transaction_id,
                'error' => $e,
                
            ], 
            function ($m) {
            $m->from('catoruno@hssoluciones.com', 'FC Mujeres');
            $m->to('catoruno@gmail.com', 'webmaster')->subject('Error al efectuar débito automático.');
            $m->bcc('wendy@fcmujeres.org')->subject('Error al efectuar débito automático.');
            });

            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }

    }

}
