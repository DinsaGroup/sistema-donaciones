<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use SoapClient;
use Carbon\Carbon;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\GetConversionAmount;
use App\Soap\Response\GetConversionAmountResponse;


class WebServiceController extends Controller
{

    protected $soapWrapper;

    public function __construct(SoapWrapper $soapWrapper)
    {
        $this->soapWrapper = $soapWrapper;
    }

    public function index_bk()
    {
        ini_set('soap.wsdl_cache_enabled', 0);
        ini_set('soap.wsdl_cache_ttl', 900);
        ini_set('default_socket_timeout', 15);

        $ctx_opts = array(
            'http' => array(
                'header' => array(
                                'Content-Type' => 'application/x-www-form-urlencoded\r\n',

                                )

            )
        );

        $ctx = stream_context_create($ctx_opts);

        $wsdl = "https://csp.credomatic.com/Services/CSP/AuthorizationService.svc?singleWsdl";
        $options = array(
                // 'uri' => 'https://csp.credomatic.com:50581/Services/CSP/AuthorizationService/',
                'style' => SOAP_RPC,
                'use' => SOAP_ENCODED,
                'soap_version' => SOAP_1_2,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'connection_timeout' => 15,
                'trace' => true,
                'encoding' => 'UTF-8',
                'exceptions' => true,
                'stream_context' => $ctx,
                
        );

        $identification = array(
            'Password' => 'juGreifB3_1', // contraseña de conexion
            'UserName' => 'wcfnic2016', // usuario de conexion
        );

        $request = array(
            'transactionType'   => 'SALE',
            'terminalId'        => 'EMVNIC12',
            'invoice'           => 123456,
            'accountNumber'     => 5402910000000000,
            'expirationDate'    => 1609,
            'securityCode'      => 600,
            'entryMode'         => 'MNL',
            'totalAmount'       => '1.99',

        ); 

        $parametros = array(
            'identification'=>$identification,
            'request'=>$request,
        );


        try{



            $client = new SoapClient($wsdl,array('soap_version' => SOAP_1_2));

            $actionHeader = new \SoapHeader('https://csp.credomatic.com:50581/Services/CSP/AuthorizationService',
                               'executeTransaction',
                               $parametros,
                               false);
            $client->__setSoapHeaders($actionHeader);

            $data = $client->executeTransaction($parametros);
            // $data = $client->__soapCall('executeTransaction',$parametros,['https://csp.credomatic.com:50581/Services/CSP/AuthorizationService/iAuthorizationService/executeTransaction','https://csp.credomatic.com:50581/Services/CSP/AuthorizationService.svc']);

        }catch(Exception $e){
            die($e->getMessage());
        }

        var_dump($data);
        die;

        // $action = 'executeTransaction';
        // $client->__soapCall("executeTransaction", $parametros,
        //         array(
        //             'soapaction'=> $action,
        //             'uri'=> 'https://csp.credomatic.com:50581/Services/CSP/AuthorizationService.svc'
        //             ));
    }

    public function index(){
        $wsdl = 'https://csp.credomatic.com/Services/CSP/AuthorizationService.svc?singleWsdl';
       
        // construir la peticion
        $identification = array(
            'Password' => 'juGreifB3_1', // contraseña de conexion
            'UserName' => 'wcfnic2016', // usuario de conexion
        );

        $request = array(
            'terminalId' => 'EMVNIC12',
            'transactionType' => 'ECHO_TEST',
        ); 

        // $request = array(
        //     'transactionType'   => 'SALE',
        //     'terminalId'        => 'EMVNIC12',
        //     'invoice'           => 123456,
        //     'accountNumber'     => 4026830102182179,
        //     'expirationDate'    => 1905,
        //     'securityCode'      => 304,
        //     'entryMode'         => 'MNL',
        //     'totalAmount'       => '0.1',

        // );

        $parametros = array(
            'identification'=>$identification,
            'request'=>$request,
        );

        $options = array(
                'soap_version' => SOAP_1_2,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'connection_timeout' => 15,
                'trace' => true,
                'encoding' => 'UTF-8',
                'exceptions' => true,
                // 'SOAPAction'=>'https://csp.credomatic.com:50581/Services/CSP/AuthorizationService/IAuthorizationService/executeTransaction',
                // 'Action'=>'https://csp.credomatic.com:50581/Services/CSP/AuthorizationService/IAuthorizationService/executeTransaction',
                // 'uri'=>'https://csp.credomatic.com:50581/Services/CSP/AuthorizationService/IAuthorizationService/executeTransaction',
            );


        $client = new SoapClient($wsdl, $options);
        $actionHeader = new \SoapHeader('http://www.w3.org/2005/08/addressing',
                               'Action',
                               'https://csp.credomatic.com/Services/CSP/AuthorizationService/IAuthorizationService/executeTransaction');
        $client->__setSoapHeaders($actionHeader);
        $client->__setLocation('https://csp.credomatic.com:50581/Services/CSP/AuthorizationService.svc');
        $response = $client->executeTransaction($parametros);

        dd($response);

    }
    
}
