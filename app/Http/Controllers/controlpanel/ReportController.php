<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Transaction;
use App\CreditCard;
use Carbon\Carbon;

class ReportController extends Controller
{
    
    public function index(){
        try{
            // Seleccionar solo donantes
            $donantes = User::whereHas(
                            'roles', function($q){
                                $q->where('name', 'donante');
                            }
                        )->get()->pluck('name','id');


            return view('controlpanel.reportes.index',[
                'title' => 'Reportes del sistema',
                'menu' => $this->menu(),
                'donantes' => $donantes,
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function donante($id){
        
        $donante = User::find($id);
        $transactions = array();

        foreach($donante->creditcards as $ccard){
            $transac = Transaction::where('creditcard_id',$ccard->id)->get();
            foreach($transac as $item){
                $transactions[] = $item;  
            }
        }

        $vistaurl = 'controlpanel.reportes.donante_detail';
        $view =  \View::make($vistaurl, ['donante'=>$donante,'transactions' => $transactions] )->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        return $pdf->stream('reporte');     
    }

    public function todosLosDonantes(){
        
        $donantes = User::whereHas(
                            'roles', function($q){
                                $q->where('name', 'donante');
                            }
                        )->orderBy('id','asc')->get();

        $vistaurl = 'controlpanel.reportes.donantes';
        $view =  \View::make($vistaurl, ['donantes'=>$donantes] )->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        return $pdf->stream('Listado de donantes');     
    }

    public function tarjetas(){
    
        $tarjetas = Creditcard::orderBy('id','asc')->get();

        $vistaurl = 'controlpanel.reportes.tarjetas';
        $view =  \View::make($vistaurl, ['tarjetas'=>$tarjetas] )->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        return $pdf->stream('Listado de tarjetas');     
    }

    public function createPDF(Request $request,$section,$action){

        if($section=='donante'){
            $donante = User::find($request->donante_id);
            $transactions = array();

            foreach($donante->creditcards as $ccard){
                $transac = Transaction::where('creditcard_id',$ccard->id)->get();
                foreach($transac as $item){
                    $transactions[] = $item;  
                }
            }

            $vistaurl = 'controlpanel.reportes.donante_detail';
            $view =  \View::make($vistaurl, ['donante'=>$donante,'transactions' => $transactions] )->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            
            if($action=='pdf'){return $pdf->stream('reporte');}
            if($action=='download'){return $pdf->download('reporte.pdf'); }
        }
    }

    public function donaciones(Request $request){

        $desde = carbon::parse($request->desde .' 00:00:00');
        $hasta = carbon::parse($request->hasta .' 23:59:59');

        $transactions = Transaction::where('status','APROBADA')->whereBetween('created_at', array($desde, $hasta))->get();


        $aprobada = 0;
        $procesando = 0;
        $tansacionReversada = 0;
        $reversionAprobada = 0;
        $anulacion = 0;
        $pagoRevertido = 0;
        $total = 0;

        

            foreach ($transactions as $item) {
                
                if($item->status === "REVERSION APROBADA"){
                    $reversionAprobada = $reversionAprobada + $item->amount;
                }

                if($item->status === "PROCESANDO" ){
                    $procesando = $procesando + $item->amount;
                }

                if($item->status === "ANULACION" ){
                    $anulacion = $anulacion + $item->amount;
                }

                if($item->status === "PAGO REVERTIDO" ){
                    $pagoRevertido = $pagoRevertido + $item->amount;
                }

                if($item->status === "APROBADA" ){
                    $aprobada = $aprobada + $item->amount;
                }

            }

       

        $total = $reversionAprobada + $procesando + $anulacion + $pagoRevertido + $aprobada;

        $vistaurl = 'controlpanel.reportes.donaciones';
        $view =  \View::make($vistaurl, ['transactions'=>$transactions, 'total'=>$total, 'reversionAprobada'=>$reversionAprobada, 'procesando'=>$procesando, 'anulacion' => $anulacion, 'pagoRevertido'=>$pagoRevertido, 'aprobada'=>$aprobada, 'desde' => $desde, 'hasta'=>$hasta ] )->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        return $pdf->stream('Listado de tarjetas');     
    }

    public function transacciones(Request $request){

        $desde = carbon::parse($request->desde .' 00:00:00');
        $hasta = carbon::parse($request->hasta .' 23:59:59');

        $transactions = Transaction::whereBetween('created_at', array($desde, $hasta))->get();


        $aprobada = 0;
        $procesando = 0;
        $tansacionReversada = 0;
        $reversionAprobada = 0;
        $anulacion = 0;
        $pagoRevertido = 0;
        $total = 0;

        

            foreach ($transactions as $item) {
                
                if($item->status === "REVERSION APROBADA"){
                    $reversionAprobada = $reversionAprobada + $item->amount;
                }

                if($item->status === "PROCESANDO" ){
                    $procesando = $procesando + $item->amount;
                }

                if($item->status === "ANULACION" ){
                    $anulacion = $anulacion + $item->amount;
                }

                if($item->status === "PAGO REVERTIDO" ){
                    $pagoRevertido = $pagoRevertido + $item->amount;
                }

                if($item->status === "APROBADA" ){
                    $aprobada = $aprobada + $item->amount;
                }

            }

       

        $total = $reversionAprobada + $procesando + $anulacion + $pagoRevertido + $aprobada;

        $vistaurl = 'controlpanel.reportes.donaciones';
        $view =  \View::make($vistaurl, ['transactions'=>$transactions, 'total'=>$total, 'reversionAprobada'=>$reversionAprobada, 'procesando'=>$procesando, 'anulacion' => $anulacion, 'pagoRevertido'=>$pagoRevertido, 'aprobada'=>$aprobada, 'desde' => $desde, 'hasta'=>$hasta ] )->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        return $pdf->stream('Listado de tarjetas');     
    }

    private function menu(){
        $menu = [
                'level_1' => 'reportes',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }

}
