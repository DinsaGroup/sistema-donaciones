<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

use App\Transaction;
use App\User;
use App\Ledger;
use App\Report;
use Carbon\Carbon;
use SoapClient;
class DashboardController extends Controller
{  
    public function index()
    {
        try{

            $transactions = Transaction::orderBy('id','desc')->get()->take(5);
            $donantes = User::whereHas('roles', function($q){$q->where('name', 'donante');})->orderBy('id','desc')->get()->take(8);
            $ledger = Ledger::first();
            
            return view('controlpanel.dashboard.index',[
                'title' => 'Dashboard',
                'transactions' => $transactions,
                'donantes' => $donantes,
                'ledger' => $ledger,
                'saleChart' => $this->salesChart(),
                'exchange_rate_bcn' => $this->exchange_rate_bcn(),
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function salesChart(){
        $meses = ['Ene ', 'Feb ', 'Mar ','Abr ','May ','Jun ','Jul ','Ago ','Sept ','Oct ','Nov ','Dic '];
        $saleChart = array();
        $mes = array();
        $report = Report::orderBy('year','desc')->orderBy('month','desc')->get()->take(12);

        // dd($report);
        if($report){

            foreach($report as $item){

                if( ($item->month-2) >= 0 ){
                    $mes['month'] = $meses[$item->month-2]. substr($item->year,-2); 
                }else{
                    $mes['month'] = $meses[ $item->month + 11]. substr($item->year-1,-2);
                }

                $mes['amount'] = $item->amount;
                $saleChart[] = $mes;

            }
        
        }else{

             $saleChart = [['Ene 19',0], ['Feb 19',0], ['Mar 19',0],['Abr 19',0],['May 19',0],['Jun 19',0],['Jul 19',0],['Ago 19',0],['Sept 19',0],['Oct 19',0],['Nov 19',0],['Dic 19',0]];

        }
        // dd($saleChart);
        return $saleChart;
    }

    private function exchange_rate_bcn(){
        /*
         * CODIGO CLIENTE WSDL DE PHP
         */
        $servicio = "https://servicios.bcn.gob.ni/Tc_Servicio/ServicioTC.asmx?WSDL"; //url del servicio
        $parametros = array(); //parametros de la llamada

        $today = Carbon::now();

        $parametros['Ano'] = $today->year;
        $parametros['Mes'] = $today->month;
        $parametros['Dia'] = $today->day;

        // try{

        //     $client = new \SoapClient($servicio);
        //     $result = $client->RecuperaTC_Dia($parametros); //llamamos al métdo que nos interesa con los parámetros
        //     $TasaDiaria = ($result->RecuperaTC_DiaResult);

        //     return $TasaDiaria;

        // }catch(SoupFaul $e){
        //     $ledger = Ledger::first();
        //     return $ledger->exchange_rate;
        // }
        $ledger = Ledger::first();
        return $ledger->exchange_rate;
    }

    private function menu(){
        $menu = [
                'level_1' => 'dashboar 19',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }

}
