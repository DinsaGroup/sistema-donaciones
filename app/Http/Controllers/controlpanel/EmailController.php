<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Email;

class EmailController extends Controller
{

    public function index()
    {
        try{

            $emails = Email::all();

            return view('controlpanel.email.index',[
                'title' => 'Correos',
                'emails' => $emails,
                'menu' => $this->menu(),
                ]);

        }catch(Exception $e){

        }
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        //
    }

    private function menu(){
        $menu = [
                'level_1' => 'emails',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
