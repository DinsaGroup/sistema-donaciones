<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Campaign;

class CampaignController extends Controller
{
    public function index()
    {
        try{

            $campaigns = Campaign::all();

            return view('controlpanel.campaign.index',[
                'title' => 'Campañas',
                'campaigns' => $campaigns,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function create()
    {
        try{

            $campaigns = Campaign::all();

            return view('controlpanel.campaign.create',[
                'title' => 'Crear nuevo',
                'campaigns' => $campaigns,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function store(Request $request)
    {
        try{

            $string = ucfirst( strtolower($request->name) );

            $campaign = Campaign::where('name',$string)->get();

            if(count($campaign)){
                \Session::flash('error_message','¡El registro ya existe, favor verifique!');
                return redirect('ControlPanel/campaigns');
            }

            $new = new Campaign;
            $new->name = $string;
            $new->description = ucfirst( strtolower($request->description) );
            $new->save();

            \Session::flash('success_message','¡El último registo se guardo correctamente!');
            return redirect('ControlPanel/campaigns');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        try{

            $campaigns = Campaign::find($id);

            return view('controlpanel.campaign.edit',[
                'title' => 'Editar registro',
                'campaigns' => $campaigns,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function update(Request $request, $id)
    {
        try{
            $update = Campaign::find($id);
            $update->name = ucfirst( strtolower($request->name));
            $update->description = ucfirst( strtolower($request->description));
            $update->active = $request->active;
            $update->update();

            \Session::flash('success_message','¡El registro fue actualizado!');
            return redirect('ControlPanel/campaigns');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function destroy($id)
    {
        try{

            $delete = Campaign::find($id);
            $delete->delete();

            \Session::flash('success_message','¡El registro fue borrado!');
            return redirect('ControlPanel/campaigns');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function menu(){
        $menu = [
                'level_1' => 'campaigns',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
