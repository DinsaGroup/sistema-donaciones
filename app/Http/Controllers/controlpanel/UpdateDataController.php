<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use App\User;
use App\DonationAmount;
use App\DonationType;
use App\Country;
use App\Currency;
use App\CreditCard;
use App\Transaction;
use App\Campaign;
use App\Schedule;
use App\Ledger;
use App\Ip;
use Auth;
use Mail;
use App\Email;
use App\Report;
use App\Reverse;


use SoapClient;
use Carbon\Carbon;


class UpdateDataController extends Controller
{
    
    public function ledger(){

        try{
            $donantes = User::whereHas('roles', function($q){$q->where('name', 'donante');})->get();
            $administradores = User::whereHas('roles', function($q){$q->where('name', 'superadmin');})->get();
            $managers = User::whereHas('roles', function($q){$q->where('name', 'manager');})->get();
            $donationtypes = DonationType::all();
            $donationamounts = DonationAmount::all();
            $countries = Country::all();
            $currencies = Currency::all();
            $creditcards = CreditCard::all();
            $creditcards_actives = 0;
            $creditcards_inactives = 0;
            $reports = Report::all();

            foreach ($reports as $item) {
                $item->amount = 0;
                $item->update();
            }

            foreach($creditcards as $item){
                if($item->status == 'active'){
                    $creditcards_actives++;
                }else{
                    $creditcards_inactives++;
                }
            }

            $transactions = Transaction::orderBy('id','asc')->get();
            $transaction_oks = 0;
            $transaction_errors = 0;
            $lastdonation = 0;
            $month = 0;
            $hoy = 0;
            $year = 0;
            $total = 0;
            $today = Carbon::now()->subHour(6);

            foreach($transactions as $item){

                if(trim($item->status) === 'APROBADA'){
                    $transaction_oks++;
                    $lastdonation = $item->amount;

                    $total += $item->amount;

                    if($item->created_at->toDateString() == $today->toDateString()){
                        $hoy += $item->amount;
                    }

                    if($item->created_at->month == $today->month ){
                        $month += $item->amount;
                    }

                    if($item->created_at->year == $today->year){
                        $year += $item->amount;
                    }


                    $day = carbon::parse($item->created_at);

                    $create_report = Report::where('year', $day->year )->where('month', $day->month )->first();

                    if($create_report){
                        $new_total = number_format($create_report->amount,2,'.',',') + number_format($item->amount,2,'.','');
                        $create_report->amount = number_format($new_total , 2,'.','');
                        $create_report->update();
                    }else{
                        $new_report = new Report;
                        $new_report->year = $day->year;
                        $new_report->month = $day->month;
                        $new_report->amount = number_format( $item->amount,2,'.','' );
                        $new_report->save();
                    }

                }else{
                    $transaction_errors++;
                }

            }
            
            $campaigns = Campaign::all();

            $schedules = Schedule::all();
            $schedule_actives = 0;
            $schedule_pendings = 0;

            foreach($schedules as $item){
                if($item->status == 'Procesado'){
                    $schedule_actives++;
                }else{
                    $schedule_pendings++;
                }
            }

            $ledger = Ledger::find(1);
            $ledger->donants = count($donantes);
            $ledger->admons = count($administradores);
            $ledger->managers = count($managers);
            $ledger->donationtypes = count($donationtypes);
            $ledger->donationamounts = count($donationamounts);
            $ledger->countries = count($countries);
            $ledger->currencies = count($currencies);
            $ledger->creditcards = count($creditcards);
            $ledger->creditcard_actives = $creditcards_actives;
            $ledger->creditcard_inactives = $creditcards_inactives;
            $ledger->transactions = count($transactions);
            $ledger->transaction_oks = $transaction_oks;
            $ledger->transaction_errors = $transaction_errors;
            $ledger->campaigns = count($campaigns);
            $ledger->schedules = count($schedules);
            $ledger->schedule_actives = $schedule_actives;
            $ledger->schedule_pendings = $schedule_pendings;
            $ledger->lastdonation = $lastdonation;
            $ledger->lastdonation = number_format($lastdonation,2,'.','');
            $ledger->month = number_format($month,2,'.','');
            $ledger->year = number_format($year,2,'.','');
            $ledger->total = number_format($total,2,'.','');
            $ledger->exchange_rate = $this->exchange_rate_bcn(); 
            $ledger->update();

            // return redirect('ControlPanel');
            \Session::flash('success_message','¡Los registros fueron actualizados con éxito!');
            return redirect()->back();

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function processcards(){
        try{
            
            ini_set('soap.wsdl_cache_enabled',0);
            ini_set('soap.wsdl_cache_ttl',0);

            // Consultar a la DB si hay reversiones pendientes
            $reverse_pending = Reverse::where('status','PENDIENTE DE REVERSION')->get();

            // Si hay reversiones pendientes, realizar dichas reversiones
            if($reverse_pending){
                
                foreach ($reverse_pending as $item) {

                    // Realizar reversion a la tarjeta, returnara false si hubo un problema
                    $resultado = $this->make_reversion($item->transaction_id);

                    // Si hubo un error en la reversion de cargo la preparamos para hacer un REVERSE
                    if(!$resultado){

                        // No hacemos nada porque haremos nuevamente el intento de reversion
                        \Log::info('Transaccion -> '. $item->transaction_id .' | **** - '. $item->transaction->creditcard->lastdigit .' | '.$item->transaction->campaign->name.' | U$ '. number_format($item->transaction->amount,2,'.',',').' | ERROR INESPERADO');

                    }else{ // Significa que la reversión paso sin problemas

                        // Guardamos los resultados de la reversión
                        // Crear una nueva transaccion
                        $new_reversion = new Transaction;
                        $new_reversion->process_type = 'Débito automático';
                        $new_reversion->donation_type_id = $item->transaction->donation_type_id;
                        $new_reversion->creditcard_id = $item->transaction->creditcard_id;
                        $new_reversion->campaign_id = $item->transaction->campaign_id;
                        $new_reversion->amount = $item->transaction->amount;
                        $new_reversion->ip_id = $this->host();
                        $new_reversion->status = 'REVERSION APROBADA';
                        $new_reversion->comments = 'Reversión aprobada para la transación/invoice No. '. $item->transaction_id;
                        $new_reversion->type = 'MNL';
                        $new_reversion->save();

                        // Actualizamos el estatus de la transacción reversada
                        $transaction_update = Transaction::find($item->transaction_id);
                        $transaction_update->status = 'TRANSACCION REVERSADA';
                        $transaction_update->comments = 'La reversión fue realizada ('. $transaction_update->comments.')';
                        $transaction_update->update();

                        // Borramos de la tabla de reversa la transaccion
                        $item->status = 'REVERSION REALIZADA';
                        $item->update();

                    }  
                }
            }

            $today = Carbon::today();
            $schedules = Schedule::where('status','Activa')->where('nextProcess', '<=' ,$today)->whereIn('status',['Activa','Pendiente'])->get();
            $report = array();

            // Preguntamos si hay donaciones pendientes por realizar a la fecha
            if($schedules){

                // Procesamos cada una de las donaciones programadas
                foreach($schedules as $item){

                    $transacciones = 0;

                    // Desencriptar tarjeta de credito
                    $number_card = decrypt($item->creditcard->number);

                    // Preformar la fecha de expiracion de la tarjeta
                    if($item->creditcard->month<10){
                        $month = '0'. $item->creditcard->month;
                    }else{
                        $month = $item->creditcard->month;    
                    }

                    $exp_date = substr($item->creditcard->year,2,2) . $month;

                    // Cargar el codigo de seguridad
                    $security_code = $item->creditcard->cvc;

                    // Preformar el monto a cobrar
                    $amount = number_format($item->amount,2,'.','');

                    // Crear una nueva transaccion
                    $new_transaction = new Transaction;
                    $new_transaction->process_type = 'Débito automático';
                    $new_transaction->donation_type_id = $item->donation_type_id;
                    $new_transaction->creditcard_id = $item->creditcard_id;
                    $new_transaction->campaign_id = $item->campaign_id;
                    $new_transaction->amount = $item->amount;
                    $new_transaction->ip_id = $this->host();
                    $new_transaction->status = 'PROCESANDO';
                    $new_transaction->comments = 'Procesando el cargo automático.';
                    $new_transaction->type = 'MNL';
                    $new_transaction->save();

                    $transacciones++;

                    $report[] = $new_transaction->id;

                    // Realizar cargo a la tarjeta, returnara false si hubo un problema
                    $response = $this->make_charge($number_card, $exp_date, $security_code, $amount, $new_transaction->id); 
                    // dd($response);
                    // Si hubo un error en la transaccion de cargo, la preparamos para hacer un REVERSE
                    if( $response->executeTransactionResult->responseCode === 'NA' || $response->executeTransactionResult->responseCodeDescription === 'DENEGADA FI' || $response->executeTransactionResult->responseCodeDescription === 'TARJETA INVALIDA'  || $response->executeTransactionResult->responseCodeDescription === 'TARJETA VENCIDA' || $response->executeTransactionResult->responseCodeDescription === 'CODIGO DE SEGURIDAD INVALIDO' || $response->executeTransactionResult->responseCodeDescription === 'INVALID USER' || $response->executeTransactionResult->responseCodeDescription === 'TRANSACCION INVALIDA'){

                        // Anulamos la transacción y procedemos a realizar su reversion
                        $new_transaction->status = 'ERROR INESPERADO';
                        $new_transaction->comments = $response->executeTransactionResult->responseCodeDescription.' - Se procede hacer la reversión'; 
                        $new_transaction->update();

                        \Log::info('Transaccion -> '. $new_transaction->id .' | **** - '. $item->creditcard->lastdigit .' | '.$item->campaign->name.' | U$ '. number_format($item->amount,2,'.',',').' | ERROR INESPERADO');

                        // Salvamos en la tabla de reversa para reversar en el futuro, por si hay problemas
                        $reverse = new Reverse;
                        $reverse->transaction_id = $new_transaction->id;
                        $reverse->save();

                        // Realizamos un ciclo de tres intentos para hacer la reversión y no caer en un ciclo indefinido
                        for($i=0;$i<3;$i++){                            

                            // Realizar cargo a la tarjeta, returnara false si hubo un problema
                            $resultado = $this->make_reversion($new_transaction->id);

                            // Si hubo un error en la reversion de cargo la preparamos para hacer un REVERSE
                            if(!$resultado){

                                // No hacemos nada porque haremos nuevamente el intento de reversion
                                \Log::info('Transaccion -> '. $new_transaction->id .' | **** - '. $item->creditcard->lastdigit .' | '.$item->campaign->name.' | U$ '. number_format($item->amount,2,'.',',').' | ERROR INESPERADO');

                            }else{ // Significa que la reversión paso sin problemas

                                // Guardamos los resultados de la reversión
                                // Crear una nueva transaccion
                                $new_reversion = new Transaction;
                                $new_reversion->process_type = 'Débito automático';
                                $new_reversion->donation_type_id = $item->donation_type_id;
                                $new_reversion->creditcard_id = $item->creditcard_id;
                                $new_reversion->campaign_id = $item->campaign_id;
                                $new_reversion->amount = $item->amount;
                                $new_reversion->ip_id = $this->host();
                                $new_reversion->status = 'REVERSION APROBADA';
                                $new_reversion->comments = 'Reversión aprobada para la transación/invoice No. '. $new_transaction->id;
                                $new_reversion->type = 'MNL';
                                $new_reversion->save();

                                $transacciones++;
                                $report[] = $new_reversion->id;

                                // Actualizamos el estatus de la transacción reversada
                                $new_transaction->status = 'TRANSACCION REVERSADA';
                                $new_transaction->comments = 'La reversión fue realizada ('.$new_transaction->comments.')';
                                $new_transaction->update();

                                // Borramos de la tabla de reversa la transaccion
                                $reverse->status = 'REVERSION REALIZADA';
                                $reverse->update();

                                // Terminamos el ciclo
                                $i = 4;

                            } // Cierre del else

                        } // Cierre del for

                        $item->nextProcess = Carbon::tomorrow(1); // Actualizamos fecha para proximo procesamiento

                        if( ($item->failedAttempts+1) >= 3){ // Validar cantidad de intentos fallidos

                            $item->status = 'DESACTIVA';
                            $item->comments = 'Demasiados intentos fallidos, revisar y ponerse en contacto con el donante';

                        }
                        
                        $item->failedAttempts = $item->failedAttempts + 1; // Incrementar intento fallido de cobro

                        $item->update();

                        $user = User::find($item->creditcard->user->id);

                        // Enviar notificacion al donante
                        Mail::send('email.user_notification_2', 
                        [
                            'transaction' => $new_transaction,
                            'user' => $user,
                            'nextProcess' => $item->nextProcess,
                            'status' => $item->status,
                            'notes' => $item->comments,
                            
                        ], 
                        function ($m) use($user) {
                        $m->from('catoruno@hssoluciones.com', 'FC Mujeres');
                        $m->to($user->email, $user->name)->subject('Gracias por su donativo.');
                        $m->bcc('wendy@fcmujeres.org')->subject('Gracias por su donativo.');
                    
                        });
                        
                    // si todo esta correcto con el cargo a la tarjeta, procedemos a guardar los resultados para seguir con la siguiente transaccion.
                    }else{ 

                        // Actualizamos la información de la transacción
                        $new_transaction->auth_code                 = $response->executeTransactionResult->authorizationNumber;
                        $new_transaction->referenceNumber           = $response->executeTransactionResult->referenceNumber;
                        $new_transaction->responseCode              = $response->executeTransactionResult->responseCode;
                        $new_transaction->responseCodeDescription   = $response->executeTransactionResult->responseCodeDescription;
                        $new_transaction->systemTraceNumber         = $response->executeTransactionResult->systemTraceNumber;
                        $new_transaction->transactionid             = $response->executeTransactionResult->transactionId;
                        

                        // Si la transaccion fue aprobada actualizamos la fecha para proximo debito automatico
                        if($response->executeTransactionResult->responseCodeDescription == 'APROBADA'){

                            $ledger = Ledger::first();
                            $ledger->transactions = $transacciones;
                            $ledger->transaction_oks = $ledger->transaction_oks + 1;
                            $ledger->lastdonation = $item->amount;
                            $ledger->month = number_format($ledger->month + $item->amount,2,'.','');
                            $ledger->year = number_format($ledger->year + $item->amount,2,'.','');
                            $ledger->total = number_format($ledger->total + $item->amount,2,'.','');
                            
                            $item->failedAttempts = 0;

                            // Actualizamos fecha de su ultimo donativo
                            $item->lastProcess = Carbon::now();

                            // Actualizamos fecha para el proximo debito automatico
                            $days = DonationType::find($item->donation_type_id);
                            // $item->nextProcess = Carbon::now()->addDays($days->days);
                            $item->nextProcess = Carbon::now()->addMonths($days->days);
                            // Guardamos la respuesta del server
                            $item->comments = $response->executeTransactionResult->responseCode .' - '. $response->executeTransactionResult->responseCodeDescription;


                            $new_transaction->status = $response->executeTransactionResult->responseCodeDescription;
                            $new_transaction->comments = 'Cargo realizado con éxito.';

                            \Log::info('Transaccion -> '. $new_transaction->id .' | **** - '. $item->creditcard->lastdigit .' | '.$item->campaign->name.' | U$ '. number_format($item->amount,2,'.',',').' | '.$response->executeTransactionResult->responseCodeDescription);

                        }else{

                            $item->nextProcess = Carbon::tomorrow(1); // Actualizamos fecha para proximo procesamiento

                            if( ($item->failedAttempts+1) >= 3){ // Validar cantidad de intentos fallidos

                                $item->status = 'DESACTIVA';
                                $item->comments = 'Demasiados intentos fallidos, revisar y ponerse en contacto con el donante';

                            }
                            
                            $item->failedAttempts = $item->failedAttempts + 1; // Incrementar intento fallido de cobro

                            $new_transaction->status = 'Error';
                            $new_transaction->comments = $response->executeTransactionResult->responseCode.' - '.$response->executeTransactionResult->responseCodeDescription;
                            $item->comments = $response->executeTransactionResult->responseCode.' - '.$response->executeTransactionResult->responseCodeDescription;

                            \Log::info('Transaccion -> '. $new_transaction->id .' | **** - '. $item->creditcard->lastdigit .' | '.$item->campaign->name.' U$ '. number_format($item->amount,2,'.',',').' '.$response->executeTransactionResult->responseCodeDescription);
                        }

                        // Salvamos las modificaciones
                        $new_transaction->update();
                        $item->update();
                        $ledger->update();

                        // $report[] = $new_transaction->id;

                        $user = User::find($item->creditcard->user->id);

                        // Enviar notificacion al donante
                        Mail::send('email.user_notification_2', 
                        [
                            'transaction' => $new_transaction,
                            'user' => $user,
                            'nextProcess' => $item->nextProcess,
                            'status' => $item->status,
                            'notes' => $item->comments,
                            
                        ], 
                        function ($m) use($user) {
                        $m->from('catoruno@hssoluciones.com', 'FC Mujeres');
                        $m->to($user->email, $user->name)->subject('Gracias por su donativo.');
                        $m->bcc('wendy@fcmujeres.org')->subject('Gracias por su donativo.');
                        });

                    } // End Else if 

                } // End foreach schedule

                $transactions = Transaction::whereIn('id',$report)->get();

                return view('controlpanel.calendario.transaction_report',[
                        'title' => 'Resultado última transacción',
                        'transactions' => $transactions,
                        'menu' => ['level_1' => 'calendario'],
                    ]);
            }else{
                // Sino hay nada que procesar, regresamos a la pagina normal con un mensaje
                $schedules = Schedule::all();

                \Session::flash('success_message','¡Por el momento, no hubo tarjetas que procesar!');

                return view('controlpanel.calendario.index',[
                    'title' => 'Calendario de donaciones',
                    'schedules' => $schedules,
                    'menu' => $this->menu(),
                    ]);
            }

        }catch(Exception $e){
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function host(){

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
                $ip = $_SERVER["HTTP_CLIENT_IP"];
            }
            elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
                $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            }
            elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
                $ip = $_SERVER["HTTP_X_FORWARDED"];
            }
            elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
                $ip = $_SERVER["HTTP_FORWARDED_FOR"];
            }
            elseif (isset($_SERVER["HTTP_FORWARDED"])){
                $ip = $_SERVER["HTTP_FORWARDED"];
            }
            else{
                $ip = $_SERVER["REMOTE_ADDR"];
            }

        
            
        $new_ip = New Ip;
        $new_ip->ip = $ip;
        $new_ip->user_id = Auth::id(); 
        $new_ip->save();

        return $new_ip->id;
    }

    private function get_object_from_url($result){

        $result = substr($result, 1, strlen($result));

        $string = explode('&', $result);

        $return_result = array();

        foreach($string as $item){
            $value = explode('=', $item);
            $return_result[$value[0]] = $value[1];
        }

        return $return_result;
    }

    private function exchange_rate_bcn(){
        /*
         * CODIGO CLIENTE WSDL DE PHP
         */
        

            $servicio = "https://servicios.bcn.gob.ni/Tc_Servicio/ServicioTC.asmx?WSDL"; //url del servicio
            $parametros = array(); //parametros de la llamada

            $today = Carbon::now();

            $parametros['Ano'] = $today->year;
            $parametros['Mes'] = $today->month;
            $parametros['Dia'] = $today->day;

            // $client = new \SoapClient($servicio);
            // $result = $client->RecuperaTC_Dia($parametros); //llamamos al métdo que nos interesa con los parámetros
            // $TasaDiaria = ($result->RecuperaTC_DiaResult);

            // if($TasaDiaria>0){
            //     return $TasaDiaria;
            // }else{
                $ledger = Ledger::find(1);
                return $ledger->exchange_rate; 
            // }
    }

    private function make_charge($number_card, $exp_date, $security_code, $amount, $transaction_id){

            $original = ini_get('default_socket_timeout');
            ini_set('default_socket_timeout', 45); // Seteamos el tiempo de conexion a 45segundos

            //-------------------------------------- DEMO SANDBOX -------------------------------------------------
            // Preformamos los paremetros para la reversion
            //$wsdl = 'https://csp.credomatic.com/Services/CSP/AuthorizationService.svc?singleWsdl';
           
            // construir la peticion
            // $identification = array(
            //     'Password' => 'juGreifB3_1', // contraseña de conexion
            //     'UserName' => 'wcfnic2016', // usuario de conexion
            // );
            // $terminalId = 'EMVNIC12';

            //-----------------------------------------------------------------------------------------------------
            //--------------------------------------- PRODUCCION --------------------------------------------------
            // Preformamos los paremetros para la reversion
            $wsdl = 'https://csp.credomatic.com/Services/CSP/AuthorizationService.svc?singleWsdl';
           
            // construir la peticion
            $identification = array(
                'Password' => '05STglcII2', // contraseña de conexion
                'UserName' => 'fcam_nic', // usuario de conexion
            );

            // $terminalId = '80006343';
            $terminalId = 'NISP0565'; // 18/01/2019

            // $request = array(
            //     'terminalId' => 'EMVNIC12',
            //     'transactionType' => 'ECHO_TEST',
            // ); 

            $request = array(
                'transactionType'   => 'SALE',
                'terminalId'        => $terminalId,
                'invoice'           => $transaction_id,
                'accountNumber'     => $number_card,
                'expirationDate'    => $exp_date,
                'securityCode'      => $security_code,
                'entryMode'         => 'MNL',
                'totalAmount'       => $amount,

            );

            $parametros = array(
                'identification'=>$identification,
                'request'=>$request,
            );

            $options = array(
                    'soap_version' => SOAP_1_2,
                    'cache_wsdl' => WSDL_CACHE_NONE,
                    'connection_timeout' => 45,
                    'trace' => true,
                    'encoding' => 'UTF-8',
                    'exceptions' => false,
                );


            $client = new SoapClient($wsdl, $options);
            $actionHeader = new \SoapHeader('http://www.w3.org/2005/08/addressing',
                                   'Action',
                                   // 'https://csp.credomatic.com/Services/CSP/AuthorizationService/IAuthorizationService/executeTransaction');
                                   'https://csp.credomatic.com/Services/CSP/AuthorizationService/IAuthorizationService/executeTransaction');
            $client->__setSoapHeaders($actionHeader);
            // $client->__setLocation('https://csp.credomatic.com:50581/Services/CSP/AuthorizationService.svc');
            $client->__setLocation('https://csp.credomatic.com.ni:443/Services/CSP/AuthorizationService.svc');

            $response = $client->executeTransaction($parametros);
            
            // Preguntamos si hubo un problema con la solicitud soap
            if(is_soap_fault($response)){

                // Notificamos al administrador que hubo un problema con la transacción
                Mail::send('email.user_notification_error', 
                [
                    'URL' => 'http://portal.fcmujeres.org/ControlPanel/calendario',
                    'number_card' => $number_card,
                    'exp_date' => $exp_date,
                    'amount' => $amount,
                    'transaction_id' => $transaction_id,
                    'error' => $response,
                    
                ], 
                function ($m) {
                $m->from('catoruno@hssoluciones.com', 'FC Mujeres');
                $m->to('catoruno@gmail.com', 'webmaster')->subject('Error al efectuar débito automático.');
                $m->bcc('wendy@fcmujeres.org')->subject('Error al efectuar débito automático.');
                });

                ini_set('default_socket_timeout', $original);
                return false; // Regresamos que hubo un error en la transaccion

            }else{

                ini_set('default_socket_timeout', $original);
                return $response;
            }
    } // end make_charge

    private function make_reversion($invoice){

        $original = ini_get('default_socket_timeout');
        ini_set('default_socket_timeout', 45); // Seteamos el tiempo de conexion a 45segundos

        //-------------------------------------- DEMO SANDBOX -------------------------------------------------
            // Preformamos los paremetros para la reversion
            //$wsdl = 'https://csp.credomatic.com/Services/CSP/AuthorizationService.svc?singleWsdl';
           
            // construir la peticion
            // $identification = array(
            //     'Password' => 'juGreifB3_1', // contraseña de conexion
            //     'UserName' => 'wcfnic2016', // usuario de conexion
            // );
            // $terminalId = 'EMVNIC12';

            //-----------------------------------------------------------------------------------------------------
            //--------------------------------------- PRODUCCION --------------------------------------------------
            // Preformamos los paremetros para la reversion
            $wsdl = 'https://csp.credomatic.com/Services/CSP/AuthorizationService.svc?singleWsdl';
           
            // construir la peticion
            $identification = array(
                'Password' => '05STglcII2', // contraseña de conexion
                'UserName' => 'fcam_nic', // usuario de conexion
            );

            // $terminalId = '80006343';
            $terminalId = 'NISP0565'; // 18/01/2019

        // Parametros de reversion
        $request = array(
            'transactionType'   => 'REVERSE',
            'terminalId'        => $terminalId,
            'invoice'           => $invoice,
            'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false))),
        );

        // Construimos los parametros para el request
        $parametros = array(
            'identification'=>$identification,
            'request'=>$request,
        );

        // Opciones de conexion soap
        $options = array(
                'soap_version' => SOAP_1_2,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'connection_timeout' => 45,
                'trace' => true,
                'encoding' => 'UTF-8',
                'exceptions' => false,
            );

        // Ejecutamos la solicitud de reversion al servidor 
        $client = new SoapClient($wsdl, $options);
        $actionHeader = new \SoapHeader('http://www.w3.org/2005/08/addressing',
                               'Action',
                               'https://csp.credomatic.com/Services/CSP/AuthorizationService/IAuthorizationService/executeTransaction');
        $client->__setSoapHeaders($actionHeader);
        $client->__setLocation('https://csp.credomatic.com.ni:443/Services/CSP/AuthorizationService.svc');
        $response = $client->executeTransaction($parametros);

        if(is_soap_fault($response) || $response->executeTransactionResult->responseCode == 'NA'){

            // Guardamos el registro de la transaccion de reversion, en caso de error
            \Log::info('Error al hacer la reversión de la transacción '.$invoice );
            ini_set('default_socket_timeout', $original);
            return false; // Regresamos que hubo un error en la transaccion

        }else{

            // Guardamos el registro de la reversion
            \Log::info('Reversión APROBADA de la transacción '.$invoice);
            ini_set('default_socket_timeout', $original);
            return $response;

        }
    }
        
}
