<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Role;
use App\Permission;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class RoleController extends Controller
{

    public function index()
    {
        try{

            $roles = Role::all();
            return view('controlpanel.role.index',[
                'title' => 'Roles administrativos',
                'roles' => $roles,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function create()
    {
        try{

            $roles = Role::all();
            /*$permissions = Permission::OrderBy('name', 'asc')->get();*/
            $permissions = Permission::all();
            $count = count($permissions)/3;

            return view('controlpanel.role.create',[
                'title' => 'Crear nuevo',
                'roles' => $roles,
                'permissions' => $permissions,
                'count' => $count,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function store(Request $request)
    {
        try{

            $perm = Input::get('perm');
            list($keys,$values) = array_divide($perm);
            // dd($keys);

            $role = Role::where('name',$request->name)->get();
            $permissions = Permission::whereIn('id',$keys)->get();
            // dd($permissions);

            if(count($role)){
                \Session::flash('error_message','¡El registro ya existe, favor verifique!');
                return redirect('ControlPanel/roles-administrativos');
            }

            $new = new Role;
            $new->name = $request->name;
            $new->display_name = $request->display_name;
            $new->description = $request->description;
            $new->acceso = $request->acceso;
            $new->save();

            foreach ($permissions as $item) {
                $new->attachPermission($item);
            }

            \Session::flash('success_message','¡El último registo se guardo correctamente!');
            return redirect('ControlPanel/roles-administrativos');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        try{

            $roles = Role::find($id);
            $permissions = Permission::all();
            $count = count($permissions)/3;

            $result = DB::table('permission_role')->where('role_id', $id)->get();
            // dd($result);

            return view('controlpanel.role.edit',[
                'title' => 'Editar registro',
                'roles' => $roles,
                'permissions' => $permissions,
                'count' => $count,
                'result' => $result,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function update(Request $request, $id)
    {
        try{

            $perm = Input::get('perm');
            list($keys,$values) = array_divide($perm);

            $role = Role::where('name',$request->name)->get();
            $permissions = Permission::whereIn('id',$keys)->get();

            DB::table('permission_role')->where('role_id',$id)->delete();
            // DB::table('permissions')->truncate();

            $update = Role::find($id);
            $update->name = $request->name;
            $update->display_name = $request->display_name;
            $update->description = $request->description;
            $update->acceso = $request->acceso;
            $update->update();

            foreach ($permissions as $item) {
                $update->attachPermission($item);
            }

            \Session::flash('success_message','¡El registro fue actualizado!');
            return redirect('ControlPanel/roles-administrativos');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function destroy($id)
    {
        try{

            $role = Role::find($id);
            $role->delete();

            \Session::flash('success_message','¡El registro fue borrado!');
            return redirect('ControlPanel/roles-administrativos');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function menu(){
        $menu = [
                'level_1' => 'configuraciones',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
