<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Country;

class PaisController extends Controller
{

    public function index()
    {
        try{

            $countries = Country::all();

            return view('controlpanel.country.index',[
                'title' => 'Paises del sistema',
                'countries' => $countries,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function create()
    {
        try{

            $countries = Country::all();

            return view('controlpanel.country.create',[
                'title' => 'Crear nuevo',
                'countries' => $countries,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function store(Request $request)
    {
        try{

            $value = strtoupper($request->name);

            $countries = Country::where('name',$value)->get();

            if(count($countries)){
                \Session::flash('error_message','¡El registro ya existe, favor verifique!');
                return redirect('ControlPanel/countries-administrativos');
            }

            $new = new Country;
            $new->code1 = strtoupper($request->code1);
            $new->code2 = strtoupper($request->code2);
            $new->ext = strtoupper($request->ext);
            $new->name = strtoupper($request->name);
            $new->save();

            \Session::flash('success_message','¡El último registo se guardo correctamente!');
            return redirect('ControlPanel/configuraciones');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        try{

            $countries = Country::find($id);

            return view('controlpanel.country.edit',[
                'title' => 'Editar registro',
                'countries' => $countries,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function update(Request $request, $id)
    {
        try{
            $update = Country::find($id);
            $update->code1 = strtoupper($request->code1);
            $update->code2 = strtoupper($request->code2);
            $update->ext = strtoupper($request->ext);
            $update->name = strtoupper($request->name);
            $update->update();

            \Session::flash('success_message','¡El registro fue actualizado!');
            return redirect('ControlPanel/configuraciones');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function destroy($id)
    {
        try{

            $countries = Country::find($id);
            $countries->delete();

            \Session::flash('success_message','¡El registro fue borrado!');
            return redirect('ControlPanel/configuraciones');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function menu(){
        $menu = [
                'level_1' => 'configuraciones',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
