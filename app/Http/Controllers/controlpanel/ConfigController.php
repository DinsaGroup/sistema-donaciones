<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\DonationType;
use App\Country;
use App\DonationAmount;
use App\Currency;

class ConfigController extends Controller
{
    
    public function index()
    {
        try{

            $donante_types = DonationType::all();
            $countries = Country::all();
            $donationAmounts = DonationAmount::all();
            $currencies = Currency::all();
            
            return view('controlpanel.config.index',[
                'title' => 'Panel de configuraciones',
                'donante_types' => $donante_types,
                'countries' => $countries,
                'donationAmounts' => $donationAmounts,
                'currencies' => $currencies,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function menu(){
        $menu = [
                'level_1' => 'configuraciones',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }

}
