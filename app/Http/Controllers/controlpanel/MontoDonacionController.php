<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\DonationAmount;
use App\Currency;

class MontoDonacionController extends Controller
{
    public function index()
    {
        try{

            $donationAmount = DonationAmount::all();

            return view('controlpanel.donation_amount.index',[
                'title' => 'Monto de donaciones',
                'donationAmounts' => $donationAmount,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function create()
    {
        try{

            $donationAmount = DonationAmount::all();
            $currencies = Currency::all()->pluck('symbol','id');

            return view('controlpanel.donation_amount.create',[
                'title' => 'Crear nuevo',
                'donationAmount' => $donationAmount,
                'currencies' => $currencies,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function store(Request $request)
    {
        try{

            $new = new DonationAmount;
            $new->currency_id = $request->currency_id;
            $new->amount = $request->amount;
            $new->save();

            \Session::flash('success_message','¡El último registo se guardo correctamente!');
            return redirect('ControlPanel/monto-donaciones');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        try{

            $donationAmount = DonationAmount::find($id);
            $currencies = Currency::all()->pluck('symbol','id');

            return view('controlpanel.donation_amount.edit',[
                'title' => 'Editar registro',
                'donationAmount' => $donationAmount,
                'currencies' => $currencies,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function update(Request $request, $id)
    {
        try{

            $update = DonationAmount::find($id);
            $update->currency_id = $request->currency_id;
            $update->amount = $request->amount;
            $update->update();

            \Session::flash('success_message','¡El registro fue actualizado!');
            return redirect('ControlPanel/monto-donaciones');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function destroy($id)
    {
        try{

            $donationAmount = DonationAmount::find($id);
            $donationAmount->delete();

            \Session::flash('success_message','¡El registro fue borrado!');
            return redirect('ControlPanel/monto-donaciones');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function menu(){
        $menu = [
                'level_1' => 'configuraciones',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
    
}
