<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Ip;

class IpController extends Controller
{
    
    public function index(){
        try{

            $ips = ip::all();

            return view('controlpanel.ipaccess.index',[
                'title' => 'Registro de conexiones IP',
                'ips' => $ips,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    
    public function create(){
        //
    }

    
    public function store(Request $request){
        //
    }

    
    public function show($id){
        //
    }

    
    public function edit($id){
        //
    }

    
    public function update(Request $request, $id){
        //
    }

    
    public function destroy($id){
        //
    }

    private function menu(){
        $menu = [
                'level_1' => 'configuraciones',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
