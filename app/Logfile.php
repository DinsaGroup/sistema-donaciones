<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logfile extends Model
{
    
    protected $table = 'logfiles';

    protected $fillable = [ 'user_id', 'log', 'ip'];

    public function user(){
    	return $this->belognsTo('App\User');
    }

}
