<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DonationAmount extends Model
{
    use SoftDeletes;

    protected $table = 'donation_amounts';

    protected $fillable = [ 'currency_id','amount' ];

    public function currency(){
        return $this->belongsTo('App\Currency');
    }
}
