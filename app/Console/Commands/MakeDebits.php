<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;
use App\DonationAmount;
use App\DonationType;
use App\Country;
use App\Currency;
use App\CreditCard;
use App\Transaction;
use App\Campaign;
use App\Schedule;
use App\Ledger;
use App\Ip;
use Auth;
use Mail;
use App\Email;
use App\Report;
use App\Reverse;


use SoapClient;
use Carbon\Carbon;

class MakeDebits extends Command
{

    protected $signature = 'make:debits';
   
    protected $description = 'Make all debit schedule from donants.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        \Log::info('Se comienza hacer los debitos automáticos de las tarjetas');

        ini_set('soap.wsdl_cache_enabled',0);
        ini_set('soap.wsdl_cache_ttl',0);

        $today = Carbon::today();
        $schedules = Schedule::where('status','Activa')->where('nextProcess', '<=' ,$today)->whereIn('status',['Activa','Pendiente'])->get();
        $report = array();

        // Preguntamos si hay donaciones pendientes por realizar a la fecha
        if($schedules){

            // Procesamos cada una de las donaciones programadas
            foreach($schedules as $item){

                $transacciones = 0;

                // Desencriptar tarjeta de credito
                $number_card = decrypt($item->creditcard->number);

                // Preformar la fecha de expiracion de la tarjeta
                if($item->creditcard->month<10){
                    $month = '0'. $item->creditcard->month;
                }else{
                    $month = $item->creditcard->month;    
                }

                $exp_date = substr($item->creditcard->year,2,2) . $month;

                // Cargar el codigo de seguridad
                $security_code = $item->creditcard->cvc;

                // Preformar el monto a cobrar
                $amount = number_format($item->amount,2,'.','');

                // Crear una nueva transaccion
                $new_transaction = new Transaction;
                $new_transaction->process_type = 'Débito automático';
                $new_transaction->donation_type_id = $item->donation_type_id;
                $new_transaction->creditcard_id = $item->creditcard_id;
                $new_transaction->campaign_id = $item->campaign_id;
                $new_transaction->amount = number_format($item->amount,2);
                $new_transaction->ip_id = 1;
                $new_transaction->status = 'PROCESANDO';
                $new_transaction->comments = 'Procesando el cargo automático.';
                $new_transaction->type = 'MNL';
                $new_transaction->save();

                $transacciones++;

                $report[] = $new_transaction->id;

                // Realizar cargo a la tarjeta, returnara false si hubo un problema
                $response = $this->make_charge($number_card, $exp_date, $security_code, $amount, $new_transaction->id);
            
                // Si hubo un error en la transaccion de cargo la preparamos para hacer un REVERSE
                if(!$response || $response->executeTransactionResult->responseCode === 'NA' || $response->executeTransactionResult->responseCodeDescription === 'TARJETA INVALIDA' || $response->executeTransactionResult->responseCodeDescription === 'DENEGADA FI' || $response->executeTransactionResult->responseCodeDescription === 'TARJETA VENCIDA' || $response->executeTransactionResult->responseCodeDescription === 'CODIGO DE SEGURIDAD INVALIDO' || $response->executeTransactionResult->responseCodeDescription === 'TRANSACCION INVALIDA'){

                    // Anulamos la transacción y procedemos a realizar su reversion
                    $new_transaction->status = 'ERROR INESPERADO';
                    $new_transaction->comments = $response->executeTransactionResult->responseCodeDescription.' - Se procede hacer la reversión';
                    $new_transaction->update();

                    \Log::info('Transaccion -> '. $new_transaction->id .' | **** - '. $item->creditcard->lastdigit .' | '.$item->campaign->name.' | U$ '. number_format($item->amount,2,'.',',').' | ERROR INESPERADO');

                    // Salvamos en la tabla de reversa para reversar en el futuro, por si hay problemas
                    $reverse = new Reverse;
                    $reverse->transaction_id = $new_transaction->id;
                    $reverse->save();

                    // Realizamos un ciclo de tres intentos para hacer la reversión y no caer en un ciclo indefinido
                    for($i=0;$i<3;$i++){

                        // Realizar cargo a la tarjeta, returnara false si hubo un problema
                        $resultado = $this->make_reversion($new_transaction->id);

                        // Si hubo un error en la reversion de cargo la preparamos para hacer un REVERSE
                        if(!$resultado){

                            // No hacemos nada porque haremos nuevamente el intento de reversion
                            \Log::info('Transaccion -> '. $new_transaction->id .' | **** - '. $item->creditcard->lastdigit .' | '.$item->campaign->name.' | U$ '. number_format($item->amount,2,'.',',').' | ERROR INESPERADO');

                        }else{ // Significa que la reversión paso sin problemas

                            // Guardamos los resultados de la reversión
                            // Crear una nueva transaccion
                            $new_reversion = new Transaction;
                            $new_reversion->process_type = 'Débito automático';
                            $new_reversion->donation_type_id = $item->donation_type_id;
                            $new_reversion->creditcard_id = $item->creditcard_id;
                            $new_reversion->campaign_id = $item->campaign_id;
                            $new_reversion->amount = $item->amount;
                            $new_reversion->ip_id = 1;
                            $new_reversion->status = 'REVERSION APROBADA';
                            $new_reversion->comments = 'Reversión aprobada para la transación/invoice No. '. $new_transaction->id;
                            $new_reversion->type = 'MNL';
                            $new_reversion->save();

                            $transacciones++;
                            $report[] = $new_reversion->id;

                            // Actualizamos el estatus de la transacción reversada
                            $new_transaction->status = 'TRANSACCION REVERSADA';
                            $new_transaction->comments = 'La reversión fue realizada ('.$new_transaction->comments.')';
                            $new_transaction->update();

                            // Borramos de la tabla de reversa la transaccion
                            $reverse->status = 'REVERSION REALIZADA';
                            $reverse->update();

                            // Terminamos el ciclo
                            $i = 4;

                        } // Cierre del else

                    } // cierre del for


                    $item->nextProcess = Carbon::tomorrow(1); // Actualizamos fecha para proximo procesamiento

                    if( ($item->failedAttempts+1) >= 3){ // Validar cantidad de intentos fallidos

                        $item->status = 'DESACTIVA';
                        $item->comments = 'Demasiados intentos fallidos, revisar y ponerse en contacto con el donante';

                    }
                    
                    $item->failedAttempts = $item->failedAttempts + 1; // Incrementar intento fallido de cobro

                    $item->update();

                    $user = User::find($item->creditcard->user->id);

                    // Enviar notificacion al donante
                    Mail::send('email.user_notification_2', 
                    [
                        'transaction' => $new_transaction,
                        'user' => $user,
                        'nextProcess' => $item->nextProcess,
                        'status' => $item->status,
                        'notes' => $item->comments,
                        
                    ], 
                    function ($m) use($user) {
                    $m->from('catoruno@hssoluciones.com', 'FC Mujeres');
                    $m->to($user->email, $user->name)->subject('Gracias por su donativo.');
                    $m->bcc('wendy@fcmujeres.org')->subject('Gracias por su donativo.');
                    });

                // si todo esta correcto con el cargo a la tarjeta, procedemos a guardar los resultados para seguir con la siguiente transaccion.        
                }else{    

                    // Actualizamos la información de la transacción
                    $new_transaction->auth_code                 = $response->executeTransactionResult->authorizationNumber;
                    $new_transaction->referenceNumber           = $response->executeTransactionResult->referenceNumber;
                    $new_transaction->responseCode              = $response->executeTransactionResult->responseCode;
                    $new_transaction->responseCodeDescription   = $response->executeTransactionResult->responseCodeDescription;
                    $new_transaction->systemTraceNumber         = $response->executeTransactionResult->systemTraceNumber;
                    $new_transaction->transactionid             = $response->executeTransactionResult->transactionId;
                    

                    // Si la transaccion fue aprobada actualizamos la fecha para proximo debito automatico
                    if($response->executeTransactionResult->responseCodeDescription == 'APROBADA'){

                        $ledger = Ledger::first();
                        $ledger->transactions = $transacciones;
                        $ledger->transaction_oks = $ledger->transaction_oks + 1;
                        $ledger->lastdonation = $item->amount;
                        $ledger->month = number_format($ledger->month + $item->amount,2,'.','');
                        $ledger->year = number_format($ledger->year + $item->amount,2,'.','');
                        $ledger->total = number_format($ledger->total + $item->amount,2,'.','');
                        
                        $item->failedAttempts = 0;

                        // Actualizamos fecha de su ultimo donativo
                        $item->lastProcess = Carbon::now();

                        // Actualizamos fecha para el proximo debito automatico
                        $days = DonationType::find($item->donation_type_id);
                        // $item->nextProcess = Carbon::now()->addDays($days->days);
                        $item->nextProcess = Carbon::now()->addMonths($days->days);
                        // Guardamos la respuesta del server
                        $item->comments = $response->executeTransactionResult->responseCode .' - '. $response->executeTransactionResult->responseCodeDescription;


                        $new_transaction->status = $response->executeTransactionResult->responseCodeDescription;
                        $new_transaction->comments = 'Cargo realizado con éxito.';

                        \Log::info('Transaccion -> '. $new_transaction->id .' | **** - '. $item->creditcard->lastdigit .' | '.$item->campaign->name.' | U$ '. number_format($item->amount,2,'.',',').' | '.$response->executeTransactionResult->responseCodeDescription);

                    }else{

                        $item->nextProcess = Carbon::tomorrow(1); // Actualizamos fecha para proximo procesamiento

                        if( ($item->failedAttempts+1) >= 3){ // Validar cantidad de intentos fallidos

                            $item->status = 'DESACTIVA';
                            $item->comments = 'Demasiados intentos fallidos, revisar y ponerse en contacto con el donante';

                        }
                        
                        $item->failedAttempts = $item->failedAttempts + 1; // Incrementar intento fallido de cobro

                        $new_transaction->status = 'Error';
                        $new_transaction->comments = $response->executeTransactionResult->responseCode.' - '.$response->executeTransactionResult->responseCodeDescription;
                        $item->comments = $response->executeTransactionResult->responseCode.' - '.$response->executeTransactionResult->responseCodeDescription;

                        \Log::info('Transaccion -> '. $new_transaction->id .' | **** - '. $item->creditcard->lastdigit .' | '.$item->campaign->name.' U$ '. number_format($item->amount,2,'.',',').' '.$response->executeTransactionResult->responseCodeDescription);
                    } // end else

                    // Salvamos las modificaciones
                    $new_transaction->update();
                    $item->update();
                    $ledger->update();

                    // $report[] = $new_transaction->id;

                    $user = User::find($item->creditcard->user->id);

                    // Enviar notificacion al donante
                    Mail::send('email.user_notification_2', 
                    [
                        'transaction' => $new_transaction,
                        'user' => $user,
                        'nextProcess' => $item->nextProcess,
                        'status' => $item->status,
                        'notes' => $item->comments,
                        
                    ], 
                    function ($m) use($user) {
                    $m->from('catoruno@hssoluciones.com', 'FC Mujeres');
                    $m->to($user->email, $user->name)->subject('Gracias por su donativo.');
                
                    });

                } // Cierre Else if response

                $transactions = Transaction::whereIn('id',$report)->get();

                foreach($transactions as $item){

                    // Guardamos cada transaccion en registro para su verificacion
                    \Log::info('Transaccion -> '. $item->transactionid .' **** - '. $item->creditcard->lastdigit .' '.$item->campaign->name.' U$ '. number_format($item->amount,2,'.',',').' '.$item->responseCodeDescription); 
                } // end foreach

                \Log::info('Se termina con exito los debitos automaticos');

            }// Enf foreach schedule

        }// End if schedules
        
    }

    private function get_object_from_url($result){

        $result = substr($result, 1, strlen($result));

        $string = explode('&', $result);

        $return_result = array();

        foreach($string as $item){
            $value = explode('=', $item);
            $return_result[$value[0]] = $value[1];
        }

        return $return_result;
    }

    private function exchange_rate_bcn(){
        /*
         * CODIGO CLIENTE WSDL DE PHP
         */
        $servicio = "https://servicios.bcn.gob.ni/Tc_Servicio/ServicioTC.asmx?WSDL"; //url del servicio
        $parametros = array(); //parametros de la llamada

        $today = Carbon::now();

        $parametros['Ano'] = $today->year;
        $parametros['Mes'] = $today->month;
        $parametros['Dia'] = $today->day;

        // dd($parametros);

        // $client = new \SoapClient($servicio);
        // $result = $client->RecuperaTC_Dia($parametros); //llamamos al métdo que nos interesa con los parámetros
        // $TasaDiaria = ($result->RecuperaTC_DiaResult);

        // if($TasaDiaria>0){
        //     return $TasaDiaria;
        // }else{
            $ledger = Ledger::find(1);
            return $ledger->exchange_rate; 
        // }
    }

    private function make_charge($number_card, $exp_date, $security_code, $amount, $transaction_id){

        $original = ini_get('default_socket_timeout');
        ini_set('default_socket_timeout', 45); // Seteamos el tiempo de conexion a 45segundos

        // $wsdl = 'https://csp.credomatic.com/Services/CSP/AuthorizationService.svc?singleWsdl';
       
        // construir la peticion
        // $identification = array(
        //     'Password' => 'juGreifB3_1', // contraseña de conexion
        //     'UserName' => 'wcfnic2016', // usuario de conexion
        // );

        // $request = array(
        //     'terminalId' => 'EMVNIC12',
        //     'transactionType' => 'ECHO_TEST',
        // ); 

        //-----------------------------------------------------------------------------------------------------
        //--------------------------------------- PRODUCCION --------------------------------------------------
        // Preformamos los paremetros para la reversion
        $wsdl = 'https://csp.credomatic.com.ni/Services/CSP/AuthorizationService.svc?singleWsdl';
       
        // construir la peticion
        $identification = array(
            'Password' => '05STglcII2', // contraseña de conexion
            'UserName' => 'fcam_nic', // usuario de conexion
        );

        // $terminalId = '80006343';
        $terminalId = 'NISP0565'; // 18/01/2019

        //------------------------------------------------------------------------------------------------------


        $request = array(
            'transactionType'   => 'SALE',
            'terminalId'        => $terminalId,
            'invoice'           => $transaction_id,
            'accountNumber'     => $number_card,
            'expirationDate'    => $exp_date,
            'securityCode'      => $security_code,
            'entryMode'         => 'MNL',
            'totalAmount'       => $amount,

        );

        $parametros = array(
            'identification'=>$identification,
            'request'=>$request,
        );

        $options = array(
                'soap_version' => SOAP_1_2,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'connection_timeout' => 45,
                'trace' => true,
                'encoding' => 'UTF-8',
                'exceptions' => false,
            );


        $client = new SoapClient($wsdl, $options);
        $actionHeader = new \SoapHeader('http://www.w3.org/2005/08/addressing',
                               'Action',
                               'https://csp.credomatic.com/Services/CSP/AuthorizationService/IAuthorizationService/executeTransaction');
        $client->__setSoapHeaders($actionHeader);
        // $client->__setLocation('https://csp.credomatic.com:50581/Services/CSP/AuthorizationService.svc');
        $client->__setLocation('https://csp.credomatic.com.ni:443/Services/CSP/AuthorizationService.svc');

        $response = $client->executeTransaction($parametros);
        
        // Preguntamos si hubo un problema con la solicitud soap
        if(is_soap_fault($response)){

            // Notificamos al administrador que hubo un problema con la transacción
            Mail::send('email.user_notification_error', 
            [
                'URL' => 'http://portal.fcmujeres.org/ControlPanel/calendario',
                'number_card' => $number_card,
                'exp_date' => $exp_date,
                'amount' => $amount,
                'transaction_id' => $transaction_id,
                'error' => $response,
                
            ], 
            function ($m) {
            $m->from('catoruno@hssoluciones.com', 'FC Mujeres');
            $m->to('catoruno@gmail.com', 'webmaster')->subject('Error al efectuar débito automático.');
        
            });

            ini_set('default_socket_timeout', $original);
            return false; // Regresamos que hubo un error en la transaccion

        }else{

            ini_set('default_socket_timeout', $original);
            return $response;
        }
    } 

    private function make_reversion($invoice){

        $original = ini_get('default_socket_timeout');
        ini_set('default_socket_timeout', 45); // Seteamos el tiempo de conexion a 45segundos

        // Preformamos los paremetros para la reversion
        // $wsdl = 'https://csp.credomatic.com/Services/CSP/AuthorizationService.svc?singleWsdl';
       
        // construir la peticion
        // $identification = array(
        //     'Password' => 'juGreifB3_1', // contraseña de conexion
        //     'UserName' => 'wcfnic2016', // usuario de conexion
        // );

        //-----------------------------------------------------------------------------------------------------
        //--------------------------------------- PRODUCCION --------------------------------------------------
        // Preformamos los paremetros para la reversion
        $wsdl = 'https://csp.credomatic.com.ni/Services/CSP/AuthorizationService.svc?singleWsdl';
       
        // construir la peticion
        $identification = array(
            'Password' => '05STglcII2', // contraseña de conexion
            'UserName' => 'fcam_nic', // usuario de conexion
        );

        $terminalId = '80006343';

        //------------------------------------------------------------------------------------------------------


        // Parametros de reversion
        $request = array(
            'transactionType'   => 'REVERSE',
            'terminalId'        => $terminalId,
            'invoice'           => $invoice,
            'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false))),
        );

        // Construimos los parametros para el request
        $parametros = array(
            'identification'=>$identification,
            'request'=>$request,
        );

        // Opciones de conexion soap
        $options = array(
                'soap_version' => SOAP_1_2,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'connection_timeout' => 45,
                'trace' => true,
                'encoding' => 'UTF-8',
                'exceptions' => false,
            );

        // Ejecutamos la solicitud de reversion al servidor 
        $client = new SoapClient($wsdl, $options);
        $actionHeader = new \SoapHeader('http://www.w3.org/2005/08/addressing',
                               'Action',
                               'https://csp.credomatic.com/Services/CSP/AuthorizationService/IAuthorizationService/executeTransaction');
        $client->__setSoapHeaders($actionHeader);
        // $client->__setLocation('https://csp.credomatic.com:50581/Services/CSP/AuthorizationService.svc');
        $client->__setLocation('https://csp.credomatic.com.ni:443/Services/CSP/AuthorizationService.svc');
        $response = $client->executeTransaction($parametros);

        if(is_soap_fault($response) || $response->executeTransactionResult->responseCode == 'NA'){

            // Guardamos el registro de la transaccion de reversion, en caso de error
            \Log::info('Error al hacer la reversión de la transacción '.$invoice );
            ini_set('default_socket_timeout', $original);
            return false; // Regresamos que hubo un error en la transaccion

        }else{

            // Guardamos el registro de la reversion
            \Log::info('Reversión APROBADA de la transacción '.$invoice);
            ini_set('default_socket_timeout', $original);
            return $response;

        }
    }

}
