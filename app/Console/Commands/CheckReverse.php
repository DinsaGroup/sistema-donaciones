<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;
use App\DonationAmount;
use App\DonationType;
use App\Country;
use App\Currency;
use App\CreditCard;
use App\Transaction;
use App\Campaign;
use App\Schedule;
use App\Ledger;
use App\Ip;
use Auth;
use Mail;
use App\Email;
use App\Report;
use App\Reverse;


use SoapClient;
use Carbon\Carbon;

class CheckReverse extends Command
{

    protected $signature = 'check:reverse';

    protected $description = 'Check if there are some reverse transaction pending';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // Escribimos en el log
        \Log::info('Verificación de reversas pendientes');

        ini_set('soap.wsdl_cache_enabled',0);
        ini_set('soap.wsdl_cache_ttl',0);

        // Consultar a la DB si hay reversiones pendientes
        $reverse_pending = Reverse::where('status','PENDIENTE DE REVERSION')->get();

        // Si hay reversiones pendientes, realizar dichas reversiones
        if($reverse_pending){
            
            foreach ($reverse_pending as $item) {

                // Realizar reversion a la tarjeta, returnara false si hubo un problema
                $resultado = $this->make_reversion($item->transaction_id);

                // Si hubo un error en la reversion de cargo la preparamos para hacer un REVERSE
                if(!$resultado){

                    // No hacemos nada porque haremos nuevamente el intento de reversion
                    \Log::info('Transaccion -> '. $item->transaction_id .' | **** - '. $item->transaction->creditcard->lastdigit .' | '.$item->transaction->campaign->name.' | U$ '. number_format($item->transaction->amount,2,'.',',').' | ERROR INESPERADO');

                }else{ // Significa que la reversión paso sin problemas

                    // Guardamos los resultados de la reversión
                    // Crear una nueva transaccion
                    $new_reversion = new Transaction;
                    $new_reversion->process_type = 'Débito automático';
                    $new_reversion->donation_type_id = $item->transaction->donation_type_id;
                    $new_reversion->creditcard_id = $item->transaction->creditcard_id;
                    $new_reversion->campaign_id = $item->transaction->campaign_id;
                    $new_reversion->amount = $item->transaction->amount;
                    $new_reversion->ip_id = 1;
                    $new_reversion->status = 'REVERSION APROBADA';
                    $new_reversion->comments = 'Reversión aprobada para la transación/invoice No. '. $item->transaction_id;
                    $new_reversion->type = 'MNL';
                    $new_reversion->save();

                    // Actualizamos el estatus de la transacción reversada
                    $transaction_update = Transaction::find($item->transaction_id);
                    $transaction_update->status = 'TRANSACCION REVERSADA';
                    $transaction_update->comments = 'La reversión fue realizada ('.$transaction_update->comments.')';
                    $transaction_update->update();

                    // Borramos de la tabla de reversa la transaccion
                    $item->status = 'REVERSION REALIZADA';
                    $item->update();

                }  
            }

        } // End if reverse pending
    }

    private function make_reversion($invoice){

        $original = ini_get('default_socket_timeout');
        ini_set('default_socket_timeout', 45); // Seteamos el tiempo de conexion a 45segundos

        // Preformamos los paremetros para la reversion
        $wsdl = 'https://csp.credomatic.com/Services/CSP/AuthorizationService.svc?singleWsdl';
       
        // construir la peticion
        $identification = array(
            'Password' => 'juGreifB3_1', // contraseña de conexion
            'UserName' => 'wcfnic2016', // usuario de conexion
        );

        // Parametros de reversion
        $request = array(
            'transactionType'   => 'REVERSE',
            'terminalId'        => 'EMVNIC12',
            'invoice'           => $invoice,
            'stream_context'=>stream_context_create(array('ssl'=>array('verify_peer'=>false,'verify_peer_name'=>false))),
        );

        // Construimos los parametros para el request
        $parametros = array(
            'identification'=>$identification,
            'request'=>$request,
        );

        // Opciones de conexion soap
        $options = array(
                'soap_version' => SOAP_1_2,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'connection_timeout' => 45,
                'trace' => true,
                'encoding' => 'UTF-8',
                'exceptions' => false,
            );

        // Ejecutamos la solicitud de reversion al servidor 
        $client = new SoapClient($wsdl, $options);
        $actionHeader = new \SoapHeader('http://www.w3.org/2005/08/addressing',
                               'Action',
                               'https://csp.credomatic.com/Services/CSP/AuthorizationService/IAuthorizationService/executeTransaction');
        $client->__setSoapHeaders($actionHeader);
        $client->__setLocation('https://csp.credomatic.com:50581/Services/CSP/AuthorizationService.svc');
        $response = $client->executeTransaction($parametros);

        if(is_soap_fault($response) || $response->executeTransactionResult->responseCode == 'NA'){

            // Guardamos el registro de la transaccion de reversion, en caso de error
            \Log::info('Error al hacer la reversión de la transacción '.$invoice );
            ini_set('default_socket_timeout', $original);
            return false; // Regresamos que hubo un error en la transaccion

        }else{

            // Guardamos el registro de la reversion
            \Log::info('Reversión APROBADA de la transacción '.$invoice);
            ini_set('default_socket_timeout', $original);
            return $response;

        }
    }

    


}
