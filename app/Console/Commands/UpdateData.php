<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;
use App\DonationAmount;
use App\DonationType;
use App\Country;
use App\Currency;
use App\CreditCard;
use App\Transaction;
use App\Campaign;
use App\Schedule;
use App\Ledger;
use App\Ip;
use Auth;
use Mail;
use App\Email;
use App\Report;


use SoapClient;
use Carbon\Carbon;

class UpdateData extends Command
{

    protected $signature = 'update:data';


    protected $description = 'Update all data from tables and saved in a report result.';


    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        \Log::info('Se comienza la actualización de los reportes y estadisticas');

        try{
            $donantes = User::whereHas('roles', function($q){$q->where('name', 'donante');})->get();
            $administradores = User::whereHas('roles', function($q){$q->where('name', 'superadmin');})->get();
            $managers = User::whereHas('roles', function($q){$q->where('name', 'manager');})->get();
            $donationtypes = DonationType::all();
            $donationamounts = DonationAmount::all();
            $countries = Country::all();
            $currencies = Currency::all();
            $creditcards = CreditCard::all();
            $creditcards_actives = 0;
            $creditcards_inactives = 0;
            $reports = Report::all();

            foreach ($reports as $item) {
                $item->amount = 0;
                $item->update();
            }

            foreach($creditcards as $item){
                if($item->status == 'active'){
                    $creditcards_actives++;
                }else{
                    $creditcards_inactives++;
                }
            }

            $transactions = Transaction::orderBy('id','asc')->get();
            $transaction_oks = 0;
            $transaction_errors = 0;
            $lastdonation = 0;
            $month = 0;
            $hoy = 0;
            $year = 0;
            $total = 0;
            $today = Carbon::now()->subHour(6);

            foreach($transactions as $item){

                if(trim($item->status) === 'APROBADA'){
                    $transaction_oks++;
                    $lastdonation = $item->amount;

                    $total += $item->amount;

                    if($item->created_at->toDateString() == $today->toDateString()){
                        $hoy += $item->amount;
                    }

                    if($item->created_at->month == $today->month ){
                        $month += $item->amount;
                    }

                    if($item->created_at->year == $today->year){
                        $year += $item->amount;
                    }


                    $day = carbon::parse($item->created_at);

                    $create_report = Report::where('year', $day->year )->where('month', $day->month )->first();

                    if($create_report){
                        $new_total = number_format($create_report->amount,2,'.',',') + number_format($item->amount,2,'.','');
                        $create_report->amount = number_format($new_total , 2,'.','');
                        $create_report->update();
                    }else{
                        $new_report = new Report;
                        $new_report->year = $day->year;
                        $new_report->month = $day->month;
                        $new_report->amount = number_format( $item->amount,2,'.','' );
                        $new_report->save();
                    }

                }else{
                    $transaction_errors++;
                }

            }
            
            $campaigns = Campaign::all();

            $schedules = Schedule::all();
            $schedule_actives = 0;
            $schedule_pendings = 0;

            foreach($schedules as $item){
                if($item->status == 'Procesado'){
                    $schedule_actives++;
                }else{
                    $schedule_pendings++;
                }
            }

            $ledger = Ledger::find(1);
            $ledger->donants = count($donantes);
            $ledger->admons = count($administradores);
            $ledger->managers = count($managers);
            $ledger->donationtypes = count($donationtypes);
            $ledger->donationamounts = count($donationamounts);
            $ledger->countries = count($countries);
            $ledger->currencies = count($currencies);
            $ledger->creditcards = count($creditcards);
            $ledger->creditcard_actives = $creditcards_actives;
            $ledger->creditcard_inactives = $creditcards_inactives;
            $ledger->transactions = count($transactions);
            $ledger->transaction_oks = $transaction_oks;
            $ledger->transaction_errors = $transaction_errors;
            $ledger->campaigns = count($campaigns);
            $ledger->schedules = count($schedules);
            $ledger->schedule_actives = $schedule_actives;
            $ledger->schedule_pendings = $schedule_pendings;
            $ledger->lastdonation = $lastdonation;
            $ledger->lastdonation = number_format($lastdonation,2,'.','');
            $ledger->month = number_format($month,2,'.','');
            $ledger->year = number_format($year,2,'.','');
            $ledger->total = number_format($total,2,'.','');
            $ledger->exchange_rate = $this->exchange_rate_bcn(); 
            $ledger->update();

            \Log::info('Se termino con exito el reporte de estadisticas');

        } catch(Exception $e) {
            \Log::info('Error '.$e.' en update:data ' );
        }

    }

    private function host(){

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
                $ip = $_SERVER["HTTP_CLIENT_IP"];
            }
            elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
                $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            }
            elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
                $ip = $_SERVER["HTTP_X_FORWARDED"];
            }
            elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
                $ip = $_SERVER["HTTP_FORWARDED_FOR"];
            }
            elseif (isset($_SERVER["HTTP_FORWARDED"])){
                $ip = $_SERVER["HTTP_FORWARDED"];
            }
            else{
                $ip = $_SERVER["REMOTE_ADDR"];
            }

        
            
        $new_ip = New Ip;
        $new_ip->ip = $ip;
        $new_ip->user_id = Auth::id(); 
        $new_ip->save();

        return $new_ip->id;
    }

    private function get_object_from_url($result){

        $result = substr($result, 1, strlen($result));

        $string = explode('&', $result);

        $return_result = array();

        foreach($string as $item){
            $value = explode('=', $item);
            $return_result[$value[0]] = $value[1];
        }

        return $return_result;
    }

    private function exchange_rate_bcn(){
        /*
         * CODIGO CLIENTE WSDL DE PHP
         */
        $servicio = "https://servicios.bcn.gob.ni/Tc_Servicio/ServicioTC.asmx?WSDL"; //url del servicio
        $parametros = array(); //parametros de la llamada

        $today = Carbon::now();

        $parametros['Ano'] = $today->year;
        $parametros['Mes'] = $today->month;
        $parametros['Dia'] = $today->day;

        // dd($parametros);

        $client = new \SoapClient($servicio);
        $result = $client->RecuperaTC_Dia($parametros); //llamamos al métdo que nos interesa con los parámetros
        $TasaDiaria = ($result->RecuperaTC_DiaResult);

        // if($TasaDiaria>0){
        //     return $TasaDiaria;
        // }else{
            $ledger = Ledger::find(1);
            return $ledger->exchange_rate; 
        // }
    }
}
