<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    protected $commands = [
        'App\Console\Commands\CheckReverse',
        'App\Console\Commands\UpdateData',
        'App\Console\Commands\MakeDebits',
    ];


    protected function schedule(Schedule $schedule)
    {
        $schedule->command('check:reverse')->hourly(); // Revisar si hay reversas pendientes cada hora
        $schedule->command('make:debits')->dailyAt('00:30'); // Hacemos todos los debitos las 12:30 am
        $schedule->command('update:data')->dailyAt('00:30'); // Actualizar el reporte de estadisticas del sistema
    }


    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
