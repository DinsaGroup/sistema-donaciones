<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reverse extends Model
{
    protected $table = 'reverses';

    protected $fillable = [ 'id', 'transaction_id'];

    public function transaction(){
        return $this->belongsTo('App\Transaction');
    }
}
