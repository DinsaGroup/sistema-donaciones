<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
    use SoftDeletes;

    protected $table = 'schedules';

    protected $fillable = [ 'donation_type_id', 'creditcard_id', 'campaign_id','amount' ,'lastProcess', 'nextProcess', 'failedAttempts', 'status', 'comments' ];

    public function donationtype(){
    	return $this->belongsTo('App\DonationType','donation_type_id','id');
    }

    public function creditcard(){
    	return $this->belongsTo('App\CreditCard');
    }

    public function campaign(){
    	return $this->belongsTo('App\Campaign');
    }
}
