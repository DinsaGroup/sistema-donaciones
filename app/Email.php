<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends Model
{
    use SoftDeletes;

    protected $table = 'emails';

    protected $fillable = [ 'user_id', 'subject', 'body' ];

    public function user(){
    	return $this->belongsTo('App\User');
    }

}
